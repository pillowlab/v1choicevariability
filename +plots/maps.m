% SCRIPT THAT GENERATES VSDI MEAN MAPS, AS IN FIG 2A,B
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Kinda hacky approach to recyclable code... MJM@191004

[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;
% faded = hsv2rgb(bsxfun(@times, rgb2hsv(jet), [1 0.9 0.9]));
faded = jet;

%% Means
f1 = figure; set(f1,'Color','white','units','normalized','Position',[.1 .3 .5 .53]);
colormap(f1,faded);
ha = tight_subplot(2, 4, [.025 .01], .05, .05);

AllMapData = [PresentMap(:)' AbsentMap(:)'...
    HitMap(:)' MissMap(:)'...
    FalseAlarmMap(:)' CorrectRejectMap(:)'...
    HitVMissMap(:)' FalseAlarmVCorrectRejectMap(:)'];
% clipHI = 1.5987;
% clipLO = -0.2665;
clipHI = 1.20 .* max(max(AllMapData));
clipLO = -0.20 .* max(max(AllMapData));
Clips = [clipLO clipHI];
axes(ha(1)); 
    imagesc(PresentMap',Clips); axis square;
    text(0.5,1.1,'Present','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(2)); 
    imagesc(HitMap',Clips); axis square;
    text(0.5,1.1,'Hit','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(3)); 
    imagesc(MissMap',Clips); axis square;
    text(0.5,1.1,'Miss','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(4)); 
    imagesc(HitVMissMap',Clips); axis square;
    text(0.5,1.1,'Hit-Miss','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(5)); 
    imagesc(AbsentMap',Clips); axis square;
    text(0.5,1.1,'Absent','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(6)); 
    imagesc(FalseAlarmMap',Clips); axis square;
    text(0.5,1.1,'FA','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(7)); 
    imagesc(CorrectRejectMap',Clips); axis square;
    text(0.5,1.1,'CR','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(ha(8)); 
    imagesc(FalseAlarmVCorrectRejectMap',Clips); axis square;
    text(0.5,1.1,'FA-CR','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')

set(ha,'xtick',[],'ytick',[],'fontsize',14)
set(ha(5),'ydir','normal','xtick',[0 1/2 1]*size(AbsentMap,1)+[1 1/2 0],'ytick',[0 1/2 1]*size(AbsentMap,1)+[1 1/2 0],'xticklabel',[-4 0 4],'yticklabel',[-4 0 4],'fontsize',14);

%% Variances
% f2 = figure; set(f2,'Color','white','units','normalized','Position',[.1 .3 .5 .53]);
% colormap(f2,faded);
% ha = tight_subplot(2, 4, [.025 .01], .05, .05);
% 
% AllVarData = sqrt([PresentVar(:)' AbsentVar(:)'...
%     HitVar(:)' MissVar(:)'...
%     FalseAlarmVar(:)' CorrectRejectVar(:)']);
% % clipHI = 1.5987;
% % clipLO = -0.2665;
% clipHI = max(AllVarData);
% clipLO = min(AllVarData);
% Clips = [clipLO clipHI];
% axes(ha(1)); 
%     imagesc(sqrt(PresentVar'),Clips); axis square;
%     text(0.5,1.1,'Present','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(2)); 
%     imagesc(sqrt(HitVar'),Clips); axis square;
%     text(0.5,1.1,'Hit','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(3)); 
%     imagesc(sqrt(MissVar'),Clips); axis square;
%     text(0.5,1.1,'Miss','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(4)); 
%     imagesc(HitVMissVar',Clips); axis square;
% 
% axes(ha(5)); 
%     imagesc(sqrt(AbsentVar'),Clips); axis square;
%     text(0.5,1.1,'Absent','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(6)); 
%     imagesc(sqrt(FalseAlarmVar'),Clips); axis square;
%     text(0.5,1.1,'FA','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(7)); 
%     imagesc(sqrt(CorrectRejectVar'),Clips); axis square;
%     text(0.5,1.1,'CR','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(8)); 
%     imagesc(FalseAlarmVCorrectRejectVar',Clips); axis square;
% 
% 
% set(ha,'xtick',[],'ytick',[],'fontsize',14)
% set(ha(5),'ydir','normal','xtick',[0 1/2 1]*size(AbsentVar,1)+[1 1/2 0],'ytick',[0 1/2 1]*size(AbsentVar,1)+[1 1/2 0],'xticklabel',[-4 0 4],'yticklabel',[-4 0 4],'fontsize',14);
