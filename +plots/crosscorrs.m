% SCRIPT THAT GENERATES CROSS CORRELATION PLOTS, AS IN FIG 7
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Kinda hacky approach to recyclable code... MJM@191004

[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;

B = 100;
[Distance1D,bootCorrelation1DAA] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bAA),B);
[Distance1D,bootCorrelation1DBB] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bBB),B);
if ~isnan(bCC)
[Distance1D,bootCorrelation1DCC] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bCC),B);
[Distance1D,bootCorrelation1DDD] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bDD),B);
end

bootCorrelationDifference = bootCorrelation1DAA - bootCorrelation1DBB;
pCorrelationDifference = sum(bootCorrelationDifference<0) / B;

% trim the data to include only correlations over some range in mm
trimsize = 5;
Distance1DForPlotting = Distance1D(1:end-trimsize);
bootCorrelation1DAAForPlotting = bootCorrelation1DAA(:,1:end-trimsize);
bootCorrelation1DBBForPlotting = bootCorrelation1DBB(:,1:end-trimsize);
if ~isnan(bCC)
bootCorrelation1DCCForPlotting = bootCorrelation1DCC(:,1:end-trimsize);
bootCorrelation1DDDForPlotting = bootCorrelation1DDD(:,1:end-trimsize);
end

% plot
axes(handles(1)); hold on; grid on;
if ~isnan(bCC)
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DAAForPlotting,BLUE,'-',4,sParameters);
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DBBForPlotting,BLUE,':',4,sParameters);
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DCCForPlotting,RED,':',4,sParameters);
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DDDForPlotting,RED,'-',4,sParameters); 
else
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DAAForPlotting,[0 0 0],'-',4,sParameters);
    PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DBBForPlotting,[0.5 0.5 0.5],':',4,sParameters);
end
xlim([-10 10]);
set(gca,'FontSize',14,'XTick',[-10 -5 0 5 10],'XTickLabel',[-10 -5 0 5 10],'YTick',0:0.25:1,'YTickLabel',0:0.25:1); 


%**********************************************************************
%******* Standard deviation of responses ******************************  <-- Stochastic
%**********************************************************************
B = 1000;

[bootVals AASTD AALO95 AAHI95 AALO99 AAHI99] = BootSTD(PaperScalars(bAA),B);
[bootVals BBSTD BBLO95 BBHI95 BBLO99 BBHI99] = BootSTD(PaperScalars(bBB),B);
if ~isnan(bCC)
[bootVals CCSTD CCLO95 CCHI95 CCLO99 CCHI99] = BootSTD(PaperScalars(bCC),B);
[bootVals DDSTD DDLO95 DDHI95 DDLO99 DDHI99] = BootSTD(PaperScalars(bDD),B);
end

axes(handles(2)); hold on;
if ~isnan(bCC)
    h = ShowBarPlotWithError(1,AASTD,AALO99,AAHI99,BLUE,'-',.5);
    h = ShowBarPlotWithError(2,BBSTD,BBLO99,BBHI99,BLUE,':',.5);
    h = ShowBarPlotWithError(3,CCSTD,CCLO99,CCHI99,RED,':',.5);
    h = ShowBarPlotWithError(4,DDSTD,DDLO99,DDHI99,RED,'-',.5);
else
    h = ShowBarPlotWithError(2,AASTD,AALO99,AAHI99,[0 0 0],'-',.5);
    h = ShowBarPlotWithError(3,BBSTD,BBLO99,BBHI99,[0.5 0.5 0.5],'-',.5);    
end
set(gca,'FontSize',14,'XTickLabel','','XTick',[1 2 3 4],'YTick',[0 1.5 3 4.5 6],'YTickLabel',[0 1.5 3 4.5 6]); 
xlim([0.5 4.5])


