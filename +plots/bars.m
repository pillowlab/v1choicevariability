% SCRIPT THAT GENERATES VSDI MEAN/SE BARS, AS IN FIG 2C
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Kinda hacky approach to recyclable code... MJM@191004

[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;

B = 1000;

% %load([ReadDir 'AmpsByModel.mat']);
% load([ReadDir 'data2/AmpsByModel.mat']);
% PaperScalars = AmpsByModel(1,:);
% PaperScalarsNorm01 = PaperScalars;
% PaperScalarsNorm01 = PaperScalarsNorm01 - mean(PaperScalarsNorm01(bA));
% PaperScalarsNorm01 = PaperScalarsNorm01 ./ mean(PaperScalarsNorm01(bP));

% fitted choice-triggered map
Coordinates.X = [1:size(Data.Amplitudes,1)];
Coordinates.Y = [1:size(Data.Amplitudes,2)];

[FitValuesChoiceTriggeredMap, ~] = FitGaussian2D(Coordinates,ChoiceTriggeredMap);
FittedChoiceTriggeredMap = FuncGaussian2D(FitValuesChoiceTriggeredMap, Coordinates);
% % f = figure;
% % subplot(1,2,1); imagesc(ChoiceTriggeredMap'); axis square;
% % subplot(1,2,2); imagesc(FittedChoiceTriggeredMap'); axis square;


[PaperScalarsNorm01, ~, ~] = PoolingModel_B_Weights(AmplitudesRenormalized,FittedChoiceTriggeredMap,bP,bA);


[bootValsAA AAMean AALOSE AAHISE AALO95 AAHI95 AALO99 AAHI99] = BootWeightedMean(PaperScalarsNorm01(bAA),Weights(bAA),B);
[bootValsBB BBMean BBLOSE BBHISE BBLO95 BBHI95 BBLO99 BBHI99] = BootWeightedMean(PaperScalarsNorm01(bBB),Weights(bBB),B);
[bootValsCC CCMean CCLOSE CCHISE CCLO95 CCHI95 CCLO99 CCHI99] = BootWeightedMean(PaperScalarsNorm01(bCC),Weights(bCC),B);
[bootValsDD DDMean DDLOSE DDHISE DDLO95 DDHI95 DDLO99 DDHI99] = BootWeightedMean(PaperScalarsNorm01(bDD),Weights(bDD),B);
% % [bootVals AAMean AALOSE AAHISE AALO95 AAHI95 AALO99 AAHI99] = BootMean(PaperScalarsNorm01(bAA),B);
% % [bootVals BBMean BBLOSE BBHISE BBLO95 BBHI95 BBLO99 BBHI99] = BootMean(PaperScalarsNorm01(bBB),B);
% % [bootVals CCMean CCLOSE CCHISE CCLO95 CCHI95 CCLO99 CCHI99] = BootMean(PaperScalarsNorm01(bCC),B);
% % [bootVals DDMean DDLOSE DDHISE DDLO95 DDHI95 DDLO99 DDHI99] = BootMean(PaperScalarsNorm01(bDD),B);

% bar plots with mean and SE
f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on;
h = ShowBarPlotWithError(1,AAMean,AALOSE,AAHISE,BLUE,'-',.5);
h = ShowBarPlotWithError(2,BBMean,BBLOSE,BBHISE,BLUE,':',.5);
h = ShowBarPlotWithError(3,CCMean,CCLOSE,CCHISE,RED,':',.5);
h = ShowBarPlotWithError(4,DDMean,DDLOSE,DDHISE,RED,'-',.5);
set(gca,'FontSize',18,'XTick',[1,2,3,4],'XTickLabel',{'Hit','Miss','FA','CR'},'YTick',[-0.5 0 0.5 1 1.5 2],'TickDir','out');
xlim([0.25 4.75]);
ylim(min([get(gca,'ylim'); -0.5 2],[],1));
ylabel('\Delta F/F (Normalized)')
line(get(gca,'xlim'),[0 0],'linewidth',1,'color','k');