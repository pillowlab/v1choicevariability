% SCRIPT THAT GENERATES TIME COURSE PLOTS, AS IN FIG 2D & FIG 6
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Run this script after f1_plot_timecourses; this code edits the figure made
% by that script.
%
% Kinda hacky approach to recyclable code... MJM@191123

[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;

patches = findobj(gcf,'type','patch');
delete(patches(1:3)); delete(patches(7:end));
% axes(ax1);
% x-axis for significance test patches
axs = findobj(gcf,'type','axes');
axes(axs(2));
gx = [timeAxis(hardstart:hardend) fliplr(timeAxis(hardstart:hardend))];

% H-M (hacked from PlotMeanAndSEWaveform)
gy = [bootAcrossTCBinnedPrc_HM(hardstart:hardend, 1)' fliplr(bootAcrossTCBinnedPrc_HM(hardstart:hardend, 2)')];
p = patch(gx, gy, BLUE); set(p, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
% CR-FA
gy = [bootAcrossTCBinnedPrc_FACR(hardstart:hardend, 1)' fliplr(bootAcrossTCBinnedPrc_FACR(hardstart:hardend, 2)')];
p = patch(gx, gy, RED); set(p, 'EdgeAlpha', 0, 'FaceAlpha', 0.075);
% P-A (hacked from PlotMeanAndSEWaveform)
gy = [bootAcrossTCBinnedPrc_PA(hardstart:hardend, 1)' fliplr(bootAcrossTCBinnedPrc_PA(hardstart:hardend, 2)')];
p = patch(gx, gy, [0.5 0.5 0.5]); set(p, 'EdgeAlpha', 0, 'FaceAlpha', 0.25);


% Overwrite default specs
set(findobj(gcf,'linewidth',6),'linewidth',2);