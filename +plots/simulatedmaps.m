% SCRIPT THAT GENERATES SIMULATION SUMMARY PLOTS, AS IN FIG 9
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Kinda hacky approach to recyclable code... MJM@191010

%% Maps
sims.showmaps(AmpsSim, bPs, bAs, bHs, bFAs, bMs, bCRs);

%% Cross-correlation comparison
% REAL DATA FIRST
f = figure; set(f,'Color','white','units','normalized','Position',[.1 .3 .4274 .3819]);
ha = tight_subplot(1, 2, 0.15, 0.2, [0.2 0.1]);
set(ha,'tickdir','out')
handles = ha([1 2]);
% Load preprocessed observed data
load([WriteDir 'AmpsRenorm_MM190913.mat'])
load([ReadDir 'AmpsByModel.mat']);
sParameters = Parameters();
% Append simulated data to observed data
PaperScalars = cat(2, AmpsByModel(8,:), (Rm * pool)'); % Used by crosscorrs <--- Our pooling rule should be #4?
AmplitudesRenormalized = cat(3, AmpsNorm, AmpsSim);
bAA = [bP; false(size(bPs))];
bBB = [bA; false(size(bAs))];
bCC = [false(size(bP)); bPs];
bDD = [false(size(bA)); bAs];
plots.crosscorrs;
set(handles(2),'XTickLabel',{'P, obs.    ','A, obs.  ','  P, sim.','    A, sim.'},'ytick',0:2:8,'yticklabel',0:2:8)
ylabel(handles(1),'Correlation coefficient')
xlabel(handles(1),'Cortical distance (mm)')
ylabel(handles(2),{'Standard deviation of','response amplitudes (normalized)'})

pp = flipud(findobj(gcf,'type','patch'));
ll = flipud(findobj(gcf,'type','line'));
% Post-hoc formatting hacks
set(pp([1 5]),'facecolor',[1 1 1],'edgecolor',[0 0 0], 'linewidth',3); set(pp([2 6]),'facecolor',[1 1 1],'edgecolor',[0.5 0.5 0.5], 'linewidth',3);
set(pp([3 7]),'facecolor',[0 0 0]); set(pp([4 8]),'facecolor',[0.5 0.5 0.5]);
set(ll([1 5 6 7]),'color',[0.5 0.5 0.5], 'linestyle',':'); set(ll([2 8 9 10]),'color',[0.75 0.75 0.75], 'linestyle',':');
set(ll([3 11 12 13]),'color',[0.5 0.5 0.5], 'linestyle','-'); set(ll([4]),'color',[0.75 0.75 0.75], 'linestyle','--'); set(ll([14 15 16]),'color',[0.75 0.75 0.75]);
set(findobj(gcf,'type','axes'), 'fontsize',16)

%% Performances
co = [0.5 0.5 0.5; BLUE; 0.5 0.5 0.5; RED];
f = figure; set(f,'Color','white','units','inches','Position',[1 1 5 5]);
set(f, 'DefaultAxesColorOrder', co,'defaultaxesfontsize',16)
ha = tight_subplot(1, 1, 0.1, 0.1, [0.15 0.1]);
bar([gt_performance; (performance.*[1 1 0 0])]', 'facealpha',0.5); hold on;
bar([gt_performance.*0; (performance.*[0 0 1 1])]', 'facealpha',0.5)
set(ha,'box','off','tickdir','out','Xtick',[1 2 3 4],'XTickLabel',{'H','M','FA','CR'}); axis square;
ylabel('Performance');
legend({'Observed Data','Simulated Data'},'location','north')

set(findobj('type','figure'),'renderer','painters')


%% Scalar barplots
PaperScalars = (Rm * pool)';
bAA = bHs;
bBB = bMs;
bCC = bFAs;
bDD = bCRs;

B = 1000;
Weights = ones(size(bAA));
[bootValsAA AAMean AALOSE AAHISE AALO95 AAHI95 AALO99 AAHI99] = BootWeightedMean(PaperScalars(bAA),Weights(bAA),B); %#ok<*NCOMMA>
[bootValsBB BBMean BBLOSE BBHISE BBLO95 BBHI95 BBLO99 BBHI99] = BootWeightedMean(PaperScalars(bBB),Weights(bBB),B);
[bootValsCC CCMean CCLOSE CCHISE CCLO95 CCHI95 CCLO99 CCHI99] = BootWeightedMean(PaperScalars(bCC),Weights(bCC),B);
[bootValsDD DDMean DDLOSE DDHISE DDLO95 DDHI95 DDLO99 DDHI99] = BootWeightedMean(PaperScalars(bDD),Weights(bDD),B);
% % [bootVals AAMean AALOSE AAHISE AALO95 AAHI95 AALO99 AAHI99] = BootMean(PaperScalarsNorm01(bAA),B);
% % [bootVals BBMean BBLOSE BBHISE BBLO95 BBHI95 BBLO99 BBHI99] = BootMean(PaperScalarsNorm01(bBB),B);
% % [bootVals CCMean CCLOSE CCHISE CCLO95 CCHI95 CCLO99 CCHI99] = BootMean(PaperScalarsNorm01(bCC),B);
% % [bootVals DDMean DDLOSE DDHISE DDLO95 DDHI95 DDLO99 DDHI99] = BootMean(PaperScalarsNorm01(bDD),B);

% bar plots with mean and SE
f = figure; set(f,'Color','white','units','inches','Position',[1 1 5 5]); hold on;
set(f, 'defaultaxesfontsize',16)
h = ShowBarPlotWithError(1,AAMean,AALOSE,AAHISE,BLUE,'-',.5);
h = ShowBarPlotWithError(2,BBMean,BBLOSE,BBHISE,BLUE,':',.5);
h = ShowBarPlotWithError(3,CCMean,CCLOSE,CCHISE,RED,':',.5);
h = ShowBarPlotWithError(4,DDMean,DDLOSE,DDHISE,RED,'-',.5);
set(gca,'fontsize',16, 'XTick',[1,2,3,4],'XTickLabel',{'H','M','FA','CR'},'YTick',[-0.5 0 0.5 1 1.5 2],'TickDir','out');
xlim([0.25 4.75]);
ylim(min([get(gca,'ylim'); -0.2 2],[],1));
ylabel('\Delta F/F (Normalized)')
line(get(gca,'xlim'),[0 0],'linewidth',1,'color','k');
set(findobj('type','figure'),'renderer','painters')

% LEGACY VERSION, SLIGHT DIFFERENCES IN VARIABLE NAMES
% %% Maps
% sims.showmaps(AmplitudesSimulated, bPs, bAs, bHs, bFAs, bMs, bCRs);
% 
% %% Cross-correlation comparison
% % REAL DATA FIRST
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .3 .5 .53]);
% ha = tight_subplot(2, 3, 0.1, 0.1, [0.1 0.05]);
% set(ha,'tickdir','out')
% handles = ha([1 2]);
% % Load preprocessed observed data
% load([WriteDir 'AmpsRenorm_MM190913.mat'])
% load([ReadDir 'AmpsByModel.mat']);
% sParameters = Parameters();
% % Append simulated data to observed data
% PaperScalars = cat(2, AmpsByModel(5,:), squeeze(nansum(nansum(bsxfun(@times, AmplitudesSimulated, decoder),1),2) ./ nansum(nansum(decoder)))');
% AmplitudesRenormalized = cat(3, AmplitudesRenormalized, AmplitudesSimulated);
% bAA = [bP; false(size(bPs))];
% bBB = [bA; false(size(bAs))];
% bCC = [false(size(bP)); bPs];
% bDD = [false(size(bA)); bAs];
% plots.crosscorrs;
% set(handles(2),'XTickLabel',{'P, obs.    ','A, obs.  ','  P, sim.','    A, sim.'},'ytick',0:2:8,'yticklabel',0:2:8)
% ylabel(handles(1),'Correlation coefficient')
% xlabel(handles(1),'Cortical distance (mm)')
% ylabel(handles(2),{'Standard deviation of','response amplitudes (normalized)'})
% 
% pp = flipud(findobj(gcf,'type','patch'));
% ll = flipud(findobj(gcf,'type','line'));
% set(pp([1 5]),'facecolor',[0 0 0]); set(pp([2 6]),'facecolor',[0.5 0.5 0.5]);
% set(pp([3 7]),'facecolor',PURPLE); set(pp([4 8]),'facecolor',GREEN);
% set(ll([1 5 6 7]),'color',[0 0 0]); set(ll([2 8 9 10]),'color',[0.5 0.5 0.5]);
% set(ll([3 11 12 13]),'color',PURPLE); set(ll([4 14 15 16]),'color',GREEN);
% 
% %% Performances
% co = [0.25 0.25 0.25;
%       YELLOW];
% f = figure; set(f,'Color','white','units','inches','Position',[1 1 5 5]);
% set(f, 'DefaultAxesColorOrder', co,'defaultaxesfontsize',16)
% ha = tight_subplot(1, 1, 0.1, 0.1, [0.15 0.1]);
% bar([PERFORMANCES; Phat]')
% set(ha,'box','off','tickdir','out','Xtick',[1 2 3 4],'XTickLabel',{'H','M','FA','CR'}); axis square;
% ylabel('Performance');
% legend({'Observed Data','Simulated Data'},'location','north')
% 
% set(findobj('type','figure'),'renderer','painters')