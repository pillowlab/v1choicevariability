% SCRIPT THAT GENERATES TIME COURSE PLOTS, AS IN FIG 2D & FIG 6
% (Assumes the necessary variables are already in the workspace and defined
% as desired/intended)
%
% Kinda hacky approach to recyclable code... MJM@191003

[BLUE, RED, PURPLE, GREEN, YELLOW, ALTBLUE, ALTRED] = get_colors;

f = figure; hold on; set(f, 'Color', 'w');
hardstart = MS2Frame(Data.timeAxis,XLIMS(1));
hardend = MS2Frame(Data.timeAxis,XLIMS(2)); % max(IntegrationTimes.EndFrame)-1;
% widths = [diff([hardstart hardend])+12.5 diff([100 120])]; widths = widths ./ sum(widths);
widths = [diff([hardstart hardend]) 16]; widths = widths ./ sum(widths);

YLIMS = [-1.65 3];
    significance_HM(1:hardstart) = false;
    significance_FACR(1:hardstart) = false;
    significance_PA(1:hardstart) = false;
% integRange = [min(IntegrationTimes.StartMS) max(IntegrationTimes.EndMS)];
plotRange = [hardstart hardend];
ax1 = subplot('position',[.1 .2 0.75*widths(1) .75]); hold on;
h1 = CreateWeightedTimeCoursePanel(SPTimeCourseRenormalized,Weights,bH,bM,bFA,bCR,RT,Data.timeAxis,bShowPlot,plotRange,integRange,0,YLIMS);
set(gca,'fontsize',14,'ytick',-1:3); boto;
xlim(XLIMS);
% Overwrite default specs for thicc lines
set(findobj(gcf,'linewidth',6),'linewidth',2);

% Add significance patches
y = -1.0;
xedges = timeAxis([find(significance_PA, 1, 'first'), find(significance_PA, 1, 'last')]);
xerr = abs(xedges(1) - firstCrossPrc_PA);
yedges = y - [-0.05, 0.05];
patch([xedges fliplr(xedges)], reshape([yedges; yedges], 1, []), [.5 .5 .5], 'facealpha',0.5, 'edgecolor', 'none')
errorbar(xedges(1), y, xerr(1), xerr(2), 'horizontal', 'color', [.5 .5 .5], 'linewidth', 2, 'markersize', 8, 'marker', 'o')
%
y = -1.25;
xedges = timeAxis([find(significance_HM, 1, 'first'), find(significance_HM, 1, 'last')]);
xerr = abs(xedges(1) - firstCrossPrc_HM);
yedges = y - [-0.05, 0.05];
patch([xedges fliplr(xedges)], reshape([yedges; yedges], 1, []), BLUE, 'facealpha',0.5, 'edgecolor', 'none')
errorbar(xedges(1), y, xerr(1), xerr(2), 'horizontal', 'color', BLUE, 'linewidth', 2, 'markersize', 8, 'marker', 'o')
%
y = -1.5;
xedges = timeAxis([find(significance_FACR, 1, 'first'), find(significance_FACR, 1, 'last')]);
xerr = abs(xedges(1) - firstCrossPrc_FACR);
yedges = y - [-0.05, 0.05];
patch([xedges fliplr(xedges)], reshape([yedges; yedges], 1, []), RED, 'facealpha',0.5, 'edgecolor', 'none')
errorbar(xedges(1), y, xerr(1), xerr(2), 'horizontal', 'color', RED, 'linewidth', 2, 'markersize', 8, 'marker', 'o')

% % Add significance
% prev = -10; 
% for k = find(significance_PA)
%     if k-prev-10*eps < 1
%         plot(timeAxis([prev k]), 0*timeAxis([prev k])-1.125, 'linewidth',2, 'color',[.5 .5 .5]) 
%     end 
%     prev = k; 
% end
% prev = -10; 
% for k = find(significance_HM)
%     if k-prev-10*eps < 1
%         plot(timeAxis([prev k]), 0*timeAxis([prev k])-1.25, 'linewidth',2, 'color',BLUE)
%     end 
%     prev = k; 
% end
% prev = -10; 
% for k = find(significance_FACR)
%     if k-prev-10*eps < 1
%         plot(timeAxis([prev k]), 0*timeAxis([prev k])-1.375, 'linewidth',2, 'color',RED)
%     end 
%     prev = k; 
% end
xlabel('Time (ms)','fontsize',14)
ylabel('Normalized \Delta F/F')

[timeAxisSacAligned, SPTimeCourseRenormalizedSacAligned] = ...
    AlignTimeCourseToSac(SPTimeCourseRenormalized,bH,bM,bFA,bCR,Data.timeAxis,IntegrationTimes.StartFrame,IntegrationTimes.EndFrame);

% plotRange = [110 120];
% subplot('position',[.55 .2 .075 .7]); hold on;
plotRange = [100 118];
ax2 = subplot('position',[.15+0.75*widths(1) .2 0.75*widths(2) .75]); hold on;
h2 = CreateWeightedTimeCoursePanel(SPTimeCourseRenormalizedSacAligned,Weights,bH,bM,bFA,bCR,Data.RT,timeAxisSacAligned,bShowPlot,plotRange,integRange,1,YLIMS);
set(gca,'fontsize',14,'ytick',-1:3); boto;

% Overwrite default specs
set(findobj(gcf,'linewidth',6),'linewidth',2);