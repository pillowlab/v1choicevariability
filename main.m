
% ******* get local file directories ***************************************
% adjust this file with local file directories as needed:
setDirs;


% ******* load Preprocessed data *******************************************
load([ReadDir 'Preprocessed.mat']);
load([ReadDir 'Inclusion.mat']);

% ******* set parameters for analysis **************************************
sParameters = Parameters();

% ******* reduce data ******************************************************
% First reduction in trials: 10163 --> 10153
load([WriteDir 'dat_MM190805.mat']);
Amplitudes = Data.Amplitudes(:,:,bInclude);
SPTimeCourse = Data.SPTimeCourse(:,bInclude);
RT = Data.RT(bInclude);
Xs = Data.Xs(bInclude,:);
Ys = Data.Ys(bInclude,:);
TargCtrX = Data.TargCtrX(bInclude);
TargCtrY = Data.TargCtrY(bInclude);
AdjustmentX = Data.AdjustmentX(bInclude);
AdjustmentY = Data.AdjustmentY(bInclude);
% All of these variables are __ x 10153 trials
clear dat;
clear bInclude;

% Second reduction in trials: 10163 --> 10153 --> 9979, OK!
load([WriteDir 'dat_MM190805_processed.mat']);
Amplitudes = Amplitudes(:,:,bInclude);
SPTimeCourse = SPTimeCourse(:,bInclude);
RT = RT(bInclude);
Xs = Data.Xs(bInclude,:);
Ys = Data.Ys(bInclude,:);
TargCtrX = Data.TargCtrX(bInclude);
TargCtrY = Data.TargCtrY(bInclude);
AdjustmentX = Data.AdjustmentX(bInclude);
AdjustmentY = Data.AdjustmentY(bInclude);

Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

bInclude = bInclude(bInclude);

%%%%%% debug check
% CurrentList = unique([dat.date dat.monkey],'rows')
% numTex = size(CurrentList(CurrentList(:,end)==1,:),1)
% numCharlie = size(CurrentList(CurrentList(:,end)==2,:),1)
%%%%%%

%%

% ******* remove high contrast trials and choose monkey(s) *****************
bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
bMyMonkey = dat.monkey == 1 | dat.monkey == 2; % ALL
% bMyMonkey = dat.monkey == 1; % MONKEY T
% bMyMonkey = dat.monkey == 2; % MONKEY C
% bMyMonkey = dat.datenum == 11; % REPRESENTATIVE SESSION
bInclude = bMyMonkey & ~bIsInHighContrastBlock;

bP = dat.bP & bInclude;
bA = dat.bA & bInclude;
bH = dat.bH & bInclude;
bM = dat.bM & bInclude;
bFA = dat.bFA & bInclude;
bCR = dat.bCR & bInclude;

% ******* re-normalize *****************************************************
nrmContrResampled = dat.nrmContr;
nrmContrResampled(bA) = randsample(nrmContrResampled(bP),sum(bA),'true');
RenormalizationWeightsSpace = permute(repmat(nrmContrResampled,[1 17 17]),[2 3 1]);
AmplitudesRenormalized = Amplitudes ./ RenormalizationWeightsSpace;
RenormalizationWeightsTime = permute(repmat(nrmContrResampled,[1 120]),[2 1]);
SPTimeCourseRenormalized = SPTimeCourse ./ RenormalizationWeightsTime;


% ******* weight experiments by SNR ****************************************
% AmplitudesRenormalized = AmplitudesRenormalized .* permute(repmat(dat.nrmSNR,[1 17 17]),[2 3 1]);
% SPTimeCourseRenormalized = SPTimeCourseRenormalized .* permute(repmat(dat.nrmSNR,[1 120]),[2 1]);
Weights = dat.nrmSNR';


% ******* Normalize for Display only ***************************************

PresentMap = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bP);
displayNormFactor = max(max(PresentMap));

AmplitudesRenormalized = AmplitudesRenormalized ./ ...
    repmat(displayNormFactor, ...
    [size(AmplitudesRenormalized,1), ...
    size(AmplitudesRenormalized,2), ...
    size(AmplitudesRenormalized,3) ...
    ]); 

% Useful for simulations or analyses depending only on maps and trial types
% save([WriteDir 'dat_onlyAmpsRenorm_MM190913.mat'],'AmplitudesRenormalized','bP','bA','bH','bM','bFA','bCR')

% ******* Compute Maps *****************************************************
[PresentMap, PresentVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bP);
[AbsentMap, AbsentVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bA);
[HitMap, HitVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bH);
[MissMap, MissVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bM);
[FalseAlarmMap, FalseAlarmVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bFA);
[CorrectRejectMap, CorrectRejectVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bCR);
HitVMissMap = HitMap - MissMap;
HitVMissVar = zeros(size(HitVar));
FalseAlarmVCorrectRejectMap = FalseAlarmMap - CorrectRejectMap;
FalseAlarmVCorrectRejectVar = zeros(size(HitVar));

% ******* Choice-Triggered Map *****************************************
AmplitudesNormalizedStimulusRemoved = AmplitudesRenormalized;
AmplitudesNormalizedStimulusRemoved(:,:,bP) = AmplitudesNormalizedStimulusRemoved(:,:,bP) - repmat(PresentMap,[1 1 sum(bP)]);
GoMap = nanmean(AmplitudesNormalizedStimulusRemoved(:,:,(bH|bFA)),3);
NoGoMap = nanmean(AmplitudesNormalizedStimulusRemoved(:,:,(bM|bCR)),3);
ChoiceTriggeredMap = GoMap - NoGoMap;


% FIGURE 2A/B
%**************************************************************************
plots.maps;
set(f1,'Color','white','units','inches','Position',[1 1 7 5]);
% set(f2,'Color','white','units','inches','Position',[1 1 7 5]);


%%
integRange = [min(IntegrationTimes.StartMS(bP|bA)) max(IntegrationTimes.EndMS(bP|bA))];
integRangeFrames = MS2Frame(Data.timeAxis,integRange);
%GrandPresentTimeCourses = SPTimeCourse(:,(bH | bM));

[meanGrandAbsentTimeCourse, ~] = ...
    ComputeWeightedMeanAndSEWaveform(SPTimeCourseRenormalized,Weights,bA);
meanA = nanmean(meanGrandAbsentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
SPTimeCourseRenormalized = SPTimeCourseRenormalized - meanA; % makes average of absent trials 0 in integration window

[meanGrandPresentTimeCourse, ~] = ...
    ComputeWeightedMeanAndSEWaveform(SPTimeCourseRenormalized,Weights,bP);
meanP = nanmean(meanGrandPresentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
SPTimeCourseRenormalized = SPTimeCourseRenormalized ./ meanP; % makes average of present trials 1 in integration window

% FIGURE 2C
%**************************************************************************
bShowPlot = [0 1 1 0 1 1 0 0];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR]
XLIMS = [0 340]; % [-300 340];
plots.timecourses;
set(f,'Color','white','units','inches','Position',[1 1 4 * 7/5 4]);

% FIGURE 2D
%**************************************************************************
% Note: Doesn't match the original paper, which reported these quantities
% without SNR-reweighting. I report **with** SNR reweighting, to be
% consistent with all other reports from the paper. --MJM@191004

% Mean and standard error of responses
bAA = bH;
bBB = bM;
bCC = bFA;
bDD = bCR;
plots.bars;
set(f,'Color','white','units','inches','Position',[1 1 4 * 7/5 4],'renderer','painters');

% significance testing - unweighted
pHvM = sum((bootValsAA-bootValsBB)<0)/B;
pFAvCR = sum((bootValsCC-bootValsDD)<0)/B;
pFAvM = sum((bootValsCC-bootValsBB)<0)/B;

%% Correlation plots

load([ReadDir 'AmpsByModel.mat']);
PaperScalars = AmpsByModel(1,:);

f = figure; set(f,'Color','white','units','normalized','Position',[.1 .3 .5 .53]);
ha = tight_subplot(2, 3, 0.1, 0.1, [0.1 0.05]);
set(ha,'tickdir','out')

% FIGURE 7A/B
%**************************************************************************
handles = ha([1 4]);
bAA = bP;
bBB = bA;
bCC = NaN;
bDD = NaN;
plots.crosscorrs;
axes(ha(4));
set(ha(4),'XTickLabel',{'','Present      ','      Absent',''})
ylabel(ha(1),'Correlation coefficient')
xlabel(ha(1),'Cortical distance (mm)')
ylabel(ha(4),{'SD of \Delta F/F (normalized)'})

% FIGURE 7C/D
%**************************************************************************
handles = ha([2 5]);
bAA = (bH|bCR);
bBB = (bM|bFA);
bCC = NaN;
bDD = NaN;
plots.crosscorrs;
set(ha(5),'XTickLabel',{'','Correct      ','      Incorrect',''})

% FIGURE 7E/F
%**************************************************************************
handles = ha([3 6]);
bAA = bH;
bBB = bM;
bCC = bFA;
bDD = bCR;
plots.crosscorrs;
set(ha(6),'XTickLabel',{'H','M','FA','CR'})

set(f,'Color','white','units','inches','Position',[1 1 10 7],'renderer','painters');

% %%
% %******* Correlation as a Function of Distance ************************  <-- Stochastic
% bAA = (bH|bCR);
% bBB = (bM|bFA);
% bCC = NaN;
% bDD = NaN;
% 
% B = 100;
% [Distance1D,bootCorrelation1DP] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bAA),B);
% [Distance1D,bootCorrelation1DA] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bBB),B);
% bootCorrelationDifference = bootCorrelation1DA - bootCorrelation1DP;
% pCorrelationDifference = sum(bootCorrelationDifference<0) / B
% 
% % trim the data to include only correlations over some range in mm
% trimsize = 5;
% Distance1DForPlotting = Distance1D(1:end-trimsize);
% bootCorrelation1DPForPlotting = bootCorrelation1DP(:,1:end-trimsize);
% bootCorrelation1DAForPlotting = bootCorrelation1DA(:,1:end-trimsize);
% 
% % plot
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on; grid on;
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DPForPlotting,'k','-',4,sParameters);
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DAForPlotting,'k','--',4,sParameters);
% %set(gca,'XTickLabel',
% xlim([-10 10]);
% set(gca,'FontSize',24);
% 
% 
% %******* Standard deviation of responses ******************************  <-- Stochastic
% B = 1000;
% load([ReadDir 'AmpsByModel.mat']);
% PaperScalars = AmpsByModel(1,:);
% 
% [bootVals PSTD PLO95 PHI95 PLO99 PHI99] = BootSTD(PaperScalars(bAA),B);
% [bootVals ASTD ALO95 AHI95 ALO99 AHI99] = BootSTD(PaperScalars(bBB),B);
% 
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on;
% h = ShowBarPlotWithError(1,PSTD,PLO99,PHI99,'k','-',.5);
% h = ShowBarPlotWithError(2,ASTD,ALO99,AHI99,'k','--',.5);
% set(gca,'FontSize',14,'XTickLabel',''); boto;
% 
% %% FIGURE 7E/F
% %**************************************************************************
% %**************************************************************************
% 
% %******* Correlation as a Function of Distance ************************  <-- Stochastic
% bAA = bH;
% bBB = bM;
% bCC = bFA;
% bDD = bCR;
% 
% B = 100;
% [Distance1D,bootCorrelation1DAA] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bAA),B);
% [Distance1D,bootCorrelation1DBB] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bBB),B);
% [Distance1D,bootCorrelation1DCC] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bCC),B);
% [Distance1D,bootCorrelation1DDD] = BootCalculateCorrByDistance(AmplitudesRenormalized(:,:,bDD),B);
% 
% bootCorrelationDifference = bootCorrelation1DAA - bootCorrelation1DBB;
% pCorrelationDifference = sum(bootCorrelationDifference<0) / B
% 
% % trim the data to include only correlations over some range in mm
% trimsize = 5;
% Distance1DForPlotting = Distance1D(1:end-trimsize);
% bootCorrelation1DAAForPlotting = bootCorrelation1DAA(:,1:end-trimsize);
% bootCorrelation1DBBForPlotting = bootCorrelation1DBB(:,1:end-trimsize);
% bootCorrelation1DCCForPlotting = bootCorrelation1DCC(:,1:end-trimsize);
% bootCorrelation1DDDForPlotting = bootCorrelation1DDD(:,1:end-trimsize);
% 
% % plot
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on; grid on;
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DAAForPlotting,'g','-',4,sParameters);
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DBBForPlotting,'g','--',4,sParameters);
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DCCForPlotting,'r','--',4,sParameters);
% PlotCorrByDistance(Distance1DForPlotting,bootCorrelation1DDDForPlotting,'r','-',4,sParameters);
% xlim([-10 10]);
% set(gca,'FontSize',24);
% 
% 
% %**********************************************************************
% %******* Standard deviation of responses ******************************  <-- Stochastic
% %**********************************************************************
% B = 1000;
% load([ReadDir 'AmpsByModel.mat']);
% PaperScalars = AmpsByModel(1,:);
% 
% [bootVals AASTD AALO95 AAHI95 AALO99 AAHI99] = BootSTD(PaperScalars(bAA),B);
% [bootVals BBSTD BBLO95 BBHI95 BBLO99 BBHI99] = BootSTD(PaperScalars(bBB),B);
% [bootVals CCSTD CCLO95 CCHI95 CCLO99 CCHI99] = BootSTD(PaperScalars(bCC),B);
% [bootVals DDSTD DDLO95 DDHI95 DDLO99 DDHI99] = BootSTD(PaperScalars(bDD),B);
% 
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on;
% h = ShowBarPlotWithError(1,AASTD,AALO99,AAHI99,'g','-',.5);
% h = ShowBarPlotWithError(2,BBSTD,BBLO99,BBHI99,'g','--',.5);
% h = ShowBarPlotWithError(3,CCSTD,CCLO99,CCHI99,'r','--',.5);
% h = ShowBarPlotWithError(4,DDSTD,DDLO99,DDHI99,'r','-',.5);
% set(gca,'FontSize',24,'XTickLabel',''); 

%% Reviewer Request: Test for lateral shift of FA responses on Monkey C
% This is a straightforward bootstrap in practice of H and FA labels

% ******* remove high contrast trials and choose monkey(s) *****************
bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
bMyMonkey = dat.monkey == 2; % MONKEY C
bInclude = bMyMonkey & ~bIsInHighContrastBlock;

bP = dat.bP & bInclude;
bA = dat.bA & bInclude;
bH = dat.bH & bInclude;
bFA = dat.bFA & bInclude;

nrmContrResampled = dat.nrmContr;
nrmContrResampled(bA) = randsample(nrmContrResampled(bP),sum(bA),'true');
RenormalizationWeightsSpace = permute(repmat(nrmContrResampled,[1 17 17]),[2 3 1]);
AmplitudesRenormalized = Amplitudes ./ RenormalizationWeightsSpace;
Weights = dat.nrmSNR';
Coordinates.X = [1:size(Amplitudes,1)];
Coordinates.Y = [1:size(Amplitudes,2)];

% ******* Compute Maps and Best-fit 2D Gaussian ****************************
[HitMap, HitVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bH);
[FalseAlarmMap, FalseAlarmVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bFA);


% present map
[FitValuesHitMap, ~] = FitGaussian2D(Coordinates, HitMap);
[FitValuesFAMap, ~] = FitGaussian2D(Coordinates, FalseAlarmMap);
trueHFAdist = norm(FitValuesHitMap(2:3) - FitValuesFAMap(2:3));
    
nboot = 10000;
HFAdist = nan(nboot,1);
bGo = (bH | bFA);
nH = sum(bH);
inds = find(bGo);
for k = 1:nboot
    bH = false(size(bH));
    bFA = false(size(bFA));
    perm = inds(randperm(numel(inds)));
    bH(perm(1:nH)) = true;
    bFA(perm(nH+1:end)) = true;
    
    % ******* Compute Maps and Best-fit 2D Gaussian ****************************
    [HitMap, HitVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bH);
    [FalseAlarmMap, FalseAlarmVar] = ComputeWeightedMeanMap(AmplitudesRenormalized,Weights,bFA);

    % present map
    [FitValuesHitMap, ~] = FitGaussian2D(Coordinates, HitMap);
    [FitValuesFAMap, ~] = FitGaussian2D(Coordinates, FalseAlarmMap);
    HFAdist(k) = norm(FitValuesHitMap(2:3) - FitValuesFAMap(2:3));
    if mod(k, nboot / 20) == 0
        fprintf('.')
    end
end
fprintf('\n')

%% UNKNOWN

% bootValsTesting = randn([1 B]);
% % sampling distributions of means
% dx = .01;
% X = [-1:dx:2.5];
% % PrTestingbar = hist(bootValsTesting,X); PrTestingbar = PrTestingbar ./ sum(PrTestingbar.*dx);
% PrHbar = hist(bootValsAA,X); PrHbar = PrHbar ./ sum(PrHbar.*dx);
% PrMbar = hist(bootValsBB,X); PrMbar = PrMbar ./ sum(PrMbar.*dx);
% PrFAbar = hist(bootValsCC,X); PrFAbar = PrFAbar ./ sum(PrFAbar.*dx);
% PrCRbar = hist(bootValsDD,X); PrCRbar = PrCRbar ./ sum(PrCRbar.*dx);
% f = figure; set(f,'Color','white','units','normalized','Position',[.1 .1 .6 .7]); hold on;
% % h = plot(X,PrTestingbar); set(h,'Color','k','LineStyle','-','LineWidth',2);
% h = plot(X,PrHbar); set(h,'Color','g','LineStyle','-','LineWidth',2);
% h = plot(X,PrMbar); set(h,'Color','g','LineStyle','--','LineWidth',2);
% h = plot(X,PrFAbar); set(h,'Color','r','LineStyle','--','LineWidth',2);
% h = plot(X,PrCRbar); set(h,'Color','r','LineStyle','-','LineWidth',2);
% set(gca,'FontSize',18);
% 
% % fit sampling distributions with Gaussians (central limit theorem)
% % [P0Testing PhatTesting] = FitGaussian(X,PrTestingbar);
% % PrTestingbarhat = FuncGaussian(PhatTesting,X);
% % h = plot(X,PrTestingbarhat); set(h,'Color','k','LineStyle','-','LineWidth',4);
% [P0H PhatH] = FitGaussian(X,PrHbar);
% PrHbarhat = FuncGaussian(PhatH,X);
% h = plot(X,PrHbarhat); set(h,'Color','g','LineStyle','-','LineWidth',4);
% [P0M PhatM] = FitGaussian(X,PrMbar);
% PrMbarhat = FuncGaussian(PhatM,X);
% h = plot(X,PrMbarhat); set(h,'Color','g','LineStyle','--','LineWidth',4);
% [P0FA PhatFA] = FitGaussian(X,PrFAbar);
% PrFAbarhat = FuncGaussian(PhatFA,X);
% h = plot(X,PrFAbarhat); set(h,'Color','r','LineStyle','--','LineWidth',4);
% [P0CR PhatCR] = FitGaussian(X,PrCRbar);
% PrCRbarhat = FuncGaussian(PhatCR,X);
% h = plot(X,PrCRbarhat); set(h,'Color','r','LineStyle','-','LineWidth',4);
% 
% 
% 
% 
% % compute significance with boot weighted mean difference
% bootVals = BootWeightedMeanDifference(PaperScalarsNorm01(bCC),PaperScalarsNorm01(bBB),B,Weights(bCC),Weights(bBB));
% sum(bootVals<0)/length(bootVals)


%% UNKNOWN

% bAAA = bFA;
% bBBB = bM;
% B = 100;
% K = (sum(bAAA) + sum(bBBB)) / 2;
% AAAinds = find(bAAA);
% AAAweights = dat.nrmSNR(bAAA);
% AAAsamples = zeros(1,length(bAAA));
% 
% clear DiffVal;
% clear bsMeanDiffVal;
% 
% for i=1:B
%     i
%     for j=1:K
%         % choose a AAA trial at random (weighted by SNR)
%         thisAAAInd = randsample(AAAinds,1,true,AAAweights);
%         thisAAAVal = PaperScalarsNorm01(thisAAAInd);
%         AAAsamples(thisAAAInd) = AAAsamples(thisAAAInd) + 1;
%         
%         % identify which experiment that trial came from
%         thisDateNum = dat.datenum(thisAAAInd);
%         bThisDateNum = dat.datenum==thisDateNum;
%         
%         % choose a BBB trial from that same experiment
%         BBBinds = find(bBBB & bThisDateNum);
%         thisBBBInd = randsample(BBBinds,1,true);
%         thisBBBVal = PaperScalarsNorm01(thisBBBInd);
%         
%         % calculate the difference between the AAA and BBB values
%         DiffVal(j) = thisAAAVal - thisBBBVal;
%     end
%     bsMeanDiffVal(i) = mean(DiffVal);    
% end
% 
% f = figure;
% subplot(1,2,1);
% hist(bsMeanDiffVal)
% sum(bsMeanDiffVal<0)/length(bsMeanDiffVal)
% 
% subplot(1,2,2); hold on;
% plot(dat.nrmSNR,'b');
% plot(AAAsamples./max(AAAsamples),'g');



%%


% % %**********************************************************************
% % %******* Use Jonathan's results to create theoretical distributions ***
% % %**********************************************************************
% % % first I run script3_inspectPredictedDists_on01.m to get results
% % % THIS VERSION DOES NOT RESCALE THE DATA
% % 
% % ppPresent = ppHit(:,2) + ppMiss(:,2);
% % meanP = sum(rr.*ppPresent)/sum(ppPresent)
% % meanH = sum(rr.*ppHit(:,2))/sum(ppHit(:,2))
% % meanM = sum(rr.*ppMiss(:,2))/sum(ppMiss(:,2))
% % ppAbsent = ppFA(:,2) + ppCR(:,2);
% % meanA = sum(rr.*ppAbsent)/sum(ppAbsent)
% % meanFA = sum(rr.*ppFA(:,2))/sum(ppFA(:,2))
% % meanCR = sum(rr.*ppCR(:,2))/sum(ppCR(:,2))
% % 
% % % then plot the data
% % f = figure; set(f, 'Color', 'w'); hold on;
% % myLineWidth = 2;
% % h = plot(rr,ppPresent,'b-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppHit(:,1),'g-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppMiss(:,1),'g--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppAbsent,'k-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppFA(:,1),'r--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppCR(:,1),'r-'); set(h,'LineWidth',myLineWidth);
% % 
% % myLineWidth = 4;
% % h = plot(rr,ppHit(:,2),'g-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppMiss(:,2),'g--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppFA(:,2),'r--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rr,ppCR(:,2),'r-'); set(h,'LineWidth',myLineWidth);
% % 
% % 
% % myXlim = [-4 5];
% % myYlim = [-.03 .12];
% % % xlim(myXlim);
% % % ylim(myYlim);
% % arrowYlo = 0.75*min(myYlim);
% % %arrowYhi = 0.25*min(myYlim);
% % arrowYhi = max(myYlim);
% % arrowwidth = 5;
% % arrowheadweight = 10;
% % 
% % % draw arrows
% % h = DrawArrow([meanP meanP],[arrowYlo arrowYhi],'^','b',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanH meanH],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanM meanM],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-.');
% % h = DrawArrow([meanA meanA],[arrowYlo arrowYhi],'^','k',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanFA meanFA],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-.');
% % h = DrawArrow([meanCR meanCR],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-');
% % 
% % axis square;
% % myFontSize = 32;
% % set(gca,'FontSize',myFontSize);
% % set(gca,'XTick',[-4 -2 0 2 4],'XTickLabel',[{-4},{-2},{0},{2},{4},{5}]);

%%
% % %**********************************************************************
% % %******* Use Jonathan's results to create theoretical distributions ***
% % %**********************************************************************
% % % first I run script3_inspectPredictedDists_on01.m to get results
% % % THIS VERSION RESCALES THE DATA
% % % rescale the data (for display only) to {0,1}
% % ppPresent = ppHit(:,2) + ppMiss(:,2);
% % meanP = sum(rr.*ppPresent)/sum(ppPresent)
% % meanH = sum(rr.*ppHit(:,2))/sum(ppHit(:,2))
% % meanM = sum(rr.*ppMiss(:,2))/sum(ppMiss(:,2))
% % ppAbsent = ppFA(:,2) + ppCR(:,2);
% % meanA = sum(rr.*ppAbsent)/sum(ppAbsent)
% % meanFA = sum(rr.*ppFA(:,2))/sum(ppFA(:,2))
% % meanCR = sum(rr.*ppCR(:,2))/sum(ppCR(:,2))
% % rrnew = (rr - meanA) ./ meanP;
% % 
% % % then plot the data
% % f = figure; set(f, 'Color', 'w'); hold on;
% % myLineWidth = 2;
% % %h = plot(rrnew,ppPresent,'b-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppHit(:,1),'g-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppMiss(:,1),'g--'); set(h,'LineWidth',myLineWidth);
% % %h = plot(rrnew,ppAbsent,'k-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppFA(:,1),'r--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppCR(:,1),'r-'); set(h,'LineWidth',myLineWidth);
% % 
% % myLineWidth = 4;
% % h = plot(rrnew,ppHit(:,2),'g-'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppMiss(:,2),'g--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppFA(:,2),'r--'); set(h,'LineWidth',myLineWidth);
% % h = plot(rrnew,ppCR(:,2),'r-'); set(h,'LineWidth',myLineWidth);
% % 
% % 
% % myXlim = [-4 5];
% % myYlim = [-.03 .12];
% % xlim(myXlim);
% % ylim(myYlim);
% % arrowYlo = 0.75*min(myYlim);
% % arrowYhi = 0.25*min(myYlim);
% % %arrowYhi = max(myYlim);
% % arrowwidth = 5;
% % arrowheadweight = 10;
% % 
% % ppPresent = ppHit(:,2) + ppMiss(:,2);
% % meanP = sum(rrnew.*ppPresent)/sum(ppPresent)
% % meanH = sum(rrnew.*ppHit(:,2))/sum(ppHit(:,2))
% % meanM = sum(rrnew.*ppMiss(:,2))/sum(ppMiss(:,2))
% % ppAbsent = ppFA(:,2) + ppCR(:,2);
% % meanA = sum(rrnew.*ppAbsent)/sum(ppAbsent)
% % meanFA = sum(rrnew.*ppFA(:,2))/sum(ppFA(:,2))
% % meanCR = sum(rrnew.*ppCR(:,2))/sum(ppCR(:,2))
% % 
% % % muAhitpred = rr(find(ppHit(:,2)==max(ppHit(:,2))));
% % % muAmisspred = rr(find(ppMiss(:,2)==max(ppMiss(:,2))));
% % % muAfapred = rr(find(ppFA(:,2)==max(ppFA(:,2))));
% % % muAcrpred = rr(find(ppCR(:,2)==max(ppCR(:,2))));
% % 
% % % draw arrows
% % %h = DrawArrow([meanP meanP],[arrowYlo arrowYhi],'^','b',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanH meanH],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanM meanM],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-.');
% % %h = DrawArrow([meanA meanA],[arrowYlo arrowYhi],'^','k',arrowwidth,arrowheadweight,'-');
% % h = DrawArrow([meanFA meanFA],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-.');
% % h = DrawArrow([meanCR meanCR],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-');
% % 
% % axis square;
% % myFontSize = 32;
% % set(gca,'FontSize',myFontSize);
% % set(gca,'XTick',[-4 -2 0 2 4],'XTickLabel',[{-4},{-2},{0},{2},{4},{5}]);


%%

%%
% values taken from the results of script1_fitModel_variableSNRdata.m

% % phit = sum(bH)/sum(bH|bM|bFA|bCR);
% % for i=1:9
% %     [scci(i),ns(i),cp0(i),cp1(i)] = compStatsVsDF(DFhat1(i),MFhat1(i),phit,crit(i));
% % end
% % 
% % 
% % %%
% % 
% % %**********************************************************************
% % % ******* 6-Panel Figure **********************************************
% % %**********************************************************************
% % % values taken from the results of script1_fitModel_variableSNRdata.m
% % 
% % ourPhit = sum(bH)/sum(bH|bM|bFA|bCR);   % = 0.3366
% % ourCrit = crit(1);                      % = 0.0802
% % ourDF = DFhat1(1);                      % = 0.7724
% % ourMF = 1;                              % = 1.0000
% % DFs = DFhat1(1);                        % = 0.7724
% % f = Create6PanelFigure(ourPhit,ourCrit,ourDF,ourMF,DFs);




% %**********************************************************************
% % ******* New Fig. 3 **************************************************
% %**********************************************************************
% VarTD = meanVarTD(1);
% Crit = ourCrit;
% N = 500;
% % CreateFig3Panel(1,1,VarTD,Crit,N,10e-4);    % decision variable
% CreateFig3Panel(.85,1,VarTD,Crit,N,10e-4);  % our solution
% CreateFig3Panel(.55,1,VarTD,Crit,N,10e-4);  % crossing point
% CreateFig3Panel(.05,1,VarTD,Crit,N,25e-4);  % DF = 5%
% CreateFig3Panel(.85,.40,VarTD,Crit,N,5e-4);  % our solution plus noise
% 
% ourPhit = sum(bH)/sum(bH|bM|bFA|bCR);
% ourCrit = crit(1);
% ourDF = DFhat1(1);
% ourMF = 1;
% DFs = DFhat1(1);
% f = CreateFig3Inset(ourPhit,ourCrit,ourDF,ourMF,DFs);



%**********************************************************************
% ******* New Fig. 4 **************************************************
%**********************************************************************
%% MIGRATED TO c0_SDTanalytical

% % % calculate total decision variability using behavior
% % phit = sum(bH)/sum(bP);
% % pmiss = sum(bM)/sum(bP);
% % pfa = sum(bFA)/sum(bA);
% % pcr = sum(bCR)/sum(bA);
% % dprime = norminv(phit) - norminv(pfa);
% % VarTD = 1/dprime;
% % crit = norminv(pcr,0,VarTD);
% % 
% % % % debug
% % % xvals = [-2:.01:3];
% % % absent = normpdf(xvals,0,VarTD);
% % % present = normpdf(xvals,1,VarTD);
% % % f = figure; hold on; grid on;
% % % plot(xvals,absent,'r-');
% % % plot(xvals,present,'g');
% % % h = line([crit crit],[0 0.7]);
% % 
% % f = figure; set(f,'Color','w'); hold on; axis off;
% % ha = tight_subplot(2, 4, [.02 .02], [.05 .01], .02);
% % handles = reshape(ha,4,2)';
% % N = 500;
% % %CreateFig4Panel(1,1,VarTD,Crit,N,10e-4,ll,size);    % decision variable
% % CreateFig4Panel(.77,1,VarTD,crit,N,10e-4,handles(:,1));  % our solution
% % CreateFig4Panel(.59,1,VarTD,crit,N,10e-4,handles(:,2));  % crossing point
% % CreateFig4Panel(.05,1,VarTD,crit,N,25e-4,handles(:,3));  % DF = 5%
% % CreateFig4Panel(.77,.40,VarTD,crit,N,5e-4,handles(:,4));  % our solution plus noise
% % 
% % % get values for reporting
% % [scci,ns,cp0,cp1] = compStatsVsDF([.77 .59 .05],1,phit,crit);
% % [scci,ns,cp0,cp1] = compStatsVsDF(.77,.4,phit,crit);
% % 
% % 
% % f = figure; set(f,'Color','w'); hold on; axis off;
% % ha = tight_subplot(2, 4, [.02 .02], [.05 .01], .02);
% % handles = reshape(ha,4,2)';
% % df = 0.77;
% % mf = 0.03;
% % CreateFig4Inset(phit,crit,df,mf,.77);


