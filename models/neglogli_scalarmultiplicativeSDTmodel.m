function neglogli = neglogli_scalarmultiplicativeSDTmodel(prs, Dat)
% neglogli = neglogli_scalarmultiplicativeSDTmodel(prs, Dat)
%
% Computes negative log-likelihood for choice model
% (Called by fmincon for ML estimation)
%
% INPUT:
%        prs [4x1] = [var_MeasIndep, var_DecisIndep, var_shared, crit]';
%        Dat [Nx4] = [Stim, Measurement, Decision, Mwts]
%
% OUTPUT:
%        neglogli - negative loglikelihood of data given parameters
%
% Updated 14 Oct 2020 (morais)
%         10 Feb 2014 (JW Pillow)


vm = prs(1);
vd = prs(2);
vs = prs(3);
tau = prs(4);

X = Dat(:, 1); % mean of each trial data
Rm = Dat(:, 2); % measured response
D =  Dat(:, 3); % choice
kappa = Dat(:, 4);

vtm = vm + kappa.^2.*X.^2.*vs;
pRm_X = normpdf(Rm, kappa.*X, sqrt(vtm));
alph = X.^2.*vs ./ vtm;
vtd = vd + X.^2.*vs;
pRd_RmX = normcdf(X + kappa.*alph.*(Rm - kappa.*X), tau, ...
    sqrt(vtd - kappa.^2.*alph.^2.*vtm));
pRd_RmX(D==0) = 1 - pRd_RmX(D==0);  % take 1- this number for 0 responses.


ptrials = pRm_X.* pRd_RmX;
 
EPS = 1e-15; % tiny scalar to keep probability away from 0 & 1
ptrials = max(ptrials, EPS);

% Compute negative log-likelihood
neglogli = -sum(log(ptrials));