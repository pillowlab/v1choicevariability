setDirs;

load([WriteDir 'dat_onlyAmpsRenorm_MM190913.mat']);

%% Fixed parameters of the data & likelihood model
% Caps to indicate static properties of the data (not subject to
% optimization)

PC = 0.76; % percent correct
DF = 0.96; % measurement fraction of signal-measurement noise
MF = 0.06; % decision fraction of signal-decision noise
AMP0 = 0;  % Target absent mean amplitude
AMP1 = 1;  % Target present mean amplitude
    % AMP0/1 are arbitrary, they just set the scale for the other params
ntrials = 1000; % Half will be target-present, half target-absent

% Feature-to-match #1: Performance
phit = sum(bH)/sum(bP);
pmiss = sum(bM)/sum(bP);
pfa = sum(bFA)/sum(bA);
pcr = sum(bCR)/sum(bA);
PERFORMANCE = [phit pmiss pfa pcr];

% Feature-to-match #2: Spatial correlations
[height, width, ~] = size(AmplitudesRenormalized);
d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;
[xx,yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
DISTANCE = NaN(width*height,width*height);
xy = xy(:);
for i=1:width*height
    DISTANCE(:,i) = abs(xy - xy(i));
end
% Ground truth spatial correlation profile
[Distance1D, GTCorrelation1D_P] = sims.symmxcorr(AmplitudesRenormalized(:,:,bP), DISTANCE);
[~,          GTCorrelation1D_A] = sims.symmxcorr(AmplitudesRenormalized(:,:,bA), DISTANCE);
CORRELATION = [GTCorrelation1D_P GTCorrelation1D_A];

%% Fixed properties of the signal

% Pooling rule #4 (Gaussian with sigma = 1mm) (w)
poolingrule = normpdf(grid, 0, 1) ./ sum(normpdf(grid, 0, 1));
% vec
w = kron(poolingrule, poolingrule)';

% Signal ON/OFF responses (X)
sig = 3;
onvec = mvnpdf([xx(:) yy(:)],[0 0],diag([sig sig].^2));
on = reshape(onvec,size(xx));
scale = sum(onvec.*w);
rP = AMP1.*(on./scale); % on response
rA = AMP0.*(on./scale); % off response
X = cat(3, repmat(rP,[1 1 ntrials/2]), repmat(rA,[1 1 ntrials/2]));

%% Learned properties of the signal

% Initialize
lambda = 1;
SEED = randi(100000);
% RBF covariance kernel for spatially correlated noise
K = @(logrho, logell) exp(logrho) * exp(-(DISTANCE ./ (exp(logell)^2)));
% Ground truth values, used in optimization
args = struct('w', w, ...
            'seed', SEED, ...
            'lambda', lambda, ...
            'dists', DISTANCE, ...
            'perfs', PERFORMANCE, ...
            'corrs', CORRELATION, ...
            'MF', MF, ...
            'DF', DF, ...
            'kernelfn', K);
        
% FOR REFERENCE: theta0 = [logrho logell tau];
theta0 = [0 0 0.5];

% f = @(theta) additive(theta, X, args);
% [thetaopt,fval,exitflag,output] = fminunc(f, params0);
% [logrho, logell, tau] = deal(thetaopt(1), thetaopt(2), thetaopt(3));

%% dev
theta = theta0;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Unpack
[logrho, logell, tau] = deal(theta(1), theta(2), theta(3));
% rho > 0           EXP (total measurement noise)
% ell > 0           EXP (spatial lengthscale)
% tau               1 (threshold)
[width, height, ntrials] = size(X);
rng(args.seed);
w = args.w;
Kzz = args.kernelfn(logrho, logell);
[MF, DF] = deal(arfs.MF, args.DF);

Xvec = reshape(ipermute(X, [3,1,2]), ntrials, []);
Sigma_tm = Kzz;
Nvec_tm = mvnrnd(zeros(size(DISTANCE,1),1), Sigma_tm, ntrials);
% N = permute(reshape(N, [NTRIALS width height]), [3 2 1]);

% Simulate VSDI activity
Rvec_m = Xvec + Nvec_tm;
Rvec_s = Xvec + sqrt(MF).*Nvec_tm;
DV = Rvec_s * args.w;
sigma_d = (1/DF - 1) * MF * (w' * Kzz * w);
n_d = mvnrnd(0, sigma_d, ntrials);
r_d = DV + n_d;

% Simulate decisions and compute performances
bPs  = ((1:ntrials)' <= ntrials/2); % by design
bAs  = ((1:ntrials)' >  ntrials/2);
bHs  = (r_d >= tau) & bPs; % over thresh & target present = HIT
bFAs = (r_d >= tau) & bAs; % over thresh & target absent = FA
bMs  = (r_d <  tau) & bPs; % under thresh & target present = MISS
bCRs = (r_d <  tau) & bAs; % under thresh & target absent = CR
sim_perfs = [sum(bHs)/sum(bPs), sum(bMs)/sum(bPs), ...
             sum(bFAs)/sum(bAs), sum(bCRs)/sum(bAs)];
% Compute spatial correlations
[~, corrs_P] = sims.symmxcorr(Rvec_m(:,:,1:(ntrials/2)), args.dists);
[~, corrs_A] = sims.symmxcorr(Rvec_m(:,:,(ntrials/2+1):end), args.dists);
sim_corrs = [corrs_P corrs_A];

% Compute "loss":
% Compute MSE correlation loss from the true data spatial correlations
xcorr_loss = sum(sum((args.corrs - sim_corrs).^2))/numel(sim_corrs);
% Compute MSE performance loss from true data percent correct
performance_loss = sum((args.perfs - sim_perfs).^2)/numel(sim_perfs);
% Loss for performance-xcorr L2-constrained optimization program
loss = performance_loss + args.lambda * xcorr_loss;
