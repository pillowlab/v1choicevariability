function neglogli = NLL_scalarSDTmodel(prs, Dat)
% neglogli = NLL_scalarSDTmodel(prs, Dat)
%
% Computes negative log-likelihood for choice model
% (Called by fmincon for ML estimation)
%
% INPUT:
%        prs [4x1] = [var_MeasIndep, var_DecisIndep, var_shared, crit]';
%        Dat [Nx4] = [Stim, Measurement, Decision,Mwts]
%
% OUTPUT:
%        neglogli - negative loglikelihood of data given parameters
%
% Updated 10 Feb 2014 (JW Pillow)
%         30 May 2020 (MJ Morais)


vmi = prs(1);
vdi = prs(2);
vsh = prs(3);
crit = prs(4);

mu = Dat(:,1); % mean of each trial data
Rm = Dat(:,2); % measured response
D =  Dat(:,3); % choice
wt = Dat(:,4);

pRm_X = normpdf(Rm,wt.*mu,sqrt(vmi + wt.^2.*vsh));%./sqrt(wt.^2.*vsh+vmi);
pRd_RmX = normcdf(mu+(wt.*vsh)./(vmi+wt.^2.*vsh).*(Rm-wt.*mu),crit,...
    sqrt(vdi+vsh-wt.^2.*vsh^2./(vmi+wt.^2.*vsh)));
pRd_RmX(D==0)=1-pRd_RmX(D==0);  % take 1- this number for 0 responses.


ptrials = pRm_X.*pRd_RmX;
 
EPS = 1e-15; % tiny scalar to keep probability away from 0 & 1
ptrials = max(ptrials,EPS);

% Compute negative log-likelihood
neglogli = -sum(log(ptrials));