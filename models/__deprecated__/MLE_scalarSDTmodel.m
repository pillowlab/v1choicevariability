function [prsHat, SD, exitflag, neglogli] = MLE_scalarSDTmodel(trialData, prsInit)
% [prsHat,SD,exitflag,neglogli] = compMLest_VSDIvarSNRdata(trialData,prsInit)
% 
% Compute ML estimate for simple signal detection
% theory experiment with variable contrast, variable SNR data
%
%
% INPUT:
%  trialData [N x 4] - raw data: [X M D Mwts]
%                    X  - assumed mean response to each stimulus (determined by contrast)
%                    M  - measurement
%                    D  - decision
%                    Mwts - weights for measurement that day
%  prsInit [4 x 1] - initial guess at parameters: 
%                       varMi - stdev of indep noise in measured signal
%                       varDi - stdev of indep noise in monkey's decision variable
%                       varSh - stdev of shared noise
%                       crit - monkey's criterion
%
% OUTPUT:
%      prsHat [4 x 1] - maximum Likelihood estimate of 4 parameters above
%          SD [4 x 1] - 1SD error bars for all params 
%    exitflag [1 x 1] - from fminunc
%    neglogli [1 x 1] - value of negative logli at maximum 
%
% Updated 28 Aug 2014 (JW Pillow)

% === 1. Initialize parameter estimates ===============================
if nargin ==1 % set initial params
    prsInit = initializeSDTmodelprs(trialData);
end

% === 2. Call fmincon to optimize loglikelihood for all params ========
lossfun = @(prs)(neglogli_SDTmodel_variableSNRdata(prs,trialData));
lb = [0;0;0;0]+1e-6;  % lower bounds
ub = [1000;1000;1000;1]; % upper bounds

% set options and optimize negative logli
opts = optimset('display', 'off', 'algorithm', 'active-set','maxfunevals',1e6,'maxiter',1e6);
[prsEstim,neglogli,exitflag,~,~,~,Hess] = fmincon(lossfun,prsInit,[],[],[],[],lb,ub,[],opts);

% === 3. place estimated params into struct ============================
prsHat.varmi = prsEstim(1);
prsHat.vardi = prsEstim(2);
prsHat.varsh = prsEstim(3);
prsHat.crit = prsEstim(4);

if nargout > 1
    % compute posterior standard deviation on each param ("error bars").
    sigmapost = sqrt(diag(inv(Hess)));
    SD = sigmapost;
end


