setDirs;

load([WriteDir 'AmpsRenorm_MM190913.mat']);

% PARAMETERS
PERFORMANCE = 0.76; % Observed in data (combined; Supp. Table 1)
phit = sum(bH)/sum(bP);
pmiss = sum(bM)/sum(bP);
pfa = sum(bFA)/sum(bA);
pcr = sum(bCR)/sum(bA);
PERFORMANCES = [phit pmiss pfa pcr];

AMP0 = 0; % Target absent mean amplitude
AMP1 = 1; % Target present mean amplitude
THRESH = 0.5; % Fixed threshold
    % AMP0/1 and THRESH are arbitrary, and fixed at simple values such that
    % the relevant noise parameters are anchored to these values.
NTRIALS = 1000; % Half will be target-present, half target-absent

% DISTANCES
height  = size(AmplitudesRenormalized,1);
width   = size(AmplitudesRenormalized,2);
d       = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;
[xx,yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
Distance = NaN(width*height,width*height);
xy = xy(:);
for i=1:width*height
    Distance(:,i) = abs(xy - xy(i));
end
% GET TRUE SPATIAL CORRELATION PROFILE (depending on the mechanism,
% present-vs-absent spatial correlations can be different)
[Distance1D, GTCorrelation1D_P] = sims.symmxcorr(AmplitudesRenormalized(:,:,bP), Distance);
[~,          GTCorrelation1D_A] = sims.symmxcorr(AmplitudesRenormalized(:,:,bA), Distance);
GTCORRELATION = [GTCorrelation1D_P GTCorrelation1D_A];

% SIMULATED SIGNAL (only the noise varies in simulation)
sig = 3;
G = reshape(mvnpdf([xx(:) yy(:)],[0 0],diag([sig sig].^2)),size(xx));
rP = AMP1.*(G./max(G(:)));
rA = AMP0.*(G./max(G(:)));
signal = cat(3, repmat(rP,[1 1 NTRIALS/2]), repmat(rA,[1 1 NTRIALS/2]));
decoder = zeros(17); decoder((9+(-2:2)),(9+(-2:2))) = 1/25;

% OPTIMIZE RBF-KERNEL GP TO REPLICATE SPATIAL STATISTICS AND PERFORMANCE
%       -- lengthscale determines spatial correlations
%       -- amplitude determines percent correct
% Translate distances into a RBF covariance kernel for additive noise
K = @(logx) exp(-(Distance ./ (exp(logx)^2)));

% Define auxiliary parameters
lambda = 1;
SEED = randi(100000);
[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;

%% ADDITIVE NOISE

close all;
% params0 = [logell logrho tau]; % FOR REFERENCE
params0 = [0 0 0.5];
% Optimize
f = @(x) sims.loss_additive(x, signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
[xopt,fval,exitflag,output] = fminunc(f, params0);
[logell, logrho, tau] = deal(xopt(1), xopt(2), xopt(3));

rng(SEED);
noise = mvnrnd(zeros(size(Distance,1),1), K(logell), NTRIALS);
noise = permute(reshape(noise, [NTRIALS width height]), [3 2 1]);
AmplitudesSimulated = signal + exp(logrho) .* noise;
[~, Correlation1D_testP] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), Distance);
[~, Correlation1D_testA] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), Distance);
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, thresh);
Phat = [sum(bHs)/sum(bPs) sum(bMs)/sum(bPs) sum(bFAs)/sum(bAs) sum(bCRs)/sum(bAs)];

plots.simulatedmaps;
figh = findobj('type','figure'); set(figh, 'renderer', 'painters');
% for k = 1:numel(figh)
%     figure(figh(k));
%     print(figh(k),sprintf('../manuscript/figs/panels/f9_1_%d',k),'-depsc')
% end

%% MULTIPLICATIVE GAIN

close all;
% params0 = [logell logrho tau]; % FOR REFERENCE
params0 = [0 0];
% Optimize
f = @(x) sims.loss_multiplicative(x, signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
[xopt,fval,exitflag,output] = fminunc(f, params0);
[logk, tau] = deal(xopt(1), xopt(2));

rng(SEED);
gain = gamrnd(exp(logk), 1/exp(logk), 1,1,NTRIALS);
AmplitudesSimulated = bsxfun(@times, gain, signal);
[~, Correlation1D_testP] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), Distance);
[~, Correlation1D_testA] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), Distance);
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, thresh);
Phat = [sum(bHs)/sum(bPs) sum(bMs)/sum(bPs) sum(bFAs)/sum(bAs) sum(bCRs)/sum(bAs)];

plots.simulatedmaps;
figh = findobj('type','figure'); set(figh, 'renderer', 'painters');
for k = 1:numel(figh)
    figure(figh(k));
    print(figh(k),sprintf('../manuscript/figs/panels/f9_2_%d',k),'-depsc')
end

%% ADDITIVE NOISE + SIGNAL-MULTIPLICATIVE GAIN

close all;
% params0 = [logell logrho logk tau]; % FOR REFERENCE
params0 = [0 0 0 0];
% Define auxiliary parameters
MULTTYPE = 'signalonly';
% Optimize
f = @(x) sims.loss_full(x, signal, decoder, SEED, MULTTYPE, lambda, Distance, PERFORMANCES, GTCORRELATION);
[xopt,fval,exitflag,output] = fminunc(f, params0);
[logell, logrho, logk, tau] = deal(xopt(1), xopt(2), xopt(3), xopt(4));

rng(SEED);
gain = gamrnd(exp(logk), 1/exp(logk), 1,1,NTRIALS);
rng(SEED);
noise = mvnrnd(zeros(size(Distance,1),1), K(logell), NTRIALS);
noise = permute(reshape(noise, [NTRIALS width height]), [3 2 1]);
AmplitudesSimulated = bsxfun(@times, gain, signal) + exp(logrho) .* noise;
[~, Correlation1D_testP] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), Distance);
[~, Correlation1D_testA] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), Distance);
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, thresh);
Phat = [sum(bHs)/sum(bPs) sum(bMs)/sum(bPs) sum(bFAs)/sum(bAs) sum(bCRs)/sum(bAs)];

plots.simulatedmaps;
figh = findobj('type','figure'); set(figh, 'renderer', 'painters');
for k = 1:numel(figh)
    figure(figh(k));
    print(figh(k),sprintf('../manuscript/figs/panels/f9_3_%d',k),'-depsc')
end

% loss with partial parameter holds
fprintf('Loss for mixed-parameter model: %.4f\n', fval)
gainonly = sims.loss_multiplicative([xopt(3), xopt(4)], signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
fprintf('Loss for optimal gain only (noise fixed to 0): %.4f\n', gainonly)
noiseonly = sims.loss_additive([xopt(1), xopt(2), xopt(4)], signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
fprintf('Loss for optimal noise only (gain fixed to 1): %.4f\n', noiseonly)

%% ADDITIVE NOISE + SIGNALNOISE-MULTIPLICATIVE GAIN

close all;
% params0 = [logell logrho logk tau]; % FOR REFERENCE
params0 = [0 0.2 0 0];
% Define auxiliary parameters
MULTTYPE = 'signalnoise';
% Optimize
f = @(x) sims.loss_full(x, signal, decoder, SEED, MULTTYPE, lambda, Distance, PERFORMANCES, GTCORRELATION);
[xopt,fval,exitflag,output] = fminunc(f, params0);
[logell, logrho, logk, tau] = deal(xopt(1), xopt(2), xopt(3), xopt(4));

rng(SEED);
gain = gamrnd(exp(logk), 1/exp(logk), 1,1,NTRIALS);
rng(SEED);
noise = mvnrnd(zeros(size(Distance,1),1), K(logell), NTRIALS);
noise = permute(reshape(noise, [NTRIALS width height]), [3 2 1]);
AmplitudesSimulated = bsxfun(@times, gain, signal + exp(logrho) .* noise);
[~, Correlation1D_testP] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), Distance);
[~, Correlation1D_testA] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), Distance);
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, thresh);
Phat = [sum(bHs)/sum(bPs) sum(bMs)/sum(bPs) sum(bFAs)/sum(bAs) sum(bCRs)/sum(bAs)];

plots.simulatedmaps;
figh = findobj('type','figure'); set(figh, 'renderer', 'painters');
for k = 1:numel(figh)
    figure(figh(k));
    print(figh(k),sprintf('../manuscript/figs/panels/f9_4_%d',k),'-depsc')
end

% loss with partial parameter holds
fprintf('Loss for mixed-parameter model: %.4f\n', fval)
gainonly = sims.loss_multiplicative([xopt(3), xopt(4)], signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
fprintf('Loss for optimal gain only (noise fixed to 0): %.4f\n', gainonly)
noiseonly = sims.loss_additive([xopt(1), xopt(2), xopt(4)], signal, decoder, SEED, lambda, Distance, PERFORMANCES, GTCORRELATION);
fprintf('Loss for optimal noise only (gain fixed to 1): %.4f\n', noiseonly)

