function loss = multiplicative(x, X, DECODER, SEED, LAMBDA, DISTANCE, PERFORMANCES, GTCORRELATION)

[width, height, NTRIALS] = size(X);
[logrho, logell, tau] = deal(x(1), x(2), x(3));
% rho > 0           EXP (total measurement noise)
% ell > 0           EXP (spatial lengthscale)
% tau               1 (threshold)

% Simulate noise constant over iterations of fmin (fixed seed)
%   Additive noise
rng(SEED);
K = @(logrho, logell) exp(logrho) * exp(-(DISTANCE ./ (exp(logell)^2)));

G = mvnrnd(zeros(size(DISTANCE,1),1), K(logell), NTRIALS);
G = permute(reshape(N, [NTRIALS width height]), [3 2 1]);

% Simulate VSDI activity
R_m = X * G;

% Simulate behavior
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(R_m, DECODER, thresh);
% Compute performance measures
phit = sum(bHs)/sum(bPs);
pmiss = sum(bMs)/sum(bPs);
pfa = sum(bFAs)/sum(bAs);
pcr = sum(bCRs)/sum(bAs);
Phat = [phit pmiss pfa pcr];

% Measure spatial correlations
[~, Correlation1D_P] = sims.symmxcorr(R_m(:,:,1:(NTRIALS/2)), DISTANCE);
[~, Correlation1D_A] = sims.symmxcorr(R_m(:,:,(NTRIALS/2+1):end), DISTANCE);
Correlation1D = [Correlation1D_P Correlation1D_A];
% Compute MSE correlation loss from the true data spatial correlations
xcorr_loss = sum(sum((GTCORRELATION - Correlation1D).^2))/numel(GTCORRELATION);

% Compute MSE performance loss from true data percent correct
performance_loss = sum((PERFORMANCES - Phat).^2)/numel(PERFORMANCES);

% Loss for performance-xcorr L2-constrained optimization program
loss = performance_loss + LAMBDA * xcorr_loss;

end

