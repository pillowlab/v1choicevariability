function prs0 = initializeSDTmodelprs_matrix(trialData, mapData, args)
% prs0 = initializeSDTmodelprs(Dat)
%
% Use grid search to determine good initial setting for VSDI signal-detection model
% parameters.
%
% Estimates initial setting of parameters:
%    varmi - indep measurement noise
%    vardi - indep decision noise
%    varsh - shared sensory noise
%    crit0 - criterion

ntrials = size(trialData,1);
iix1 = (trialData(:,1)>0);
iix0 = (trialData(:,1)==0);
iid1 = (trialData(:,3)==1);
iid0 = (trialData(:,3)==0);
keyboard;

%% Compute total measurement variance
nx1 = sum(iix1); 
nx0 = sum(iix0);

covtm = (nx1.*nancov(mapData(iix1,:)) + nx0.*nancov(mapData(iix0,:)))./ntrials;
logrho = log(trace(covtm) ./ size(covtm,1));
logell = log(sum(sum(-args.dists(tril(true(size(covtm)), -1)) ./ ...
    log(covtm(tril(true(size(covtm)), -1))./exp(logrho)))) ./ ...
    nchoosek(size(covtm,1), 2));

loss = @(logprs) norm(covtm - kernel(logprs(1), logprs(2), args.dists));
kernprs = fminunc(loss,[-5,1]);
[logrho, logell] = deal(kernprs(1), kernprs(2));

%% Determine criterion & total decision noise
crit0 = 0.1:.01:.8;  % possible values for criterion
pooled = mapData * args.pool;

p1_x1 = sum(iix1&iid1)/nx1;
p1_x0 = sum(iix0&iid1)/nx0;
sigtd_x1 = (1-crit0)/norminv(p1_x1); % from hits
sigtd_x0 = (0-crit0)/norminv(p1_x0); % from FAs

[~,icrit] = min(abs(sigtd_x1-sigtd_x0));  % pick crit where estimated sigtds are equal
crit0 = crit0(icrit);  % set initial guess at criterion
vartd0 = sigtd_x1(icrit).^2; % total decision noise variance

%% set initial guess at shared noise var
maxsh = max(0.1,min(vartm0,vartd0));
nvals = 10;
dfrac = maxsh/nvals;
ff = dfrac/2:dfrac:maxsh;
negll = zeros(nvals,1);
for jj = 1:nvals
    vsh = ff(jj);
    negll(jj) = neglogli_SDTmodel_variableSNRdata([vartm0-vsh;vartd0-vsh;vsh;crit0],trialData);
end
[~,jjmin] = min(negll);
vsh0 = ff(jjmin);
vmi0 = vartm0-vsh0;
vdi0 = vartd0-vsh0;

% initial parameters guess
prs0 = [vmi0; vdi0; vsh0; crit0];

function K = kernel(logrho, logell, dists)
    K = exp(logrho) * exp(-(dists.^2 ./ (2 * exp(logell)^2)));