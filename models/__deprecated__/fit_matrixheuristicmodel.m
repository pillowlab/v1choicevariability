% renormalize maps based on Jonathan's analysis and project into scalars


setDirs;

%**************************************************************************
%******* load Preprocessed data *******************************************
%**************************************************************************
load([ReadDir 'Preprocessed.mat']);
load([ReadDir 'Inclusion.mat']);


%**************************************************************************
%******* set parameters for analysis **************************************
%**************************************************************************
sParameters = Parameters();


% %**************************************************************************
% %******* check data from Jonathan *****************************************
% %**************************************************************************
% load([ReadDir 'Unbiased Scalars from dropbox/dat_NewInclusion_pruned_Dec2013.mat']);
% load([ReadDir 'Unbiased Scalars from dropbox/RenormalizedScalars_May2014.mat']);
% load([ReadDir 'Unbiased Scalars from dropbox/dat_NewInclusion.mat']);
% load([ReadDir 'Unbiased Scalars from dropbox/dat_NewInclusion_processed.mat']);



%**************************************************************************
%******* reduce data ******************************************************
%**************************************************************************
load([WriteDir 'dat_MM190805.mat']);
Amplitudes = Data.Amplitudes(:,:,bInclude);
SPTimeCourse = Data.SPTimeCourse(:,bInclude);
RT = Data.RT(bInclude);
clear dat;
clear bInclude;

% Second reduction in trials: 10163 --> 10153 --> 9979, OK!
load([WriteDir 'dat_MM190805_processed.mat']);
Amplitudes = Amplitudes(:,:,bInclude);
SPTimeCourse = SPTimeCourse(:,bInclude);
RT = RT(bInclude);

Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

bInclude = bInclude(bInclude);

%%%%%% Chuck debug check
CurrentList = unique([dat.date dat.monkey],'rows')
numTex = size(CurrentList(CurrentList(:,end)==1,:),1)
numCharlie = size(CurrentList(CurrentList(:,end)==2,:),1)
%%%%%%

%**************************************************************************
%******* remove high contrast trials and choose monkey(s) *****************
%**************************************************************************
% bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
% bMyMonkey = dat.monkey == 2;% | dat.monkey == 2;
% bExample = dat.datenum == 6;
% bInclude = bMyMonkey & ~bIsInHighContrastBlock;

bP = dat.bP & bInclude;
bA = dat.bA & bInclude;
bH = dat.bH & bInclude;
bM = dat.bM & bInclude;
bFA = dat.bFA & bInclude;
bCR = dat.bCR & bInclude;

%**************************************************************************
%******* Compute Maps *****************************************************
%**************************************************************************
PresentMap = ComputeWeightedMeanMap(Amplitudes,Weights,bP);
AbsentMap = ComputeWeightedMeanMap(Amplitudes,Weights,bA);
HitMap = ComputeWeightedMeanMap(Amplitudes,Weights,bH);
MissMap = ComputeWeightedMeanMap(Amplitudes,Weights,bM);
FalseAlarmMap = ComputeWeightedMeanMap(Amplitudes,Weights,bFA);
CorrectRejectMap = ComputeWeightedMeanMap(Amplitudes,Weights,bCR);
HitVMissMap = HitMap - MissMap;
FalseAlarmVCorrectRejectMap = FalseAlarmMap - CorrectRejectMap;


%**********************************************************************
%******* Choice-Triggered Map *****************************************
%**********************************************************************
AmplitudesStimulusRemoved = Amplitudes;
AmplitudesStimulusRemoved(:,:,bP) = AmplitudesStimulusRemoved(:,:,bP) - repmat(PresentMap,[1 1 sum(bP)]);
GoMap = nanmean(AmplitudesStimulusRemoved(:,:,(bH|bFA)),3);
NoGoMap = nanmean(AmplitudesStimulusRemoved(:,:,(bM|bCR)),3);
ChoiceTriggeredMap = GoMap - NoGoMap;

%**********************************************************************
%******* Fit Some Maps ************************************************
%**********************************************************************
Coordinates.X = [1:size(Amplitudes,1)];
Coordinates.Y = [1:size(Amplitudes,2)];

% present map
[FitValuesPresentMap, ~] = FitGaussian2D(Coordinates,PresentMap);
FittedPresentMap = FuncGaussian2D(FitValuesPresentMap, Coordinates);

% f = figure;
% subplot(1,2,1); imagesc(PresentMap'); axis square;
% subplot(1,2,2); imagesc(FittedPresentMap'); axis square;

% choice-triggered map
[FitValuesChoiceTriggeredMap, ~] = FitGaussian2D(Coordinates,ChoiceTriggeredMap);
FittedChoiceTriggeredMap = FuncGaussian2D(FitValuesChoiceTriggeredMap, Coordinates);

% f = figure;
% subplot(1,2,1); imagesc(ChoiceTriggeredMap'); axis square;
% subplot(1,2,2); imagesc(FittedChoiceTriggeredMap'); axis square;

%**********************************************************************
%******* Compute Pooled Scalars for the different models **************
%**********************************************************************

CtrX = sParameters.ROIhalfsize + 1;
CtrY = sParameters.ROIhalfsize + 1;

%**********************************************************************
% ***** Model 1. Weighted choice-triggered average
[ScalarAmplitudesByModel(1,:), PoolingMapsByModel(:,:,1), ~] = PoolingModel_B_Weights(Amplitudes,FittedChoiceTriggeredMap,bP,bA);

%**********************************************************************
% ***** Model 2. Weighted average amplitude
[ScalarAmplitudesByModel(2,:), PoolingMapsByModel(:,:,2), ~] = PoolingModel_B_Weights(Amplitudes,FittedPresentMap,bP,bA);

%**********************************************************************
% ***** Models 3-7. Gaussians
[ScalarAmplitudesByModel(3,:), PoolingMapsByModel(:,:,3), ~] = PoolingModel_B_Weights(Amplitudes,FuncGaussian2D([100 CtrX CtrY 0 0 1.0 1.0], Coordinates),bP,bA);
[ScalarAmplitudesByModel(4,:), PoolingMapsByModel(:,:,4), ~] = PoolingModel_B_Weights(Amplitudes,FuncGaussian2D([100 CtrX CtrY 0 0 2.0 2.0], Coordinates),bP,bA);
[ScalarAmplitudesByModel(5,:), PoolingMapsByModel(:,:,5), ~] = PoolingModel_B_Weights(Amplitudes,FuncGaussian2D([100 CtrX CtrY 0 0 4.0 4.0], Coordinates),bP,bA);
[ScalarAmplitudesByModel(6,:), PoolingMapsByModel(:,:,6), ~] = PoolingModel_B_Weights(Amplitudes,FuncGaussian2D([100 CtrX CtrY 0 0 6.0 6.0], Coordinates),bP,bA);
[ScalarAmplitudesByModel(7,:), PoolingMapsByModel(:,:,7), ~] = PoolingModel_B_Weights(Amplitudes,FuncGaussian2D([100 CtrX CtrY 0 0 8.0 8.0], Coordinates),bP,bA);

%**********************************************************************
% ***** Models 8-9. Optimals
% use the latest version of the optimal decoder!!
[ScalarAmplitudesByModel(8,:), PoolingMapsByModel(:,:,8)] = PoolingModel_7_Optimal2D(Amplitudes,bP,bA);
[ScalarAmplitudesByModel(9,:), PoolingMapsByModel(:,:,9)] = PoolingModel_7_Optimal2D(Amplitudes,(bH|bFA),(bM|bCR));
TemplateOptimal = PoolingMapsByModel(:,:,8);

% % clean up
% ScalarAmplitudesByModel(isinf(ScalarAmplitudesByModel)) = NaN;
% AmpsByModel = ScalarAmplitudesByModel;
% nModels = size(ScalarAmplitudesByModel,1);
% bNaN = any(isnan(ScalarAmplitudesByModel));

% % correlation between readout models
% for i=1:nModels
%     for j=1:nModels
%         [R,P] = corrcoef(ScalarAmplitudesByModel(i,~bNaN),ScalarAmplitudesByModel(j,~bNaN));
%         Corr(i,j) = R(2,1);
%     end
% end
% % Corr

% save([WriteDir 'AmpsByModel.mat'],'dat', 'AmpsByModel');

% Heuristic model atop it, using values from the scalar model
%% STAGE 1: Spatial measurement noise

[height, width, N] = size(Amplitudes);
d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;

% Vectorize the amplitudes, compute empirical covariance matrix of
% target-present-subtracted signals
Rvec = reshape(Amplitudes, 17^2, []);
Rvec_zeromean = Rvec; 
Rvec_zeromean(:,bP) = bsxfun(@minus, Rvec_zeromean(:,bP), reshape(FittedPresentMap, 17^2, 1));
Sigmahat_tm = nancov(Rvec_zeromean');
% Get distances from pixels to pixels
[xx, yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
dists = NaN(width*height,width*height);
xy = reshape(xy, height*width, []);
for i=1:width*height
    for j=1:width*height
        dists(i,j) = abs(xy(i) - xy(j));
    end
end

% Fit kernel covariance to Sigmahat_tm (the likelihood-based optimization
% is degenerate, so we're going to qualititatively optimize each object)
RBF = @(logrho, logell) 1e-8*eye(17^2) + ...
    exp(logrho) .* exp(-(dists./exp(logell)).^2 ./ 2); % dists = pairwise distances
Exponential = @(logrho, logell) 1e-8*eye(17^2) + ...
    exp(logrho) .* exp(-dists./exp(logell));
Matern32 = @(logrho, logell) 1e-8*eye(17^2) + ...
    exp(logrho) .* (1 + sqrt(3) .* dists./exp(logell)) .* exp(-sqrt(3) .* dists./exp(logell));
Matern52 = @(logrho, logell) 1e-8*eye(17^2) + ...
    exp(logrho) .* (1 + sqrt(5) .* dists./exp(logell) + (5/3) .* (dists./exp(logell)).^2) .* exp(-sqrt(5) .* dists./exp(logell));
additive_kernel_loss = @(prs, K) norm(Sigmahat_tm - K(prs(1), prs(2)), 'fro');
% multiplicative_kernel_loss = @(prs, K) norm(Sigmahat_tm - K(prs(1), prs(2)), 'fro');

% Use Exponential kernel after various tests, the data have an extremely
% broad spatial profile
thiskernel = Exponential;
prs0 = [log(max(diag(Sigmahat_tm))), 0];
[x,fval,exitflag,output,grad,hessian] = fminunc(@(prs) additive_kernel_loss(prs, thiskernel), prs0);
Kzz = thiskernel(x(1), x(2));

% [x,fval,exitflag,output,grad,hessian] = fminunc(@(prs) multiplicative_kernel_loss(prs, thiskernel), prs0);

%% STAGE 2: Decision noise

MF = [0.03, 0.03, 0.07, 0.06, 0.04, 0.03, 0.03, 0.05, 0.05];
DF = [0.77, 0.77, 0.97, 0.96, 0.86, 0.77, 0.72, 0.98, 0.99];
kappa = 1; % WLOG: These are simulated data!

rng(0);
% Simulate VSDI activity with matched spatial statistics
X = [repmat(reshape(FittedPresentMap, 17^2, 1)', sum(bP), 1); zeros(sum(bA), 17^2)];
Ns = mvnrnd(zeros(17^2, 1), Kzz, numel(bP));
Nm = mvnrnd(zeros(17^2, 1), Kzz, numel(bP));
nd = randn(numel(bP), 1);

% Order: [Hits Misses FAs CRs]
perfhat = [sum(dat.bH)/sum(dat.bP), sum(dat.bM)/sum(dat.bP),...
           sum(dat.bFA)/sum(dat.bA), sum(dat.bCR)/sum(dat.bA)];
perf_loss = @(dd) norm(perfhat - ...
          [sum(bP & dd)/sum(dat.bP), sum(bP & ~dd)/sum(dat.bP),...
           sum(bA & dd)/sum(dat.bA), sum(bA & ~dd)/sum(dat.bA)]);
for k = 1:size(PoolingMapsByModel, 3)
    Rm = kappa .* (X + sqrt(MF(k)) .* Ns) + sqrt(1-MF(k)) .* Nm;
    w_x_w = reshape(PoolingMapsByModel(:,:,k), 17^2, 1); % kron(w, w)
    sigma2_d = (1/DF(k) - 1) * MF(k) * (w_x_w' * Kzz * w_x_w);
    rd = ((X + sqrt(MF(k)) .* Ns) * w_x_w) + sqrt(sigma2_d) .* nd;

    prs0 = 0.5 * (mean(rd(1:sum(bP))) + mean(rd((sum(bP)+1):end))); % SDT optimal criterion
    [x,fval,exitflag,output,grad,hessian] = fminunc(@(t) perf_loss((rd - t > 0)), prs0);
    D = (rd - x) > 0;
    perf = [sum(bP & D)/sum(dat.bP), sum(bP & ~D)/sum(dat.bP),...
            sum(bA & D)/sum(dat.bA), sum(bA & ~D)/sum(dat.bA)];
end


% Vectorize Amps, subtract mean from target-present, compute covariance
% matrix, fit kernel hyperparameters; 
% Pass on MF to decision, fit threshold to generate behavior

% MEAN STIMULUS MAP: 1 x 289 (flattened from 17x17 image)
stimvec = reshape(stim, height*width, [])';
% VSDI DATA: 9979 x 289
trialMaps = reshape(AmpsNorm, height*width, [])';
% VSDI DATA with target-present response subtracted on target-present trials
noise = trialMaps; noise(bP,:) = bsxfun(@minus, noise(bP,:), stimvec);
% RBF kernel covariance
kernel = @(logrho, logell) 1e-8*eye(289) + exp(logrho) * exp(-(dists.^2 ./ (2 * exp(logell)^2))); % dists = pairwise distances

% Total data (negative-log) likelihood
lkl = @(hyp) -nansum(logmvnpdf(noise,zeros(size(noise(1,:))),kernel(hyp(1),hyp(2))));
% Minimize to optimize kernel hyperparameters to model the noise
hyp0 = [4,0]; % log(trace(cov(noise))/n) = 4
hyp = fminunc(lkl, hyp0);


