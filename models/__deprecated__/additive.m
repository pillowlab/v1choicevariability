function loss = additive(theta, X, args)

[logrho, logell, tau] = deal(theta(1), theta(2), theta(3));
% rho > 0           EXP (total measurement noise)
% ell > 0           EXP (spatial lengthscale)
% tau               1 (threshold)
[width, height, NTRIALS] = size(X);
rng(args.seed);
K = args.kernelfn;

% Simulate noise constant over iterations of fmin (fixed seed)
%   Additive noise
N = mvnrnd(zeros(size(DISTANCE,1),1), K(logell), NTRIALS);
N = permute(reshape(N, [NTRIALS width height]), [3 2 1]);

% Simulate VSDI activity
R_m = X + N;

% Simulate behavior
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, DECODER, thresh);
% Compute performance measures
phit = sum(bHs)/sum(bPs);
pmiss = sum(bMs)/sum(bPs);
pfa = sum(bFAs)/sum(bAs);
pcr = sum(bCRs)/sum(bAs);
Phat = [phit pmiss pfa pcr];

% Measure spatial correlations
[~, Correlation1D_P] = symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), DISTANCE);
[~, Correlation1D_A] = symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), DISTANCE);
Correlation1D = [Correlation1D_P Correlation1D_A];
% Compute MSE correlation loss from the true data spatial correlations
xcorr_loss = sum(sum((GTCORRELATION - Correlation1D).^2))/numel(GTCORRELATION);

% Compute MSE performance loss from true data percent correct
performance_loss = sum((PERFORMANCES - Phat).^2)/numel(PERFORMANCES);

% Loss for performance-xcorr L2-constrained optimization program
loss = performance_loss + LAMBDA * xcorr_loss;

end

