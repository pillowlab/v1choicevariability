% PREVIOUSLY: script3_inspectPredictedDists_on01.m
%
% Compare histograms and analytic quantities from fitted model

% ------------------------
% Load data 

a0_setDirs;

load([WriteDir 'dat_MM190805_pruned.mat']);
load([ReadDir 'AmpsByModel.mat']);
load([WriteDir 'ii25.mat']);

mVSDI = AmpsByModel';

% Load data
whichModel = 1; % Specify which (of 7) pooling models to use
bMonkey = (dat.monkey == 1 | dat.monkey == 2);  % specify which monkey to look at (1=Tex, 2=Charlie)
Mraw = mVSDI(:,whichModel);  % measurements
D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
contr = dat.contr;  % contrast of that trial
cs = unique(contr(contr>0)); % list of distinct contrasts
ncs = length(cs); % number of contrasts
nsess = max(dat.sessnums); % number of sessions

% Estimate the SNR for each experiment and contrast level
[M,Xeff,Mwts,Mmus,Mmupred,usess,ucontr] = findExptSNRs(Mraw,X,dat);
iinonnan = ~isnan(Mmus);

%% Load data and fit model params
trialData = [Xeff,M,D,Mwts]; % data used for fitting
trialData1 = trialData;
trialData1(ii25,:) = [];
sessnums = dat.sessnums;
sessnums(ii25) = [];

if 1
    % 1) Fit model to all low-contrast data
    prshat1 = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
else
    % 2) Fit only "originally included experiments"
    trialData1 = trialData;
    sessnums = dat.sessnums;
    iiNew = ~dat.bOrigInclusion;
    trialData1(iiNew,:) = [];
    sessnums(iiNew,:) = [];
    ii25b = trialData1(:,1)==1;
    trialData1(ii25b,:) = [];
    sessnums(ii25b,:) = [];
    prshat1 = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
end


%% (1) ======= Extract params from fit ============
mu0 = 0;  % mean response to no-stim (MUST BE ZERO)
varmi = prshat1.varmi;  % indep measurement noise
vardi = prshat1.vardi;  % indep decision noise
varsh = prshat1.varsh; % shared noise 
crit = prshat1.crit;  % criterion 

% params we'll need
sigdi = sqrt(vardi); 
sigsh = sqrt(varsh);
sigmi = sqrt(varmi); 
vartd = vardi+varsh;
sigtd = sqrt(vartd); 

% Get stuff we need (also sessnums)
Xeff = trialData1(:,1);
M = trialData1(:,2);
D = trialData1(:,3);
Mwts = trialData1(:,4);

% Plot stuff
linestr = {'-', '--', '-.'};

% --- Set up distributions / histograms for storing -----
dr = .2; % spacing for r grid
rr = (-15:dr:15)';  % r grid
nr = length(rr);
ppHit = zeros(nr,2);
ppMiss = zeros(nr,2);
ppFA = zeros(nr,2);
ppCR = zeros(nr,2);
nx1tot = 0;
nx0tot = 0;

for jsess = 1:nsess
    iisess = (sessnums==jsess); 
    
    % grab things we need for this session
    xx = Xeff(iisess);
    mm = M(iisess);
    dd = D(iisess);
    mmwts = Mwts(iisess);
    xlist = unique(xx);
    kap = unique(mmwts);
    xxb = xx>0; % binary (stimulus prsent or absent)
    nx0 = sum(~xxb);
    iifa = dd&~xxb;
    iicr = ~dd&~xxb;
    
    % Total measurement noise
    vartm = kap^2*varsh+varmi;
    sigtm = sqrt(vartm);

    % Compute mean of xeff
    xmult = mean(xlist(2:end));
    rrm = rr*xmult;
    
    % ===== 1. Do ABSENT trials  (CR and FA) ====================================
    % ------- Event probabilities -------- %
    % Numerical quantities
    qfa = sum(iifa)/nx0;
    qcr = sum(iicr)/nx0;
    % Analytic quantities
    z0 = (0-crit)/sigtd;
    pfa = normcdf(z0);   % probability of CR
    pcr = normcdf(-z0);  % probability of FA
    % fprintf('====\nFA: (empir:%.2f,anl:%.2f)| CR: (empir:%.2f,anl:%.2f)\n', qfa,pfa,qcr,pcr);
    
    % ------- Conditional distributions -------- %
    % Empirical dists
    hfa = hist(mm(iifa),rrm)'/dr/nx0; % dist of fa
    hcr = hist(mm(iicr),rrm)'/dr/nx0; % dist of cr
    % Analytical dists
    alph = varsh/vartm;
    gam = sqrt(vartd-(kap.^2*varsh^2)/vartm);
    prfa = 1/sigtm*normpdf((rrm)/sigtm).*normcdf((-crit+alph*kap*rrm)/gam)*xmult;
    prcr = 1/sigtm*normpdf((rrm)/sigtm).*normcdf((crit-alph*kap*rrm)/gam)*xmult;
    
    % Make fig
    clf;
    subplot(221); plot(rr,hcr,rr,prcr,'r');title('CR');
    subplot(222); plot(rr,hfa,rr,prfa,'r'); title('FA');

    % Save stuff
    ppFA(:,1) = ppFA(:,1) + hfa(:)*nx0;
    ppFA(:,2) = ppFA(:,2) + prfa*nx0;
    ppCR(:,1) = ppCR(:,1) + hcr(:)*nx0;
    ppCR(:,2) = ppCR(:,2) + prcr*nx0;
    nx0tot = nx0tot+nx0;
    
    % ===== 2. Do each contrast (Hit and Miss) ====================
    for jj = 1:length(xlist)-1
        xxeff = xlist(jj+1);
       
        % ------- Event probabilities -------- %
        % Numerical quantities
        iihit = dd&(xx==xxeff);
        iimiss = (~dd)&(xx==xxeff);
        nx1 = sum(xx==xxeff);
        qhit = sum(iihit)/nx1;
        qmiss = sum(iimiss)/nx1;
        % Analytic quantities
        z1 = (xxeff-crit)/sigtd;
        phit = normcdf(z0);   % probability of CR
        pmiss = normcdf(-z0);  % probability of FA
        % fprintf('----\ncont=%.2f, Hit:(%.2f,%.2f)| Miss:(%.2f,%.2f)\n',xxeff,qhit,phit,qmiss,pmiss);

        % ------- Conditional distributions -------- %
        % Empirical dists
        hhit = hist(mm(iihit),rrm)'/dr/nx1; % dist of hit
        hmiss = hist(mm(iimiss),rrm)'/dr/nx1; % dist of miss
        % Analytical dists
        mu1 = xxeff;  % mean response to this contrast
        mu1Rm = mu1*kap;  % mean of measured response
        prhit =  1/sigtm*normpdf((rrm-mu1Rm)/sigtm).*normcdf(-((crit- mu1*(1-alph*kap^2) - alph*kap*rrm))/gam)*xmult;
        prmiss = 1/sigtm*normpdf((rrm-mu1Rm)/sigtm).*normcdf( ((crit- mu1*(1-alph*kap^2) - alph*kap*rrm))/gam)*xmult;
        
        subplot(223); plot(rr,hmiss,linestr{jj},rr,prmiss,['r',linestr{jj}]);title('Miss'); hold on;
        subplot(224); plot(rr,hhit,linestr{jj},rr,prhit,['r',linestr{jj}]); title('Hit'); hold on;
       
        % Save stuff
        ppHit(:,1) = ppHit(:,1) + hhit(:)*nx1;
        ppHit(:,2) = ppHit(:,2) + prhit*nx1;
        ppMiss(:,1) = ppMiss(:,1) + hmiss(:)*nx1;
        ppMiss(:,2) = ppMiss(:,2) + prmiss*nx1;
        
        nx1tot = nx1tot+nx1;
    end
    
    keyboard;
        
end

% Normalize each distribution by the number of samples
ppHit = ppHit/nx1tot;
ppMiss = ppMiss/nx1tot;
ppFA = ppFA/nx0tot;
ppCR = ppCR/nx0tot;

% if 0  % renormalize so each is a normalized distribution
%     ppHit = bsxfun(@rdivide,ppHit,sum(ppHit))/dr;
%     ppMiss = bsxfun(@rdivide,ppMiss,sum(ppMiss))/dr;
%     ppCR = bsxfun(@rdivide,ppCR,sum(ppCR))/dr;
%     ppFA = bsxfun(@rdivide,ppFA,sum(ppFA))/dr;
% end

%% Save out necessary stuff, if desired
save([WriteDir 'savedDists'],'rr','ppHit','ppMiss','ppFA','ppCR');

%%  Make figure

figure;
ax = [rr(1),rr(end),0,0.125];
subplot(221);
plot(rr,ppCR); title('CR'); axis(ax);
subplot(222);
plot(rr,ppFA); title('FA'); axis(ax);
subplot(223);
plot(rr,ppMiss); title('Miss'); axis(ax);
subplot(224);
plot(rr,ppHit); title('Hit'); axis(ax);

%%

figure;
subplot(221); plot(rr,hcr,rr,prcr,'r');title('CR');
subplot(222); plot(rr,hfa,rr,prfa,'r'); title('FA');
subplot(223); plot(rr,hmiss,linestr{jj},rr,prmiss,['r',linestr{jj}]);title('Miss'); hold on;
subplot(224); plot(rr,hhit,linestr{jj},rr,prhit,['r',linestr{jj}]); title('Hit'); hold on;


%% TAKEN FROM AnalyzewithJonathan %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%**********************************************************************
%******* Use Jonathan's results to create theoretical distributions ***
%**********************************************************************
% first I run script3_inspectPredictedDists_on01.m to get results
%% THIS VERSION DOES NOT RESCALE THE DATA

ppPresent = ppHit(:,2) + ppMiss(:,2);
meanP = sum(rr.*ppPresent)/sum(ppPresent)
meanH = sum(rr.*ppHit(:,2))/sum(ppHit(:,2))
meanM = sum(rr.*ppMiss(:,2))/sum(ppMiss(:,2))
ppAbsent = ppFA(:,2) + ppCR(:,2);
meanA = sum(rr.*ppAbsent)/sum(ppAbsent)
meanFA = sum(rr.*ppFA(:,2))/sum(ppFA(:,2))
meanCR = sum(rr.*ppCR(:,2))/sum(ppCR(:,2))

% then plot the data
arrowYlo = 0.75*min(myYlim);
arrowYhi = 0; %max(myYlim);
arrowwidth = 5;
arrowheadweight = 10;
myXlim = [-15 15];
myYlim = [-.03 .12];


f = figure; set(f, 'Color', 'w');
lw = 2;
subplot(1,2,1); hold on;
h = plot(rr,ppPresent,'b-'); set(h,'LineWidth',lw);
h = plot(rr,ppAbsent,'k-'); set(h,'LineWidth',lw);
botos;
h = DrawArrow([meanP meanP],[arrowYlo arrowYhi],'^','b',arrowwidth,arrowheadweight,'-');
h = DrawArrow([meanA meanA],[arrowYlo arrowYhi],'^','k',arrowwidth,arrowheadweight,'-');

subplot(1,2,2); hold on;
h = plot(rr,ppHit(:,1),'g-'); set(h,'LineWidth',lw);
h = plot(rr,ppMiss(:,1),'g:'); set(h,'LineWidth',lw);
h = plot(rr,ppFA(:,1),'r:'); set(h,'LineWidth',lw);
h = plot(rr,ppCR(:,1),'r-'); set(h,'LineWidth',lw);

lw = 4;
h = plot(rr,ppHit(:,2),'g-'); set(h,'LineWidth',lw);
h = plot(rr,ppMiss(:,2),'g:'); set(h,'LineWidth',lw);
h = plot(rr,ppFA(:,2),'r:'); set(h,'LineWidth',lw);
h = plot(rr,ppCR(:,2),'r-'); set(h,'LineWidth',lw);
botos;
h = DrawArrow([meanH meanH],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-');
h = DrawArrow([meanM meanM],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,':');
h = DrawArrow([meanFA meanFA],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,':');
h = DrawArrow([meanCR meanCR],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-');

myFontSize = 14;
set(findobj(gcf,'type','axes'),'FontSize',myFontSize,'xlim',myXlim,'ylim',myYlim);


%% THIS VERSION RESCALES THE DATA

% % rescale the data (for display only) to {0,1}
% ppPresent = ppHit(:,2) + ppMiss(:,2);
% meanP = sum(rr.*ppPresent)/sum(ppPresent)
% meanH = sum(rr.*ppHit(:,2))/sum(ppHit(:,2))
% meanM = sum(rr.*ppMiss(:,2))/sum(ppMiss(:,2))
% ppAbsent = ppFA(:,2) + ppCR(:,2);
% meanA = sum(rr.*ppAbsent)/sum(ppAbsent)
% meanFA = sum(rr.*ppFA(:,2))/sum(ppFA(:,2))
% meanCR = sum(rr.*ppCR(:,2))/sum(ppCR(:,2))
% rrnew = (rr - meanA) ./ meanP;
% 
% % then plot the data
% f = figure; set(f, 'Color', 'w'); hold on;
% lw = 2;
% %h = plot(rrnew,ppPresent,'b-'); set(h,'LineWidth',myLineWidth);
% h = plot(rrnew,ppHit(:,1),'g-'); set(h,'LineWidth',lw);
% h = plot(rrnew,ppMiss(:,1),'g--'); set(h,'LineWidth',lw);
% %h = plot(rrnew,ppAbsent,'k-'); set(h,'LineWidth',myLineWidth);
% h = plot(rrnew,ppFA(:,1),'r--'); set(h,'LineWidth',lw);
% h = plot(rrnew,ppCR(:,1),'r-'); set(h,'LineWidth',lw);
% 
% lw = 4;
% h = plot(rrnew,ppHit(:,2),'g-'); set(h,'LineWidth',lw);
% h = plot(rrnew,ppMiss(:,2),'g--'); set(h,'LineWidth',lw);
% h = plot(rrnew,ppFA(:,2),'r--'); set(h,'LineWidth',lw);
% h = plot(rrnew,ppCR(:,2),'r-'); set(h,'LineWidth',lw);
% 
% 
% myXlim = [-15 15];
% myYlim = [-.03 .12];
% xlim(myXlim);
% ylim(myYlim);
% arrowYlo = 0.75*min(myYlim);
% arrowYhi = 0.25*min(myYlim);
% %arrowYhi = max(myYlim);
% arrowwidth = 5;
% arrowheadweight = 10;
% 
% ppPresent = ppHit(:,2) + ppMiss(:,2);
% meanP = sum(rrnew.*ppPresent)/sum(ppPresent)
% meanH = sum(rrnew.*ppHit(:,2))/sum(ppHit(:,2))
% meanM = sum(rrnew.*ppMiss(:,2))/sum(ppMiss(:,2))
% ppAbsent = ppFA(:,2) + ppCR(:,2);
% meanA = sum(rrnew.*ppAbsent)/sum(ppAbsent)
% meanFA = sum(rrnew.*ppFA(:,2))/sum(ppFA(:,2))
% meanCR = sum(rrnew.*ppCR(:,2))/sum(ppCR(:,2))
% 
% % muAhitpred = rr(find(ppHit(:,2)==max(ppHit(:,2))));
% % muAmisspred = rr(find(ppMiss(:,2)==max(ppMiss(:,2))));
% % muAfapred = rr(find(ppFA(:,2)==max(ppFA(:,2))));
% % muAcrpred = rr(find(ppCR(:,2)==max(ppCR(:,2))));
% 
% % draw arrows
% %h = DrawArrow([meanP meanP],[arrowYlo arrowYhi],'^','b',arrowwidth,arrowheadweight,'-');
% h = DrawArrow([meanH meanH],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-');
% h = DrawArrow([meanM meanM],[arrowYlo arrowYhi],'^','g',arrowwidth,arrowheadweight,'-.');
% %h = DrawArrow([meanA meanA],[arrowYlo arrowYhi],'^','k',arrowwidth,arrowheadweight,'-');
% h = DrawArrow([meanFA meanFA],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-.');
% h = DrawArrow([meanCR meanCR],[arrowYlo arrowYhi],'^','r',arrowwidth,arrowheadweight,'-');
% 
% axis square;
% myFontSize = 32;
% set(gca,'FontSize',myFontSize);
% set(gca,'XTick',[-4 -2 0 2 4],'XTickLabel',[{-4},{-2},{0},{2},{4},{5}]);
