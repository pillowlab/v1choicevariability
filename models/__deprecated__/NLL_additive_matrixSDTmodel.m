function neglogli = NLL_additive_matrixSDTmodel(prs, Dat, Rmvec, args)
% neglogli = NLL_additive_matrixSDTmodel(prs, Dat)
%
% Computes negative log-likelihood for choice model
% (Called by fmincon for ML estimation)
%
% INPUT:
%        prs [3x1] = [log_rho, log_ell, crit]';
%        Dat [Nx4] = [StimMagnitude, Measurement, Decision,Mwts]
%        X [17x17] = measured signal
%        args [struct] = MF, DF, dists
%
% OUTPUT:
%        neglogli - negative loglikelihood of data given parameters
%
% Updated 30 May 2020 (MJ Morais)

[logrho, logell, crit] = deal(prs(1), prs(2), prs(3));
[c, ~, D, kappa] = deal(Dat(:,1), Dat(:,2), Dat(:,3), Dat(:,4));
[MF, DF, Xvec, pool] = deal(args.MF, args.DF, args.stim, args.pool);
p = numel(pool);
Kzz = kernel(logrho, logell, args.dists) + 1e-6*eye(p);

% c = Dat(:,1); % pooled mean of each trial data
% Rm = Dat(:,2); % measured response
% D =  Dat(:,3); % choice
% kappa = Dat(:,4);

mu_tm = bsxfun(@times, kappa.*c, Xvec);
Sigma_tm = Kzz;
pRm_X = mvnpdf(Rmvec, mu_tm, Sigma_tm);

Sigma_s = MF^(1/p^2) .* Sigma_tm;
Sigma_sdivtm = Sigma_s / Sigma_tm;
sigma2_td = ((1/DF - 1) * MF^(1/p^2)) .* (pool' * Sigma_tm * pool);
u_d = c.*(Xvec * pool) + kappa .* ((Rmvec - mu_tm) * Sigma_sdivtm * pool);
sigma2_d = sigma2_td - kappa.^2 .* (pool' * (Sigma_sdivtm * Sigma_s) * pool);
pD_RmX = normcdf((u_d-crit)./sqrt(sigma2_d));
pD_RmX(D==0) = 1 - pD_RmX(D==0);

keyboard;
% pRm_X = normpdf(Rm,kappa.*mu,sqrt(vmi + kappa.^2.*vsh));%./sqrt(wt.^2.*vsh+vmi);
% pRm_X = mvnpdf(Rmvec, kappa.*c.*Xvec, Kzz);
% pRd_RmX = normcdf(c + kappa .* (pool * Sigma_s / Sigma_tm) * (Rmvec-kappa.*c),crit,...
%     sqrt(vdi+vsh-kappa.^2.*vsh^2./(vmi+kappa.^2.*vsh)));
% pRd_RmX(D==0)=1-pRd_RmX(D==0);  % take 1- this number for 0 responses.

ptrials = pRm_X.*pRd_RmX;
EPS = 1e-15; % tiny scalar to keep probability away from 0 & 1
ptrials = nanmax(ptrials,EPS); % probs so low they go nan

% Compute negative log-likelihood
neglogli = -sum(log(ptrials));

function K = kernel(logrho, logell, dists)
    K = exp(logrho) * exp(-(dists.^2 ./ (2 * exp(logell)^2)));
