% fit_scalarSDTmodel
%
% Compute ML fits for multi-contrast, variable SNR (unnormalized) data

setDirs;

% Load data
load([WriteDir 'dat_MM190805_pruned']);  % loads 'dat' with trial information
load([WriteDir 'AmpsByModel']);  % new data (from new maps)
load([WriteDir 'ii25']);  % load list of trials which were in blocks of contrast 25
load([WriteDir 'AmpsRaw_MM200606.mat']); % AmplitudesRenormalized
Amps = Amplitudes;

% Generate Gaussian pooling rule (== rule #5 in main text) with associated MF/DF
DF = 0.96; % measurement fraction of signal-measurement noise
MF = 0.06; % decision fraction of signal-decision noise
[height, width, N] = size(Amps);
d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;
w = normpdf(grid, 0, 2) ./ sum(normpdf(grid, 0, 2));
pool = kron(w, w)';

% Get distances from pixels to pixels
[xx, yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
dists = NaN(width*height,width*height);
xy = xy(:);
for i=1:width*height
    dists(:,i) = abs(xy - xy(i));
end

D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
contr = dat.contr;  % contrast of that trial
% Estimate the SNR for each experiment and contrast level
Coordinates.X = 1:height;
Coordinates.Y = 1:width;
% Proposed stimulus (Gaussian profile)
stim = FuncGaussian2D(FitGaussian2D(Coordinates, nanmean(Amps(:,:,X), 3)), Coordinates);
% Fit rank-one decomposition of SNRs and contrasts scaling each session
% [M, c, kappa, ~, ~, ~, ~] = findExptSNRs(Mraw, X, dat);
[AmpsNorm, stim, c, kappa, Mmus, Mmupred, usess, ucontr] = ...
    findExptSNRs_matrix(Amps, stim, X, dat, w);
% % Normalize s.t. mean pooled target-present response is 1
% uMraw = squeeze(nansum(nansum(AmpsNorm .* repmat(w' * w, [1, 1, N]))));
% uMraw(isinf(uMraw)) = 0;
% nrm = mean(uMraw(bP));
% AmpsNorm = AmpsNorm ./ nrm;
% stim = stim ./ nrm;

% Vectorize spatial inputs
stimvec = reshape(stim, height*width, [])';
trialMaps = reshape(AmpsNorm, height*width, [])';
trialMaps(ii25, :) = [];

M = trialMaps * pool; % squeeze(nansum(nansum(AmpsNorm .* repmat(w' * w, [1, 1, N]))));
trialData = [c, nan(size(c)), D, kappa]; % data used for fitting 
trialData(ii25,:) = [];

%% FIT additive model
args = struct('MF',MF, 'DF',DF, 'dists',dists, 'stim',stimvec, 'pool',pool);
prsInit = [4.45, 2, 0.5]; % Manually selected! initializeSDTmodelprs_matrix() can be a useful tool.
% < IN: trialData, trialMaps, args, prsInit

[prshat1, errbarSD1] = MLE_matrixSDTmodel('add', trialData, trialMaps, args, prsInit);

% DFhat1(whichModel) = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
% MFhat1(whichModel) = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
% DFerrbar1(whichModel,:) = [(prshat1.varsh-errbarSD1(3))/(prshat1.varsh-errbarSD1(3) + prshat1.vardi+errbarSD1(2)),...
%     (prshat1.varsh+errbarSD1(3))/(prshat1.varsh+errbarSD1(3) + max(0,prshat1.vardi-errbarSD1(2)))];
% crit(whichModel) = prshat1.crit;

%% FIT multiplicative model

[prshat2, errbarSD2] = MLE_matrixSDTmodel('mult', trialData);

%% Simulate from MLEs of two models, and plot

