function [prsHat, SD, exitflag, neglogli] = MLE_matrixSDTmodel(addOrMult, trialData, mapData, args, prsInit)
% [prsHat,SD,exitflag,neglogli] = compMLest_VSDIvarSNRdata(trialData,prsInit)
% 
% Compute ML estimate for simple signal detection
% theory experiment with variable contrast, variable SNR data
%
%
% INPUT:
%  addOrMult [str] - Flag for "add"itive noise or "mult"iplicative gain
%  trialData [N x 4] - raw data: [X M D Mwts]
%                    X  - assumed mean response to each stimulus (determined by contrast)
%                    M  - measurement
%                    D  - decision
%                    Mwts - weights for measurement that day
%  prsInit [3 x 1] - initial guess at parameters: log(rho), log(ell), crit
%
% OUTPUT:
%      prsHat [4 x 1] - maximum Likelihood estimate of 4 parameters above
%          SD [4 x 1] - 1SD error bars for all params 
%    exitflag [1 x 1] - from fminunc
%    neglogli [1 x 1] - value of negative logli at maximum 
%
% Updated 28 Aug 2014 (JW Pillow)
% Updated 30 May 2020 (MJ Morais)

% === 1. Initialize params ========
% prs0 = initializeSDTmodelprs_matrix(trialData, mapData, args);
% Just do it manually. The dataset is static after all.

% === 2. Call fmincon to optimize loglikelihood for all params ========
if strcmp(addOrMult, 'add')
    lossfun = @(prs) NLL_additive_matrixSDTmodel(prs, trialData, mapData, args);
elseif strcmp(addOrMult, 'mult')
    lossfun = @(prs) NLL_multiplicative_matrixSDTmodel(prs, trialData, mapData, args);
end

% set options and optimize negative logli
opts = optimset('display','off', 'algorithm','active-set', 'maxfunevals',1e6, 'maxiter',1e6);
[prsEstim, neglogli, exitflag, ~, ~, Hess] = fminunc(lossfun, prsInit, opts);

% === 3. place estimated params into struct ============================
prsHat.varmi = prsEstim(1);
prsHat.vardi = prsEstim(2);
prsHat.varsh = prsEstim(3);
prsHat.crit = prsEstim(4);

if nargout > 1
    % compute posterior standard deviation on each param ("error bars").
    sigmapost = sqrt(diag(inv(Hess)));
    SD = sigmapost;
end


