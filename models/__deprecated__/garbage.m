setDirs;

% Load data
load([WriteDir 'dat_MM190805_pruned']);  % loads 'dat' with trial information
load([WriteDir 'AmpsByModel']);  % new data (from new maps)
load([WriteDir 'ii25']);  % load list of trials which were in blocks of contrast 25
load([WriteDir 'AmpsRaw_MM200606.mat']); % AmplitudesRenormalized
Amps = Amplitudes;

% Generate Gaussian pooling rule (== rule #5 in main text) with associated MF/DF
DF = 0.96; % measurement fraction of signal-measurement noise
MF = 0.06; % decision fraction of signal-decision noise
[height, width, N] = size(Amps);
d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;
w = normpdf(grid, 0, 2) ./ sum(normpdf(grid, 0, 2));
pool = kron(w, w)';

% Normalize pooled responses to 1 when target present
uMraw = squeeze(nansum(nansum(Amps .* repmat(w' * w, [1, 1, N]))));
uMraw(isinf(uMraw)) = 0;
nrm = mean(uMraw(bP));
AmpsNorm = Amps ./ nrm;

% Get distances from pixels to pixels
[xx, yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
dists = NaN(width*height,width*height);
xy = reshape(xy, height*width, []);
for i=1:width*height
    for j=1:width*height
        dists(i,j) = abs(xy(i) - xy(j));
    end
end

D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
contr = dat.contr;  % contrast of that trial
% Estimate the SNR for each experiment and contrast level
Coordinates.X = 1:height;
Coordinates.Y = 1:width;
% Proposed stimulus (Gaussian profile)
% nostim = FuncGaussian2D(FitGaussian2D(Coordinates, nanmean(AmpsNorm(:,:,~X), 3)), Coordinates); % By any account, this is zero
stim = FuncGaussian2D(FitGaussian2D(Coordinates, nanmean(AmpsNorm(:,:,bP), 3)), Coordinates);

%% Likelihood-based kernel hyperparameter optimization for VSDI data sample

% MEAN STIMULUS MAP: 1 x 289 (flattened from 17x17 image)
stimvec = reshape(stim, height*width, [])';
% VSDI DATA: 9979 x 289
trialMaps = reshape(AmpsNorm, height*width, [])';
% VSDI DATA with target-present response subtracted on target-present trials
noise = trialMaps; noise(bP,:) = bsxfun(@minus, noise(bP,:), stimvec);
% RBF kernel covariance
kernel = @(logrho, logell) 1e-8*eye(289) + exp(logrho) * exp(-(dists.^2 ./ (2 * exp(logell)^2))); % dists = pairwise distances

% Total data (negative-log) likelihood
lkl = @(hyp) -nansum(logmvnpdf(noise,zeros(size(noise(1,:))),kernel(hyp(1),hyp(2))));
% Minimize to optimize kernel hyperparameters to model the noise
hyp0 = [4,0]; % log(trace(cov(noise))/n) = 4
hyp = fminunc(lkl, hyp0);

% lkl(hyp) = 3.2654e+13 <--- It's a terrible fit!
% hyp = [4,0] <--- Yet every initialization is a local minimum according to fmin()

%%
% MEAN STIMULUS MAP: 1 x 289
stimvec = reshape(stim, height*width, [])';
% VSDI DATA: 9979 x 289
trialMaps = reshape(AmpsNorm, height*width, [])'; 
% RBF kernel covariance
kernel = @(logrho, logell) exp(logrho) * exp(-(dists.^2 ./ (2 * exp(logell)^2)));
perf = [sum(bH)/sum(bP) sum(bM)/sum(bP) sum(bFA)/sum(bA) sum(bCR)/sum(bA)];

% [logrho, logell, crit] = 
logrho = 0;
logell = 0;

Rm = [mvnrnd(stimvec, MF.*kernel(logrho, logell), sum(bP)); mvnrnd(0.*stimvec, MF.*kernel(logrho, logell), sum(bA))];
pooled = Rm * pool;