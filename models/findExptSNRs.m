function [M, Xeff, Mwts, Mmus, Mmupred, usess, ucontr] = findExptSNRs(Mraw,X,dat)
% [M,Xeff,Mwts,Mmus,Mmupred,usess,ucontr] = findExptSNRs(Mraw,X,dat)
% 
% Compute matrix of mean response for each session x each contrast, and then find a rank
% one decomposition of this matrix so that the mean response on each trial is given by
% the product of an experiment-dependent (i.e., SNR-dependent) and stimulus
% contrast-dependent term.
%
% Inputs:
%   Mraw [ntrials x 1] - measured VSDI signal for each trial
%      X [ntrials x 1] - stimulus for each trial (0 or 1)
%    dat [struct] - carries info about the contrast and session number for 
%                   each trial.
%
% Outputs:
%      M [ntrials x 1] - Mraw rescaled so that max mean is 1.
%   Xeff [ntrials x 1] - effective stimulus contrast for each trial (mean of V1 resp)
%   Mwts [ntrials x 1] - SNR for each trial 
%   Mmus [nsess x ncontrasts] - matrix of experimental session vs. contrast
%   Mmupred [nsess x ncontrasts] - rank 1 approx to Mmus matrix 
%   usess  [nsess x 1] - column vector for rank 1 approx (session effects)
%   ucontr [ncntr x 1] - row vector for rank 1 approx (contrast effects)

contr = dat.contr;  % contrast of each trial
cs = unique(contr(contr>0));  % list of distinct contrasts
ncs = length(cs); % number of contrasts
nsess = max(dat.sessnums); % number of sessions
ntrials = length(Mraw);

Mmus = zeros(nsess,ncs);  % Mean response to signal in each block

% Construct matrix with mean response to each contrast, for each session
for i = 1:nsess
    for j = 1:ncs
        Mmus(i,j) = mean(Mraw((dat.sessnums==i)&(contr==cs(j))));
    end
end
maxMmu = nanmax(Mmus(:));
Mmus = Mmus./maxMmu;
M = Mraw/maxMmu;

% Fit low-rank structure of Mmus
[usess,ucontr] = estimExptSNRs(Mmus, cs);

Mmupred = usess*ucontr';
maxMmupred = max(abs(Mmupred(:)));
usess = usess/sqrt(maxMmupred);
ucontr = ucontr/sqrt(maxMmupred);
M = M/maxMmupred;
Mmupred = usess*ucontr';

% Put in contrast and SNR for each experiment
% Contrast of each stimulus
Xeff = double(X);
for j = 1:ncs
    Xeff(contr==cs(j))=ucontr(j);
end

% SNR of each experiment
Mwts = zeros(ntrials,1);
for j = 1:nsess
    Mwts(dat.sessnums==j) = usess(j);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [usess,ucontr] = estimExptSNRs(Rmus,cs)
% [usess,ucontr] = estimExptSNRs(Rmus,cs)
%
% Find a rank-1 decomposition of the mean response to the stimulus, with a linear
% model of the response to the low-contrast stimuli.  That is, find best
% vectors usess and ucontr such that:
%       Rmus ~= usess*ucontr'
% where ucontr is a linear function of the stimulus contrasts below 5%.
%
% INPUTS:
%  Rmus [nsess x ncontrasts] - mean response to signal in each expt / contrast
%    cs [ncontrasts x 1]     - number of diff contrasts
%
% OUTPUTS:
%   usess [nsess x 1] - experiment effect (SNR of each expt).
%   ucontr [ncontr x 1] - contrast effect (mean resp to each contrast)


% Process inputs
[nsess,ncs] = size(Rmus);
lincols = 1:ncs-1;  % columns where contrast-response is assumed linear

% Make initial estimate of 
eprs0 = nanmean(Rmus,2);  % initial estimate of experimental effect
ucsfull = nanmean(Rmus,1)';
cprs =  [ones(ncs-1,1), cs(1:end-1)]\ucsfull(1:end-1);
cbasis = [[ones(ncs-1,1), cs(1:end-1), zeros(ncs-1,1)];[0 0 1]];
cprs0 = [cprs;ucsfull(end)];

% Do optimization
prs0 = [eprs0;cprs0];
lfun = @(prs)mseloss(prs,cbasis,Rmus);

opts = optimset('largescale', 'off','display','off','algorithm','active-set');
lb = zeros(size(prs0));
ub = inf(size(prs0));
prshat = fmincon(lfun,prs0,[],[],[],[],lb,ub,[],opts);

usess = prshat(1:nsess);
ucontr = cbasis*prshat(nsess+1:end);

% Normalize so they have the same maximum
emx = max(usess);
cmx = max(ucontr);
usess = usess / emx * sqrt(emx*cmx);
ucontr = ucontr / cmx * sqrt(emx*cmx);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function err = mseloss(prs,cbasis,M)
% l = mseloss(prs,cbasis,M)
%
% Computes MSE between M (which contains nans) and its low-rank
% approximation

[nsess,ncs] = size(M);
eprs = prs(1:nsess);
cprs = cbasis*prs(nsess+1:end);

Mfit = eprs*cprs';
err = nansum((Mfit(:)-M(:)).^2);

    
