function loss = gamma_multxcovloss(logp, u, dists, S, MF, gtcov_P, pool)
%SYMMXCOV_MULTLOSS Optimize covariance params for multiplicative gain case
%   Not plainly available, like the additive noise case

G = gaminv(...
        normcdf(...
            mvnrnd(...
                u0, exp(-dists./exp(logell)), numel(X)), ...
            0, 1), ...
        1/exp(logtheta), exp(logtheta));

pS = exp(logp(1)) .* exp(-dists./exp(logp(2))); % Proposed covariance for underlying MVNormal
pu = -0.5.*diag(pS); % Proposed mean for underling log Gs ~ MVNormal s.t. proposed mean for Gs ~ MVLogNormal is 1 everywhere
plnS = exp(pS) - 1; % Proposed covariance for Gs ~ MVLogNormal **under that mean constraint** (not generally true)
plnXS = (u*u') .* plnS; % Proposed covariance for Gs .* X
TV = plnXS + (1-MF) .* S; % Total variance

% acov_P = NaN(size(dist1d));
% for i=1:length(dist1d)
%     tempInd = abs(dists - dist1d(i))<=.25;
%     acov_P(i) = (1-MF) * nanmean(S(tempInd));
% end

dist1d = [0:0.5:max(dists(:))];
tcov_P = NaN(size(dist1d));
for i=1:length(dist1d)
    tempInd = abs(dists - dist1d(i))<=.25;
    tcov_P(i) = nanmean(TV(tempInd));
end

lamb = 50.0;
newMF = (pool' * plnXS * pool) / (pool' * TV * pool);
loss = norm(gtcov_P - tcov_P) + lamb * norm(newMF - MF);

