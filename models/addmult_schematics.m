setDirs;

% Load data
load([WriteDir 'dat_MM190805_pruned']);  % loads 'dat' with trial information
load([WriteDir 'AmpsByModel']);  % new data (from new maps)
load([WriteDir 'ii25']);  % load list of trials which were in blocks of contrast 25
load([WriteDir 'AmpsRaw_MM200606.mat']); % AmplitudesRenorm
Amps = Amplitudes;
% scalarprs = struct('Xeff', 0.1196, 'varmi', 0.0955, 'vardi', 1.7986e-04, 'varsh', 0.0049, 'crit', 0.0764); % Unknown origin
scalarprs = struct('Xeff', 0.1040, 'varmi', 0.1494, 'vardi', 7.5905e-04, 'varsh', 0.0047, 'crit', 0.0799); % Model 5
scalar_dprime = scalarprs.Xeff / sqrt(scalarprs.vardi+scalarprs.varsh);
% NOTE: n.b. Xeff = median(Xeff(Xeff>0 & ~ii25))

% Performances : H M FA CR
gt_performance = [sum(bH)/sum(bP), sum(bM)/sum(bP) sum(bFA)/sum(bA) sum(bCR)/sum(bA)];
dprime = norminv(gt_performance(1)) - norminv(gt_performance(3)); % MacMillan & Creelman SDT textbook
AUC = normcdf(dprime / sqrt(2));
%   ** We target the ROC curve (equi-d' curve) that passes through the
%   observed performance. For the Gaussian case with fixed means (1-0),
%   this is unique.

% % Generate Gaussian pooling rule (== rule #5 in main text) with associated MF/DF
% DF = 0.8620; % measurement fraction of signal-measurement noise
% MF = 0.0308; % decision fraction of signal-decision noise
% % DF = 0.96; % measurement fraction of signal-measurement noise % WRONG MODEL
% % MF = 0.06; % decision fraction of signal-decision noise
% [height, width, N] = size(Amps);
% d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
% grid = ((0:(height-1)) - floor(width/2)) .* d;
% w = normpdf(grid, 0, 2) ./ sum(normpdf(grid, 0, 2));
% poolmap = w' * w;
% pool = kron(w, w)';

% Load optimal DoG pooling rule (== rule #8 in main text) with associated MF/DF
DF = 0.98; % measurement fraction of signal-measurement noise
MF = 0.05; % decision fraction of signal-decision noise
load('optDoG.mat'); % Loads DoG_pool
[height, width, N] = size(Amps);
d = 0.5; % correspond: sParameters.PIXELS16X16_TO_MM;
grid = ((0:(height-1)) - floor(width/2)) .* d;
poolmap = DoG_pool ./ sum(DoG_pool(:));
pool = poolmap(:);

% Normalize pooled responses to 1 when target present
uMraw = squeeze(nansum(nansum(Amps .* repmat(poolmap, [1, 1, N]))));
uMraw(isinf(uMraw)) = 0;
nrm = mean(uMraw(bP));
AmpsNorm = Amps ./ nrm;

% Get distances from pixels to pixels
[xx, yy] = meshgrid(grid, grid);
xy = xx + yy * 1i;
dists = NaN(width*height,width*height);
xy = reshape(xy, height*width, []);
for i=1:width*height
    for j=1:width*height
        dists(i,j) = abs(xy(i) - xy(j));
    end
end

% D = (bH|bFA);   % decision (1=yes or 0=no)
% X = (bH|bM);   % stimulus (1=present or 0=absent)
contr = dat.contr;  % contrast of that trial

% % Get spatial correlation profile
% [~, gtcorr_P] = symmxcorr(AmpsNorm(:,:,bP), dists);
% [~, gtcorr_A] = symmxcorr(AmpsNorm(:,:,bA), dists);
% Match kernel magnitude and lengthscale to target-present cross-covariances
[dist1d, gtcov_P] = symmxcov(AmpsNorm(:,:,bP), dists);


%% Refine simulation parameters for ADDITIVE simulation
% f = @(logell) norm(gtcorr_P(2:end) - exp(-dist1d(2:end)./exp(logell)));
f = @(logp) norm(gtcov_P(2:end) - exp(logp(1)) .* exp(-dist1d(2:end)./exp(logp(2))));
logp_add = fminunc(f, [0, 0]);
[logrho, logell] = deal(logp_add(1), logp_add(2));
% Match Independent additive noise to excess variance not explained by kernel
% sig0 = gtcov_P(1) - exp(logrho);
% sig0 = 0; median(gtvariances - exp(logrho)); % DEPRECATED
residuals = diag(nancov(reshape(AmpsNorm(:,:,bP),[width*height, sum(bP)])')) - exp(logrho);
sig0I = diag((residuals > 0) .* residuals);

% Proposed stimulus (Gaussian profile)
Coordinates.X = 1:height;
Coordinates.Y = 1:width;
% nostim = FuncGaussian2D(FitGaussian2D(Coordinates, nanmean(AmpsNorm(:,:,~X), 3)), Coordinates); % By any account, this is zero
% stim = FuncGaussian2D(FitGaussian2D(Coordinates, nanmean(AmpsNorm(:,:,bP), 3)), Coordinates); % bestfit = [1.1450, 9.6867, 8.1855, 0.4702, 77.3138, 4.0000, 3.2639]
% Clean up the best-fit Gaussian to this schematic stimulus:
stim = FuncGaussian2D([1, 9, 9, 0, 90, 4, 4], Coordinates);

u = stim(:) ./ (pool' * stim(:)); % Target-on pools to 1, as in the data
u0 = 0.*u;
% Kernel for spatial covariances ...
Kadd = exp(logrho) * exp(-dists./exp(logell)); % dists = pairwise distances
% ... + pixelwise variances = Data covariance
% Sadd = sig0*eye(289) + K;
S_unscaled = sig0I + Kadd;

% simstd = sqrt(pool' * Sadd_unscaled * pool);
% empstd = std(AmpsByModel(5,bP));
% 
% Sadd = (empstd/simstd)^2 .* Sadd_unscaled;

currentvar = pool' * S_unscaled * pool;
targetvar = (DF/MF) / (dprime^2);

Sadd = (targetvar/currentvar) .* S_unscaled;

%% Simulate ADDITIVE data
% We assume the sensory noise and measurement noise share the same
% multivariate Gaussian with spatial correlations, and partition the total
% variance by MF. With some algebra, we can also use DF to generate
% appropriately scaled univariate Gaussian decision noise.

nP = 25000; %sum(bP); % or a much larger number like 10000
nA = 25000; %sum(bA);
% Stim
X = cat(1, ones(nP, 1), zeros(nA, 1));
Xmap = cat(1, repmat(u', [nP 1]), repmat(u0', [nA 1]));
% Maps
Ns = sqrt(MF) .* mvnrnd(u0, Sadd, numel(X));
Nm = sqrt(1-MF) .* mvnrnd(u0, Sadd, numel(X));
% Can confirm: MF ~= var(Ns * pool) ./ var((Ns + Nm) * pool)
Rm = (Xmap + Ns) + Nm;

% Pooled scalars
%   MF = sigma_s / sigma_tm
%   sigma_tm = pool' * S * pool   <--- using the data, we can directly observe the empirical S == Sigma_tm
%       ==> sigma_s = MF * (pool' * S * pool)
%   DF = sigma_s / (sigma_s + sigma_d)
%       ==> sigma_d = sigma_s * (1 - DF) / DF
%       ==> sigma_d = (MF * (pool' * S * pool)) * (1 - DF) / DF
ns = Ns * pool;
nd = sqrt((MF * (pool' * Sadd * pool)) * (1 - DF) / DF) .* randn(size(ns));
% Can confirm: DF ~= var(ns) ./ var(ns+nd)
rd = (X + ns) + nd;

% Optimize the threshold -- unfortunately, this seems to be a task for
% gridsearch, as fmin is finicky
g = @(tau) performanceloss(rd, X, tau, gt_performance, true);
%   This quasi-loss function tries to match hit and miss rate
taus = linspace(min(rd), max(rd), 2000);
gval = zeros(size(taus));
for k = 1:numel(taus)
    gval(k) = g(taus(k));
end
[~, idx] = min(gval);
tau = taus(idx);
% Output the decisions and performance
D = rd >= tau;
performance = performanceloss(rd, X, tau, gt_performance, false) %#ok<NOPTS>
rd_add = rd;

%% Visualize ADDITIVE
bPs = X & X; % force logical
bAs = ~X;
bHs = (X & D);
bMs = (X & ~D);
bFAs = (~X & D);
bCRs = (~X & ~D);
AmpsSim = permute(reshape(Rm, numel(X), height, width), [2 3 1]);

plots.simulatedmaps; % Shoehorn simulated data into the empirical data's plotting utilities

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Refine simulation parameters to simulate MULTIPLICATIVE data
% We exchange the shared additive noise Ns with a shared multiplicative
% gain Gs that also features MF/DF in the same proportions, and still
% matches the observed spatial correlations in the maps. Likewise, the
% AUC/performance/d-prime are all preserved, although the implied SDT model
% does not obey the typical pair of Gaussians.
%
% We use the multivariate Gaussian copula to generate a "map" of correlated
% gamma RVs with mean constrained to be 1, which functions as a gain that
% can have spatial correlations. The additive measurement noise and the
% gain's Gaussian copula both inherit the lengthscales of the optimal 
% additive model. The Gaussian copula applies nonlinear transformations
% (inverse Gamma CDF), which cannot guarantee the input-output correlations
% are preserved, but qualtitatively this isn't noticable or important.
%
% Since the combination of additive Gaussian and multiplicative Gamma
% doesn't induce a well-posed, identifiable model, we optimize the various
% parameters through an ad-hoc gridsearch.

nP = 25000; %sum(bP); % or a much larger number like 10000
nA = 25000; %sum(bA);
asisDF = false;

mu = u0;
logell = logp_add(2);
% Copula trick mvnrnd correlation matrix:
Sigma = exp(-dists./exp(logell)); % Must be a correlation matrix for copula trick. 
% Marginal gamma distributions, constrained mean-1 with scale theta:
% logtheta = 0.97; % Use with S_unscaled, optimal noise level for covariance-matching 
% logtheta = -0.6735; % Use with Sadd, optimal noise level for additive model
logtheta = 0.3;

% The optimal scaling is somewhere between (targetvar/currentvar) = 0.25 and (None) = 1.0
%       NOTE: targetvar/currentvar = 0.24949061151565504746
Smult = 0.5 .* S_unscaled;

% Stim
X = cat(1, ones(nP, 1), zeros(nA, 1));
Xmap = cat(1, repmat(u', [nP 1]), repmat(u0', [nA 1]));
% Maps
Z = mvnrnd(mu, Sigma, numel(X)); 
U = normcdf(Z, 0, 1);
Gs = gaminv(U, 1/exp(logtheta), exp(logtheta)); % E[G] = 1, and induces Var[G] = theta
Gs(isinf(Gs)) = 1;
Nm = sqrt(1-MF) .* mvnrnd(u0, Smult, numel(X)); % This stays the same in a multiplicative case
% Can confirm: MF ~~ var(bsxfun(@times, Gs, u') * pool) ./ var((bsxfun(@times, Gs, u') + Nm) * pool)
fprintf('MF ~ %.5f vs Target MF = %.5f\n',var(bsxfun(@times, Gs, u') * pool) ./ var((bsxfun(@times, Gs, u') + Nm) * pool), MF)
Rm = (Gs .* Xmap) + Nm;

% Pooled scalars
%   MF = sigma_s / sigma_tm
%       ==> sigma_s = pool' * lnXs * pool
%   DF = sigma_s / (sigma_s + sigma_d)
%       ==> sigma_d = sigma_s * (1 - DF) / DF
%       ==> sigma_d = (pool' * lnXs * pool) * (1 - DF) / DF
xs = (Gs .* Xmap) * pool;
% nd = sqrt((pool' * lnXS * pool) * (1 - DF) / DF) .* randn(size(xs));

mingval = 1.0;
DFadjusted = DF;
while mingval > 0.005 && DFadjusted > 0
    sigma_d = sqrt(var(bsxfun(@times, Gs, u') * pool) * (1-DFadjusted)/DFadjusted);
    nd = sigma_d .* randn(size(xs));
    % Can confirm: DF ~= var(xs) ./ var(xs+nd)
    rd = xs + nd;

    % Optimize the threshold -- unfortunately, this seems to be a task for
    % gridsearch, as fmin is finicky
    g = @(tau) performanceloss(rd, X, tau, gt_performance, true);
    %   This quasi-loss function tries to match hit and miss rate
    taus = linspace(-0.5, 1, 1000);
    gval = zeros(size(taus));
    for k = 1:numel(taus)
        gval(k) = g(taus(k));
    end
    [mingval, idx] = min(gval);
    % INCREMENTALLY ADJUST DF UNTIL THE FA CAN REACH THE OBSERVED LEVEL
    if asisDF
        break
    end
    DFadjusted = DFadjusted - 0.01; % NOTE: lazy approach
end
fprintf('Max DF that permits FA: %.3f\n',DFadjusted)
tau = taus(idx);
% Output the decisions and performance
D = rd >= tau;
performance = performanceloss(rd, X, tau, gt_performance, false);
fprintf('Performance: [H=%.3f, M=%.3f, FA=%.3f, CR=%.3f]\n',performance)
rd_mult = rd;

% % ROC visualization (Uncomment when tuning multiplicative params)
% taugrid = linspace(-1,1,1000);
% figure;
% scatter(gt_performance(3), gt_performance(1), 100, 'k','filled')
% hold on; 
% plot([0,1],[0,1],'k:'); 
% plot(normcdf(-taugrid ./ sqrt(scalarprs.varsh+scalarprs.vardi)), normcdf((scalarprs.Xeff-taugrid) ./ sqrt(scalarprs.varsh+scalarprs.vardi)), 'linewidth',2)
% 
% taugrid = linspace(min(rd), max(rd), 5000);
% [tpr, fpr] = deal(nan(size(taugrid)), nan(size(taugrid)));
% for k = 1:numel(taugrid)
%     perf = performanceloss(rd, X, taugrid(k), gt_performance, false);
%     [tpr(k), fpr(k)] = deal(perf(1), perf(3));
% end
% plot(fpr, tpr,':', 'linewidth',2);
% scatter(performance(3), performance(1), 100, 'k')
% model_AUC = -trapz(fpr, tpr);
% fprintf('AUC = %.5f vs Target = %.5f\n---\n', model_AUC, AUC)
% botos;

%% Visualize MULTIPLICATIVE
bPs = X & X; % force logical
bAs = ~X;
bHs = (X & D);
bMs = (X & ~D);
bFAs = (~X & D);
bCRs = (~X & ~D);
AmpsSim = permute(reshape(Rm, numel(X), height, width), [2 3 1]);
% AmpsSim = permute(reshape(Gs .* Xmap, numel(X), height, width), [2 3 1]);

plots.simulatedmaps; % Shoehorn simulated data into the empirical data's plotting utilities

%% ROC assay

taugrid = linspace(-1,1,1000);
f = figure; set(f,'Color','white','units','inches','Position',[1 1 5 5]);

scatter(gt_performance(3), gt_performance(1), 100, 'k','filled')
hold on; 
plot([0,1],[0,1],'k--'); 
plot(normcdf(-taugrid ./ sqrt(scalarprs.varsh+scalarprs.vardi)), normcdf((scalarprs.Xeff-taugrid) ./ sqrt(scalarprs.varsh+scalarprs.vardi)), 'linewidth',2, 'color',[0.25 0.25 0.25])

taugrid = linspace(min(rd_add), max(rd_add), 5000);
[tpr, fpr] = deal(nan(size(taugrid)), nan(size(taugrid)));
for k = 1:numel(taugrid)
    perf = performanceloss(rd_add, X, taugrid(k), gt_performance, false);
    [tpr(k), fpr(k)] = deal(perf(1), perf(3));
end
plot(fpr, tpr, 'linewidth',2, 'color',BLUE);
% scatter(performance(3), performance(1), 100, 'k')
model_AUC = -trapz(fpr, tpr);
fprintf('Additive AUC = %.5f\n', model_AUC)

taugrid = linspace(min(rd_mult), max(rd_mult), 5000);
[tpr, fpr] = deal(nan(size(taugrid)), nan(size(taugrid)));
for k = 1:numel(taugrid)
    perf = performanceloss(rd_mult, X, taugrid(k), gt_performance, false);
    [tpr(k), fpr(k)] = deal(perf(1), perf(3));
end
plot(fpr, tpr,':', 'linewidth',2, 'color',BLUE, 'linestyle',':');
% scatter(performance(3), performance(1), 100, 'k')
model_AUC = -trapz(fpr, tpr);
fprintf('Multiplicative AUC = %.5f\n', model_AUC)

xlabel('FPR')
ylabel('TPR')
legend({'Observed performance','Chance performance','Scalar likelihood model ROC','Additive noise simulation ROC','Multiplicative gain simulation ROC'}, 'location','southeast')
botos;

%% How to simulate "correlated" Gamma RVs using the Gaussian copula

% % n   = 10000;           % # of random number
% % mu  = [0, 0];       % [muX1, muX2, muX3]
% % rho = [1.0, 0.5; 0.5, 1.0];  % covariance matrix
% % 
% % Z = mvnrnd(mu, rho, n); 
% % U = normcdf(Z, 0, 1);     %Compute the CDF
% % 
% % % Gamma distribution marginal parameters
% % k = 2;
% % theta = 1/k; % This constraint asserts the mean G = 1, and induces Var = 1/k = theta
% % G = gaminv(U, k, theta);
% % G(isinf(G)) = NaN;
% % 
% % figure;
% % scatterhist(G(:,1), G(:,2),'Direction','out')

%% Scraps / deprecated code
% %% Refine simulation parameters for MULTIPLICATIVE simulation
% logp_mult = [log(0.0), log(5.0)];
% ff = @(logp) multxcovloss(logp, u, dists, Sadd_unscaled, MF, gtcov_P, pool);
% [logp_mult, ffval] = fminunc(ff, logp_mult);
% 
% [logrho, logell] = deal(logp_mult(1), logp_mult(2));
% K = exp(logrho) .* exp(-dists./exp(logell)); % Proposed covariance for underlying MVNormal
% % pu = -0.5.*diag(K); % Proposed mean for underling log Gs ~ MVNormal s.t. proposed mean for Gs ~ MVLogNormal is 1 everywhere
% lnS = exp(K) - 1; % Proposed covariance for Gs ~ MVLogNormal **under that mean constraint** (not generally true)
% lnXS = (u*u') .* lnS; % Proposed covariance for Gs .* X
% Smult = lnXS + (1-MF) .* Sadd_unscaled; % Total variance
% fprintf('log(rho) = %.5f, log(ell) = %.5f\n', logrho, logell)
% fprintf('MF for these covariance params (matched if MF = 0.06): %.5f\n', (pool' * lnXS * pool) / (pool' * Smult * pool))
% 
% % % To visualize the exact covariance:
% acov_P = NaN(size(dist1d));
% for i=1:length(dist1d)
%     tempInd = abs(dists - dist1d(i))<=.25;
%     acov_P(i) = nanmean((1-MF) .* Sadd_unscaled(tempInd));
% end
% tcov_P = NaN(size(dist1d));
% for i=1:length(dist1d)
%     tempInd = abs(dists - dist1d(i))<=.25;
%     tcov_P(i) = nanmean(Smult(tempInd));
% end
% figure; plot(dist1d, tcov_P, dist1d, acov_P), hold on, plot(dist1d, gtcov_P,'k')
% 
% % % To take some samples:
% % nsim = 100000;
% % Z = mvnrnd(-0.5 .* exp(logrho) .* ones(size(u0)), ...
% %            exp(logrho) .* exp(-dists./exp(logell)), nsim);
% % Gs = exp(Z);
% % Nm = sqrt(1-MF) .* mvnrnd(u0, S, nsim);
% % altRm = bsxfun(@times, Gs, u') + Nm;
% % [dist1d, cov_P] = symmxcov(permute(reshape(altRm, nsim, height, width), [2 3 1]), dists);
% % figure; plot(dist1d, cov_P, dist1d, gtcov_P)
% % fprintf('Total variance of simulated signal minus total variance if MF = 0.06 (should be 0): %.5f\n', var(bsxfun(@times, Gs, u') * pool) - MF/(1-MF) * var(Nm * pool))
% % fprintf('Mean gain (should be 1): %.5f\n', nanmean(nanmean(Gs,2)))
% % II(reshape(Gs(10,:), height, width), reshape(Gs(11,:), height, width), reshape(Gs(12,:), height, width), reshape(Gs(16,:), height, width));
% 
% %% Simulate MULTIPLICATIVE data
% % This case is substantially more challenging, since we need to generate a
% % sensory gain -- differently distributed than the measurement noise --
% % that satisfies the MF observed. After some algebra, the total variance
% % contributed by the gain needs to be
% % var((Gs .* X) * pool) = MF/(1-MF) * var(Nm * pool)
% 
% % Stim
% X = cat(1, ones(sum(bP), 1), zeros(sum(bA), 1));
% Xmap = cat(1, repmat(u', [sum(bP) 1]), repmat(u0', [sum(bA) 1]));
% % Maps
% % Ns = sqrt(MF) .* mvnrnd(u0, S, numel(X)); 
% Z = mvnrnd(-0.5 .* exp(logrho) .* ones(size(u0)), K, numel(X));
% Gs = exp(Z); % MVLogNormal gain with mean 1 everywhere
% Nm = sqrt(1-MF) .* mvnrnd(u0, Sadd, numel(X)); % This stays the same in a multiplicative case
% % Can confirm: MF ~~ var((Gs .* Xmap) * pool) ./ var(((Gs .* Xmap) + Nm) * pool)
% Rm = (Gs .* Xmap) + Nm;
% 
% % Pooled scalars
% %   MF = sigma_s / sigma_tm  <--- for the multiplicative model, we computed lnXS == Sigma_s using the data, especially since we can't guarantee the exact value of MF
% %       ==> sigma_s = pool' * lnXs * pool
% %   DF = sigma_s / (sigma_s + sigma_d)
% %       ==> sigma_d = sigma_s * (1 - DF) / DF
% %       ==> sigma_d = (pool' * lnXs * pool) * (1 - DF) / DF
% xs = (Gs .* Xmap) * pool;
% nd = sqrt((pool' * lnXS * pool) * (1 - DF) / DF) .* randn(size(xs));
% % Can confirm: DF ~= var(xs) ./ var(xs+nd)
% rd = xs + nd;
% 
% % Optimize the threshold -- unfortunately, this seems to be a task for
% % gridsearch, as fmin is finicky
% g = @(tau) performanceloss(rd, X, tau, gt_performance, true);
% %   This quasi-loss function tries to match hit and miss rate
% taus = linspace(0, 1, 1000);
% gval = zeros(size(taus));
% for k = 1:numel(taus)
%     gval(k) = g(taus(k));
% end
% [~, idx] = min(gval);
% tau = taus(idx);
% % Output the decisions and performance
% D = rd >= tau;
% performance = performanceloss(rd, X, tau, gt_performance, false) %#ok<NOPTS>
% 
% %% Visualize MULTIPLICATIVE
% bPs = X & X; % force logical
% bAs = ~X;
% bHs = (X & D);
% bMs = (X & ~D);
% bFAs = (~X & D);
% bCRs = (~X & ~D);
% AmpsSim = permute(reshape(Rm, numel(X), height, width), [2 3 1]);
% 
% plots.simulatedmaps; % Shoehorn simulated data into the empirical data's plotting utilities
% 
% %% ROC again
% taugrid = linspace(-1,1,1000);
% figure;
% scatter(gt_performance(3), gt_performance(1), 100, 'k','filled')
% hold on; 
% plot([0,1],[0,1],'k:'); 
% plot(normcdf(-taugrid ./ sqrt(scalarprs.varsh+scalarprs.vardi)), normcdf((scalarprs.Xeff-taugrid) ./ sqrt(scalarprs.varsh+scalarprs.vardi)), 'linewidth',2)
% 
% taugrid = linspace(min(rd), max(rd), 5000);
% [tpr, fpr] = deal(nan(size(taugrid)), nan(size(taugrid)));
% for k = 1:numel(taugrid)
%     perf = performanceloss(rd, X, taugrid(k), gt_performance, false);
%     [tpr(k), fpr(k)] = deal(perf(1), perf(3));
% end
% plot(fpr, tpr,':', 'linewidth',2);
% scatter(performance(3), performance(1), 100, 'k')
% model_AUC = -trapz(fpr, tpr);
% fprintf('AUC = %.5f', model_AUC)
% botos;
