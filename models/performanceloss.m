function out = performanceloss(rd, X, tau, gtperf, optmode)
%PERFORMANCEKL Discrete KL divergence between vector pmf p and vector pmf q
%   The pmf p comes from the simulation performance determined by the
%   decision variable rd, the stimulus X, and the decision threshold tau

D = rd >= tau;
bP = X & X;
bA = ~X;
bH = (X & D);
bM = (X & ~D);
bFA = (~X & D);
bCR = (~X & ~D);

perf = [sum(bH)/sum(bP), sum(bM)/sum(bP) sum(bFA)/sum(bA) sum(bCR)/sum(bA)];
% loss = perf * log(perf ./ gtperf)';
% loss = norm(perf - gtperf, 1);
% loss = norm(perf(1:2) - gtperf(1:2), 1);
loss = norm(perf([1,3]) - gtperf([1,3]), 2);
if optmode
    out = loss; % Output the loss in optimization mode
else
    out = perf; % Output the performance otherwise % NOTE: OVERLOAD
end



