% Requires typical analysis variables to already be loaded into the
% workspace (bH, etc.)

%**********************************************************************
% ******* Fig. 4 ******************************************************
%**********************************************************************

% calculate total decision variability using behavior
phit = sum(bH)/sum(bP);
pmiss = sum(bM)/sum(bP);
pfa = sum(bFA)/sum(bA);
pcr = sum(bCR)/sum(bA);
dprime = norminv(phit) - norminv(pfa);
VarTD = 1/dprime;
crit = norminv(pcr,0,VarTD);

f = figure; set(f,'Color','white','units','normalized','Position',[.1 .3 .8 .7]); hold on; axis off;
ha = tight_subplot(2, 5, .025, .05, .05);
handles = reshape(ha,5,2)';
N = 500;
CreateFig4Panel(.77,1,VarTD,crit,N,10e-4,handles(:,1));  % our solution
CreateFig4Panel(.53,1,VarTD,crit,N,10e-4,handles(:,2));  % crossing point
CreateFig4Panel(.05,1,VarTD,crit,N,25e-4,handles(:,3));  % DF = 5%
CreateFig4Panel(.77,.40,VarTD,crit,N,5e-4,handles(:,4));  % our solution plus noise
set(findobj(gcf,'type','line'),'linewidth',2.5)
axes(handles(1,1)); 
    text(0.5,1.1,'DF = 0.77, MF = 1','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
    text(0.5,1.25,'High DF','FontSize',24,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
    ylabel('Probability','FontSize',18)
axes(handles(1,2)); 
    text(0.5,1.1,'DF = 0.59, MF = 1','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
    text(0.5,1.25,'Moderate DF','FontSize',24,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(handles(1,3)); 
    text(0.5,1.1,'DF = 0.05, MF = 1','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
    text(0.5,1.25,'Low DF','FontSize',24,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(handles(1,4)); 
    text(0.5,1.1,'DF = 0.77, MF = 0.40','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
axes(handles(2,1)); 
    text(0.95,0.15,{'CP = 0.93', 'R_F_A - R_M = 0.30'},'FontSize',18,'FontName','Helvetica','HorizontalAlignment','right','Units','Normalized','Interpreter','TeX')
    ylabel({'Decision variable';'(normalized)'},'FontSize',18)
    xlabel({'Measured Response','(normalized)'},'FontSize',18)
axes(handles(2,2)); 
    text(0.95,0.15,{'CP = 0.87', 'R_F_A - R_M = 0.00'},'FontSize',18,'FontName','Helvetica','HorizontalAlignment','right','Units','Normalized','Interpreter','TeX')
axes(handles(2,3)); 
    text(0.95,0.15,{'CP = 0.61', 'R_F_A - R_M = -0.92'},'FontSize',18,'FontName','Helvetica','HorizontalAlignment','right','Units','Normalized','Interpreter','TeX')
axes(handles(2,4)); 
    text(0.95,0.15,{'CP = 0.76', 'R_F_A - R_M = 0.30'},'FontSize',18,'FontName','Helvetica','HorizontalAlignment','right','Units','Normalized','Interpreter','TeX')

% get values for reporting
[scci,ns,cp0,cp1] = compStatsVsDF([.77 .59 .05],1,phit,crit)
[scci,ns,cp0,cp1] = compStatsVsDF(.77,.4,phit,crit)

% f = figure; set(f,'Color','w'); hold on; axis off;
% ha = tight_subplot(2, 4, [.02 .02], [.05 .01], .02);
% handles = reshape(ha,4,2)';
df = 0.77;
mf = 0.03;
CreateFig4Inset(phit,crit,df,mf,.77, handles(:,5));
set(findobj(gcf,'type','axes'),'tickdir','out');

%**********************************************************************
% Extra panel

f = figure; set(f,'Color','white','units','normalized','Position',[.1 .3 .8/5 .7]); hold on; axis off;
ha = tight_subplot(2, 1, .025, .05, .05);
handles = reshape(ha,1,2)';
N = 500;
CreateFig4Panel(1,1,VarTD,crit,N,10e-4,handles(:,1));    % decision variable
delete(handles(2,1));
set(findobj(gcf,'type','axes'),'tickdir','out');