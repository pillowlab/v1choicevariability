truenoisetype = 'Additive';
ntrials = 10000;
nsess = 40;
trialspersess = ceil(ntrials / nsess);

% %% true vals for additive model 9: struct('varmi', 0.1085, 'vardi', 5.5437e-05, 'varsh', 0.0051, 'crit', 0.0774)
% prs = struct('varmi', 0.1, 'vardi', 0.0001, 'varsh', 0.005, 'crit', 0.075);
prs = struct('varmi', 0.1, 'vardi', 0.001, 'varsh', 0.05, 'crit', 0.075);

% Xeff takes 3-4 stimulus contrasts, which vary by session in the real data
% but the modal session generally use these four values:
Xopts = [0, 0.0970560999585529, 0.117853835663957, 1]; % Most common option

%% Single pass through single round of data simulation

loglik = [nan, nan];
sim_Xeff = Xopts(randsample([1 2 3 4], ntrials, true, [0.5 0.25 0.25 0.0]))';
% Mwts ranges from [0, 1], one val per session
sim_Mwts = reshape(repmat(rand(1, nsess), [trialspersess, 1]), [], 1);

% Simulate the noise shared by each model
mi = sqrt(prs.varmi).*randn(ntrials,1);
sh = sqrt(prs.varsh).*randn(ntrials,1);
di = sqrt(prs.vardi).*randn(ntrials,1);
% Combine them per the specific model
if strcmpi(truenoisetype, 'additive')
    sim_Rm = sim_Mwts .* (sim_Xeff + sh) + mi;
    sim_Rd = sim_Xeff + sh + di;
    sim_D = 1.0 * (sim_Rd >= prs.crit);
elseif strcmpi(truenoisetype, 'multiplicative')
    sim_Rm = sim_Mwts .* (1 + sh) .* sim_Xeff + mi;
    sim_Rd = (1 + sh) .* sim_Xeff + di;
    sim_D = 1.0 * (sim_Rd >= prs.crit);
end
sim_trialData = [sim_Xeff, sim_Rm, sim_D, sim_Mwts];
% Prune high contrasts
% sim_trialData(sim_trialData(:,1)>0.99,:) = []; % We just won't draw that one at all
    
fprintf('Fitting Simulated %s Model\n', truenoisetype)
noisetype = {'additive','multiplicative'};
for i = 1:2
    fprintf('--- %s ---\n', [upper(noisetype{i}(1)), noisetype{i}(2:end)])
    % Fit model to all low-contrast data
    [prshat, errbarSD1, ~, nll] = compMLest_VSDIvarSNRdata(sim_trialData, noisetype{i});
    loglik(i) = -nll;
    fprintf('loglik = %.2f\n', -nll)
    disp(prshat)
end

%% Bootstrapped multiple passes through data for each model type
% THIS WILL TAKE TIME!!
warning('off','all')
NBOOT = 1000;
bootloglik = nan(2, NBOOT);

fprintf('Fitting many iterations of Simulated %s Model\n', truenoisetype)
for i = 1:2
    fprintf('--- %s ---\n[', [upper(noisetype{i}(1)), noisetype{i}(2:end)])
    for j = 1:NBOOT
        if mod(j, ceil(NBOOT/100)) == 0
           fprintf('.') 
        end
        % Sample a new iteration of data
        mi = sqrt(prs.varmi).*randn(ntrials,1);
        sh = sqrt(prs.varsh).*randn(ntrials,1);
        di = sqrt(prs.vardi).*randn(ntrials,1);
        % Combine them per the specific model
        if strcmpi(truenoisetype, 'additive')
            sim_Rm = sim_Mwts .* (sim_Xeff + sh) + mi;
            sim_Rd = sim_Xeff + sh + di;
            sim_D = 1.0 * (sim_Rd >= prs.crit);
        elseif strcmpi(truenoisetype, 'multiplicative')
            sim_Rm = sim_Mwts .* (1 + sh) .* sim_Xeff + mi;
            sim_Rd = (1 + sh) .* sim_Xeff + di;
            sim_D = 1.0 * (sim_Rd >= prs.crit);
        end
        sim_trialData = [sim_Xeff, sim_Rm, sim_D, sim_Mwts];
        
        % Fit model to all low-contrast data
        [prshat1, errbarSD1, ~, nll] = compMLest_VSDIvarSNRdata(sim_trialData, noisetype{i});
        bootloglik(i, j) = -nll;
    end
    fprintf(']\n')
end

fprintf('Additive model loglik (w/ CI)\n')
k = 1; fprintf('%d (%d, %d)\n',round([loglik(k), prctile(bootloglik(k,:), 2.5, 2), prctile(bootloglik(k,:), 97.5, 2)]'))
fprintf('Multiplicative model loglik (w/ CI)\n')
k = 2; fprintf('%d (%d, %d)\n',round([loglik(k), prctile(bootloglik(k,:), 2.5, 2), prctile(bootloglik(k,:), 97.5, 2)]'))
fprintf('Log-likelihood ratio (paired w/ CI)\n')
fprintf('%.1f (%d, %d)\n', [-diff(loglik, [], 2), round(prctile(-diff(bootloglik, [], 1), 2.5, 2)), round(prctile(-diff(bootloglik, [], 1), 97.5, 2))]')
save([WriteDir sprintf('LLbootstrap_sim_%s',truenoisetype)], 'bootloglik')

% --- Additive noise data results ---
% Additive model loglik (w/ CI) <<< TRUE MODEL
% -9743 (-9898, -9622)
% Multiplicative model loglik (w/ CI)
% -10086 (-10232, -9907)
% Log-likelihood ratio (paired w/ CI)
% 343.1 (98, 525)
% 
% --- Multiplicative gain data results ---
% Additive model loglik (w/ CI)
% -5331 (-5690, -5281)
% Multiplicative model loglik (w/ CI) <<< TRUE MODEL
% -5326 (-5653, -5293)
% Log-likelihood ratio (paired w/ CI)
% -4.3 (-271, 268)