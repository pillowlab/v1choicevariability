function [Distance1D, Covariance1D] = symmxcov(AmplitudesRenormalized, Distance)
% Computes 1D cross-covariance for all pairwise distances on a 2D map
%
% (hacked from BootCalculateCorrByDistance)

% calculate Covariance matrix
[width, height, ntrials] = size(AmplitudesRenormalized);
Amplitudes = reshape(AmplitudesRenormalized,[width*height,ntrials]);
CovarianceMatrix = nancov(Amplitudes');

% bin
Distance1D = [0:0.5:max(Distance(:))];
Covariance1D = NaN(1,length(Distance1D));

for i=1:length(Distance1D)
    tempInd = abs(Distance - Distance1D(i))<=.25;
    Covariance1D(i) = nanmean(CovarianceMatrix(tempInd));
end

% trim the last 5 samples
Distance1D = Distance1D(1:end-5)';
Covariance1D = Covariance1D(1:end-5)';