% fit_scalarSDTmodel
%
% Compute ML fits for multi-contrast, variable SNR (unnormalized) data

setDirs;

% Load data

load([WriteDir 'dat_MM190805_pruned']);  % loads 'dat' with trial information
load([ReadDir 'AmpsByModel']);  % new data (from new maps)
load([WriteDir 'ii25']);  % load list of trials which were in blocks of contrast 25

mVSDI = AmpsByModel';  % output of each pooling map
NSAMP = size(mVSDI, 1);
NMODELS = size(mVSDI, 2);
modelnames = {'1 - Fitted Choice-triggered Average', ...
              '2 - Fitted Average Evoked', ...
              '3 - Gaussian (sigma = 0.5mm)',...
              '4 - Gaussian (sigma = 1.0mm)',...
              '5 - Gaussian (sigma = 2.0mm)',...
              '6 - Gaussian (sigma = 3.0mm)',...
              '7 - Gaussian (sigma = 4.0mm)',...
              '8 - Optimal (Present vs. Absent)',...
              '9 - Optimal ("Yes" vs. "No")'};
noisetype = {'additive','multiplicative'};

%% Single pass through data
[DFhat1, MFhat1, crit, loglik] = deal(nan(NMODELS, 2));
% DFerrbar1 = nan(NMODELS, 2);

    D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
    X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
    contr = dat.contr;  % contrast of that trial
    cs = unique(contr(contr>0)); % list of distinct contrasts
    ncs = length(cs); % number of contrasts
    nsess = max(dat.sessnums); % number of sessions
    prs = [];
for whichModel = 1:NMODELS % Loop over models
    fprintf('Fitting Model %s\n', modelnames{whichModel})
    Mraw = mVSDI(:,whichModel);  % measurements
    % Estimate the SNR for each experiment and contrast level
    [M,Xeff,Mwts,Mmus,Mmupred,usess,ucontr] = findExptSNRs(Mraw,X,dat);
    iinonnan = ~isnan(Mmus);

    % Prune high contrasts
    trialData = [Xeff,M,D,Mwts]; % data used for fitting
    trialData1 = trialData;
    trialData1(ii25,:) = [];    
    
    tmp = [];
    for i = 1:2
        fprintf('--- %s ---\n', [upper(noisetype{i}(1)), noisetype{i}(2:end)])
        % Fit model to all low-contrast data
        [prshat1, errbarSD1, ~, nll] = compMLest_VSDIvarSNRdata(trialData1, noisetype{i});
        DFhat1(whichModel, i) = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
        MFhat1(whichModel, i) = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
%         DFerrbar1(whichModel,:) = [(prshat1.varsh-errbarSD1(3))/(prshat1.varsh-errbarSD1(3) + prshat1.vardi+errbarSD1(2)),...
%             (prshat1.varsh+errbarSD1(3))/(prshat1.varsh+errbarSD1(3) + max(0,prshat1.vardi-errbarSD1(2)))];
        crit(whichModel, i) = prshat1.crit;
        loglik(whichModel, i) = -nll;
        tmp = [tmp, prshat1];
    end
    prs = [prs; tmp];
end

%% Compute `quantities of interest` from parameter values
sigma2_s = reshape([prs.varsh], size(prs));
sigma2_m = reshape([prs.varmi], size(prs));
sigma2_d = reshape([prs.vardi], size(prs));
sigma2_td = sigma2_s + sigma2_d;
tau = reshape([prs.crit], size(prs));
probH = normcdf((median(unique(Xeff))-tau) ./ sqrt(sigma2_td));
probM = 1 - probH;
probFA = normcdf(-tau ./ sqrt(sigma2_td));
probCR = 1 - probFA;
estimated_performance_additive = [probH(:,1), probM(:,1), probFA(:,1), probCR(:,1)];
estimated_performance_multiplicative = [probH(:,2), probM(:,2), probFA(:,2), probCR(:,2)];

gt_performance = [sum(bH)/sum(bP), sum(bM)/sum(bP) sum(bFA)/sum(bA) sum(bCR)/sum(bA)];

%% Bootstrapped multiple passes through data for each model type
% THIS WILL TAKE TIME!!
warning('off','all')
NBOOT = 1000;
[bootDFhat1, bootMFhat1, bootcrit, bootloglik] = deal(nan(NMODELS, 2, NBOOT));
% DFerrbar1 = nan(NMODELS, NBOOT, 2);

    D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
    X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
    contr = dat.contr;  % contrast of that trial
    cs = unique(contr(contr>0)); % list of distinct contrasts
    ncs = length(cs); % number of contrasts
    nsess = max(dat.sessnums); % number of sessions
for whichModel = 1:NMODELS % Loop over models
    fprintf('Fitting Model %s\n', modelnames{whichModel})
    Mraw = mVSDI(:, whichModel);  % measurements
    % Estimate the SNR for each experiment and contrast level
    [M,Xeff,Mwts,Mmus,Mmupred,usess,ucontr] = findExptSNRs(Mraw,X,dat);
    iinonnan = ~isnan(Mmus);

    % Prune high contrasts
    trialData = [Xeff,M,D,Mwts]; % data used for fitting
    
    for i = 1:2
        fprintf('--- %s ---\n[', [upper(noisetype{i}(1)), noisetype{i}(2:end)])
        for j = 1:NBOOT
            if mod(j, ceil(NBOOT/100)) == 0
               fprintf('.') 
            end
            % Resample
            resamp = randsample(NSAMP,NSAMP,true);
            trialData1 = trialData(resamp, :);
            trialData1(ii25(resamp),:) = [];
            % Fit model to all low-contrast data
            [prshat1, errbarSD1, ~, nll] = compMLest_VSDIvarSNRdata(trialData1, noisetype{i});
            bootDFhat1(whichModel, i, j) = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
            bootMFhat1(whichModel, i, j) = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
%             DFerrbar1(whichModel,:) = [(prshat1.varsh-errbarSD1(3))/(prshat1.varsh-errbarSD1(3) + prshat1.vardi+errbarSD1(2)),...
%                 (prshat1.varsh+errbarSD1(3))/(prshat1.varsh+errbarSD1(3) + max(0,prshat1.vardi-errbarSD1(2)))];
            bootcrit(whichModel, i, j) = prshat1.crit;
            bootloglik(whichModel, i, j) = -nll;
        end
        fprintf(']\n')
    end
end

save([WriteDir 'LLbootstrap'], 'bootDFhat1', 'bootMFhat1', 'bootcrit', 'bootloglik')
fprintf('Additive model loglik (w/ CI)\n')
k = 1; fprintf('%d (%d, %d)\n',round([loglik(:, k), prctile(bootloglik(:,k,:), 2.5, 3), prctile(bootloglik(:,k,:), 97.5, 3)]'))
fprintf('Multiplicative model loglik (w/ CI)\n')
k = 2; fprintf('%d (%d, %d)\n',round([loglik(:, k), prctile(bootloglik(:,k,:), 2.5, 3), prctile(bootloglik(:,k,:), 97.5, 3)]'))
fprintf('Log-likelihood ratio (paired w/ CI)\n')
fprintf('%.1f (%d, %d)\n', [-diff(loglik, [], 2), round(prctile(-diff(bootloglik, [], 2), 2.5, 3)), round(prctile(-diff(bootloglik, [], 2), 97.5, 3))]')

%% MIGRATED FROM AnalyzeWithJonathan

phit = sum(dat.bH)/sum(dat.bP);
pmiss = sum(dat.bM)/sum(dat.bP);
pfa = sum(dat.bFA)/sum(dat.bA);
pcr = sum(dat.bCR)/sum(dat.bA);

dprime = norminv(phit) - norminv(pfa);
VarTD = 1/dprime;
crit_analytic = norminv(pcr,0,VarTD);

for i=1:9
    [scci(i),ns(i),cp0(i),cp1(i),acc0(i),acc1(i)] = compStatsVsDF(DFhat1(i),MFhat1(i),phit,crit_analytic);
    [~,ns_mf1(i),cp0_mf1(i),cp1_mf1(i),acc0_mf1(i),acc1_mf1(i)] = compStatsVsDF(DFhat1(i),1,phit,crit_analytic);
end

% We report cp1/cp1_mf1 and acc1/acc1_mf1 in Table 1

%%

%**********************************************************************
% ******* 6-Panel Figure **********************************************
%**********************************************************************
% values taken from the results of script1_fitModel_variableSNRdata.m

% ourPhit = sum(dat.bH)/sum(dat.bH|dat.bM|dat.bFA|dat.bCR);   % = 0.3366
% ourCrit = crit(1);                      % = 0.0802
% ourDF = DFhat1(1);                      % = 0.7724
% ourMF = 1;                              % = 1.0000
% DFs = DFhat1(1);                        % = 0.7724
% f = Create6PanelFigure(ourPhit,ourCrit,ourDF,ourMF,DFs);
