function prs0 = initializeSDTmodelprs(Dat, noisetype)
% prs0 = initializeSDTmodelprs(Dat)
%
% Use grid search to determine good initial setting for VSDI signal-detection model
% parameters.
%
% Estimates initial setting of parameters:
%    varmi - indep measurement noise
%    vardi - indep decision noise
%    varsh - shared sensory noise
%    crit0 - criterion

if nargin == 1
    noisetype = 'additive';
end
ntrials = size(Dat,1);
iix1 = (Dat(:,1)>0);
iix0 = (Dat(:,1)==0);
iid1 = (Dat(:,3)==1);
iid0 = (Dat(:,3)==0);

%% Compute total measurement variance
nx1 = sum(iix1); 
nx0 = sum(iix0);
vartm0 = (nx1*var(Dat(iix1,2))+nx0*var(Dat(iix0,2)))/ntrials;

%% Determine criterion & total decision noise
crit0 = 0.1:.01:.8;  % possible values for criterion
p1gx1 = sum(iix1&iid1)/nx1;
p1gx0 = sum(iix0&iid1)/nx0;
sigtd_x1 = (1-crit0)/norminv(p1gx1); % from hits
sigtd_x0 = (0-crit0)/norminv(p1gx0); % from FAs

[~,icrit] = min(abs(sigtd_x1-sigtd_x0));  % pick crit where estimated sigtds are equal
crit0 = crit0(icrit);  % set initial guess at criterion
vartd0 = sigtd_x1(icrit).^2; % total decision noise variance

%% set initial guess at shared noise var
maxsh = max(0.1,min(vartm0,vartd0));
nvals = 10;
dfrac = maxsh/nvals;
ff = dfrac/2:dfrac:maxsh;
negll = zeros(nvals,1);
for jj = 1:nvals
    vsh = ff(jj);
    if strcmp(noisetype, 'additive')
        negll(jj) = neglogli_scalaradditiveSDTmodel([vartm0-vsh;vartd0-vsh;vsh;crit0],Dat);
    elseif strcmp(noisetype, 'multiplicative')
        negll(jj) = neglogli_scalarmultiplicativeSDTmodel([vartm0-vsh;vartd0-vsh;vsh;crit0],Dat);
    end
%     negll(jj) = neglogli_SDTmodel_variableSNRdata([vartm0-vsh;vartd0-vsh;vsh;crit0],Dat);
end
[~,jjmin] = min(negll);
vsh0 = ff(jjmin);
vmi0 = vartm0-vsh0;
vdi0 = vartd0-vsh0;

% initial parameters guess
prs0 = [vmi0; vdi0; vsh0; crit0];
