% step1_ExtractUnnormalizedData.m
%
% Do pre-processing on Chuck's VSDI data to extract unnormalized data (1 scalar for each
% trial), and extract only the variables we need to reduce memory load.
%
% JP: 4/7/2014
% MM: 8/5/2019

if ~exist('Data','var')
    a0_setDirs;

    %******* load Preprocessed data *******************************************
    load([ReadDir 'Preprocessed.mat']);
    load([ReadDir 'Inclusion.mat']);
else
    %******* erase everything except Preprocessed data ************************
    clearvars -except Alignment Data IntegrationTimes Inclusion SummaryByRunAndContrast *Dir
end

%******* set parameters for analysis **************************************
%**************************************************************************
sParameters = Parameters();
sParameters.bAnalyzeNormalizedData = false;
sParameters.bIncludeHighContrast = true;
sParameters.bRestrictAnalysisToOriginallyIncludedExperiments = false;
%bInclude = Inclusion.bPassesCurrentInclusionCriteria;
bInclude = true(size(Data.bH));

% include high contrasts or not
if ~sParameters.bIncludeHighContrast
    bInclude(Data.Contrast > 20) = false;
end

% include threshold contrasts or not
if ~sParameters.bIncludeThresholdContrasts
    bInclude(Data.Contrast < 20) = false;
end

Data.bOrigInclusion = Inclusion.bOriginalSubmission;

%******* pull out commonly used variables for convenience *****************

bH = Data.bH;
bM = Data.bM;
bFA = Data.bFA;
bCR = Data.bCR;
bP = bH|bM;
bA = bFA|bCR;
width = size(Data.TC,1);
height = size(Data.TC,2);
frames = size(Data.TC,3);
ntrials = size(Data.TC,4);

% ===== Insert useful variables into new struct =======

dat.bH = bH';
dat.bM = bM';
dat.bFA = bFA';
dat.bCR = bCR';
dat.bP = bP';
dat.bA = bA';
dat.contr = Data.Contrast';
dat.runnum = Data.RunNum;
dat.date = Data.Date;
dat.monkey = zeros(size(Data.Monkey));
dat.monkey(Data.Monkey=='T') = 1;
dat.monkey(Data.Monkey=='C') = 2;
dat.bOrigInclusion= Data.bOrigInclusion';

StartingList = unique([dat.date(bInclude) dat.monkey(bInclude)],'rows')
numTex = size(StartingList(StartingList(:,end)==1,:),1)
numCharlie = size(StartingList(StartingList(:,end)==2,:),1)


% Load newest unbiased scalars
load([WriteDir 'UnbiasedScalars.mat']);
dat.scalars = UnbiasedScalars.Scalars';

% ===  Other pruning ========
% indicate trials with contrast = 100
ii100 = (dat.contr == 100);
bInclude(ii100) = false;

str = unique(dat.monkey);
for j = 1:length(str)
    dat.monkey(str(j)==dat.monkey) = j;
end

dat = pruneindices(dat,~bInclude);

% ==== SAVE ====    
%save dat_NewInclusion dat bInclude
save([WriteDir 'dat_MM190805'],'dat','bInclude');
