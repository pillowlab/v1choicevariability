% using unbiased decoder to generate scalars for jonathan
% MM: 8/4/2019

a0_setDirs;

%**************************************************************************
%******* load Preprocessed data *******************************************
%**************************************************************************
load([ReadDir 'Preprocessed.mat']);
load([ReadDir 'Inclusion.mat']);


% pull out the variables of interest
Amplitudes = Data.Amplitudes;
bP = Data.bH | Data.bM;
bA = Data.bFA | Data.bCR;

% compute mean difference
meanP = nanmean(Amplitudes(:,:,bP),3);
meanA = nanmean(Amplitudes(:,:,bA),3);
PresentMap = meanP - meanA;

% fit
Coordinates.X = [1:size(Data.Amplitudes,1)];
Coordinates.Y = [1:size(Data.Amplitudes,2)];
[FitValuesPresentMap, Rsquared] = FitGaussian2D(Coordinates,PresentMap)
FittedPresentMap = FuncGaussian2D(FitValuesPresentMap, Coordinates);

% plot
f = figure;
subplot(1,2,1); imagesc(PresentMap'); axis square;
subplot(1,2,2); imagesc(FittedPresentMap'); axis square;

% project amplitude maps onto this map to get scalars
NTRIALS = size(Amplitudes,3);
myWeights = FittedPresentMap;
myWeights = myWeights ./ nansum(nansum(myWeights));
Scalars = squeeze(nanmean(nanmean(Amplitudes .* repmat(myWeights,[1 1 NTRIALS]))))';

% sanity check the scalars
AMean = nanmean(Scalars(bP));
BMean = nanmean(Scalars(bA));
ASTD = nanstd(Scalars(bP));
BSTD = nanstd(Scalars(bA));
DPrime = (AMean-BMean)./sqrt((ASTD.^2+BSTD.^2)/2)

% collect key data into a structure
UnbiasedScalars.Scalars = Scalars;
UnbiasedScalars.bP = bP;
UnbiasedScalars.bA = bA;
UnbiasedScalars.Contrast = Data.Contrast;
UnbiasedScalars.PresentMap = PresentMap;
UnbiasedScalars.FittedPresentMap = FittedPresentMap;
UnbiasedScalars.Coordinates = Coordinates;
UnbiasedScalars.FitValuesPresentMap = FitValuesPresentMap;


% %**************************************************************************
% %******* Save the Data ****************************************************
% %**************************************************************************
eval(sprintf('save(''%s%s'', ''UnbiasedScalars'', ''-mat'', ''-v7.3'');',...
    WriteDir,'UnbiasedScalars.mat'));



