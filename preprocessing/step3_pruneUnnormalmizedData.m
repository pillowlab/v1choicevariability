% step2_pruneUnNormData.m
% 
% Compute cumulative block numbers, relevant high-contrast blocks, and remove blocks of 
% trials that don't have a relevant high-contrast block before them.  
%
% JP: 4/7/2014
% MM: 8/5/2019

a0_setDirs;


load([WriteDir 'dat_MM190805']);

% Prune out 100% contrast trials
ntrials = length(dat.bH);

%% Extract number for each date
dates = unique(dat.date);
ndates = length(dates);
datenum = zeros(ndates,1);
for j = 1:ndates
    datenum(dat.date==dates(j)) = j;
end

%% Assign unique block number to each trial
nblocks = 0;
blocknums = dat.runnum;
for j = 1:ndates
    ii = find(datenum==j);
    runnums = dat.runnum(ii);
    uniquerunnums = unique(runnums);
    nnums = length(uniquerunnums);
    orderedrunnums = runnums*0;
    for i = 1:nnums
        jj = (runnums==uniquerunnums(i));
        orderedrunnums(jj) = i;
    end
    
    blocknums(ii) = nblocks+orderedrunnums;
    nblocks = nblocks+nnums;
    if min(diff(blocknums(1:ii(end))))<0
        keyboard;
    end
end

% Extract contrasts used in each block
blockcontr = cell(nblocks,1);
blockcontrmax = zeros(nblocks,1);
blockcontrmaxinds = zeros(ntrials,1);
for j = 1:nblocks
    ii = (blocknums==j);
    blockcontr{j} = unique(dat.contr(ii));
    blockcontrmax(j) = max(blockcontr{j});
    blockcontrmaxinds(ii) = blockcontrmax(j);
end

%% Inspect raw data

% Make sure it's true that 25% contrast starts each day
newdays = [false;diff(dat.date)>0];
plot(1:ntrials,blocknums/nblocks,1:ntrials,dat.contr/max(dat.contr),...
    1:ntrials,newdays,'r', 1:ntrials, blockcontrmaxinds/25,'k--');

%% Now get rid of blocks that are 25% followed by another 25% 

blocksToPrune = find((blockcontrmax(1:end-1)==25) & (blockcontrmax(2:end)==25));
indsToPrune = false(ntrials,1);
for j = 1:length(blocksToPrune);
    indsToPrune(blocknums==blocksToPrune(j))=true;
end
hold on;
plot(1:ntrials,indsToPrune,'c','linewidth',1);
hold off;

% Prune data to only the relevant blocks, then resave
dat = pruneindices(dat,indsToPrune);
bInclude = ~indsToPrune;

%%%%%% Chuck debug check
CurrentList = unique([dat.date dat.monkey],'rows')
numTex = size(CurrentList(CurrentList(:,end)==1,:),1)
numCharlie = size(CurrentList(CurrentList(:,end)==2,:),1)
%%%%%%

% save here if desired
% save dat_NewInclusion_pruned0 dat bInclude;

% ===============================
% Part II:  extract info about session numbers
% ===============================

% Compare old fitting script on normalized and new script on unnormalized
% data

ntrials = length(dat.bH);

% Extract number for each date
dates = unique(dat.date);
ndates = length(dates);
datenum = zeros(ndates,1);
for j = 1:ndates
    datenum(dat.date==dates(j)) = j;
end

% Assign unique block number to each trial
nblocks = 0;
blocknums = dat.runnum;
for j = 1:ndates
    ii = find(datenum==j);
    runnums = dat.runnum(ii);
    uniquerunnums = unique(runnums);
    nnums = length(uniquerunnums);
    orderedrunnums = runnums*0;
    for i = 1:nnums
        jj = (runnums==uniquerunnums(i));
        orderedrunnums(jj) = i;
    end
    
    blocknums(ii) = nblocks+orderedrunnums;
    nblocks = nblocks+nnums;
    if min(diff(blocknums(1:ii(end))))<0
        keyboard;
    end
end

% Extract contrasts used in each block
blockcontr = cell(nblocks,1);
blockcontrmax = zeros(nblocks,1);
blockcontrmaxinds = zeros(ntrials,1);
for j = 1:nblocks
    ii = (blocknums==j);
    blockcontr{j} = unique(dat.contr(ii));
    blockcontrmax(j) = max(blockcontr{j});
    blockcontrmaxinds(ii) = blockcontrmax(j);
end

%% Inspect raw data

% Make sure it's true that 25% contrast starts each day
newdays = [false;diff(dat.date)>0];
plot(1:ntrials,blocknums/nblocks,1:ntrials,dat.contr/max(dat.contr),...
    1:ntrials,newdays,'r', 1:ntrials, blockcontrmaxinds/25,'k--');

%% Now define "sessions", defined as groups of blocks beginning with a 25% block

startblocks = find(blockcontrmax(1:end-1)==25);
nsessions = length(startblocks);
sessnums = zeros(ntrials,1);
for j = 1:nsessions
    sessnums(blocknums>=startblocks(j)) = j;
end
hold on;
plot(1:ntrials,sessnums/nsessions, 'c','linewidth', 3);
hold off;

dat.datenum = datenum;
dat.blocknums = blocknums;
dat.sessnums = sessnums;
dat.blockcontr = blockcontr;
dat.blockcontrmax = blockcontrmax;
dat.blockcontrmaxinds = blockcontrmaxinds;

% ==== SAVE ====    

save([WriteDir 'dat_MM190805_pruned'],'dat','bInclude');