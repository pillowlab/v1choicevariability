% step3_estimateSNRandContrEffects
%
% updated: 4/8/14 (JP)
% MM: 8/5/19

a0_setDirs;

%addpath ..;
load([WriteDir 'dat_MM190805_pruned']);
load([WriteDir 'ii25']);

ntrials = length(dat.bH);


%%  Load data
M = dat.scalars;  % measured value for each contr
D = (dat.bH|dat.bFA);   % decision (1=yes or 0=no)
X = (dat.bH|dat.bM);   % stimulus (1=present or 0=absent)
contr = dat.contr;  % contrast of that trial
cs = unique(contr); cs = cs(cs>0);  % list of distinct contrasts
ncs = length(cs); % number of contrasts
nsess = max(dat.sessnums); % number of sessions

% %%  Ensure mean of no-stimulus responses is zero for each experiment
% muZeroSess = zeros(nsess,1);
% for i = 1:nsess
%     muZeroSess(i) = mean(M((dat.sessnums==i)&~X));
%     M((dat.sessnums==i)) = M((dat.sessnums==i))-muZeroSess(i);
% end
% muZeroSess'

%% Estimate the SNR for each experiment and contrast level

[Xeff,Mwts,Mmus,usess,ucontr,Mstds,ntrialPerContext] = extractSNRandContrastEffects(M,dat);

% Compute predicted mean response to each contrast for each experiment
Mmupred = usess*ucontr';
maxMmupred = max(abs(Mmupred(:)));
usess = usess/sqrt(maxMmupred);
ucontr = ucontr/sqrt(maxMmupred);
M = M/maxMmupred;
Mmupred = usess*ucontr';
iinonnan = ~isnan(Mmus);

%% === Plot results of rank-1 fit to means (expt by contrast) =====
subplot(231);
Mmusavg = nanmean(Mmus); Mmusavg = Mmusavg/max(Mmusavg);
plot(1:ncs,ucontr, '.-',1:ncs,Mmusavg,'ro');
title('mean rsponse vs. contrast');
xlabel('contrast');
ylabel('mean resp to signal');
set(gca,'tickdir','out','xtick',1:ncs,'xticklabel',cs);
axis([0 ncs+1,0 1.1]);
box off; 

subplot(2,3,2:3);
plot(usess,'o-');  
title('SNR vs. session #');
xlabel('session #');
ylabel('mean response');
box off; set(gca,'tickdir','out')
hold on;
for j = 1:nsess
    plot(j,Mmus(j,:), 'r.');
end
plot(1:nsess,Mmus(:,end),'r-');
hold off;
axis tight;

subplot(234);
imagesc(Mmus);
title('observed mean');
xlabel('contrast');
ylabel('session #');
set(gca,'tickdir','out','xtick',1:ncs,'xticklabel',cs);

subplot(235);
Mmupredsprs = Mmupred;
Mmupredsprs(~iinonnan) = nan;
imagesc(Mmupredsprs);
colormap gray;
title('estimated mean');
xlabel('contrast');
set(gca,'tickdir','out','xtick',1:ncs,'xticklabel',cs);

subplot(236);
plot([0 1], [0 1], 'k', Mmus(iinonnan), Mmupred(iinonnan),'o',...
    Mmus(:,end),Mmupred(:,end),'g.');
xlabel('observed mean M'); ylabel('modeled mean M');
axis equal;
axis tight; 

%% Put in contrast and SNR for each experiment

% Contrast of each stimulus
Xeff = double(X);
for j = 1:ncs
    Xeff(contr==cs(j))=ucontr(j);
end

% SNR of each experiment
Mwts = zeros(ntrials,1);
for j = 1:nsess
    Mwts(dat.sessnums==j) = usess(j);
end

%% Fit Unnormalized model
% Dat = [Xeff,M,D,Mwts];

dat.nrmSNR = Mwts;
dat.nrmContr = Xeff;

%save dat_NewInclusion_processed dat bInclude;

save([WriteDir 'dat_MM190805_processed'],'dat', 'bInclude');
