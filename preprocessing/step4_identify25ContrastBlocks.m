% MM: 8/5/2019

a0_setDirs;

load([WriteDir 'dat_MM190805_pruned']);

ntrials = length(dat.bH);

ii25 = [];
for jsess = 1:max(dat.sessnums)
    iis = (dat.sessnums==jsess);
    rns = dat.runnum(iis);
    cs = dat.contr(iis);
    for jrun = 1:max(rns)
        jjr = (rns==jrun);
        if any(cs(jjr)>=25)
            ii25 = [ii25; true(sum(jjr),1)];
        else
            ii25 = [ii25; false(sum(jjr),1)];
        end
    end
end
ii25 = logical(ii25);
%save ii25 ii25;
save([WriteDir 'ii25'],'ii25');
