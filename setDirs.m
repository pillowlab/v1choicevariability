clear all;
close all;
% MM: 8/4/2019

set(0,'defaultaxesfontname','Helvetica','defaultaxesfontsize',14,'DefaultFigureColor','w')

%**************************************************************************
%******* set all paths ****************************************************
%**************************************************************************
CodeDir = fullfile(getenv('HOME'),'Dropbox (Princeton)/collabprojects/VSD-CP/Current-Mike/code/');
ReadDir = fullfile(getenv('HOME'),'Documents/data/VSD-CP/');
WriteDir = fullfile(getenv('HOME'),'Dropbox (Princeton)/collabprojects/VSD-CP/Current-Mike/code/data/');

addpath(CodeDir);
addpath(ReadDir);
addpath(WriteDir);
addpath(fullfile(CodeDir,'legacy'));
addpath(fullfile(CodeDir,'misc'));
addpath(fullfile(CodeDir,'models'));
addpath(fullfile(CodeDir,'preprocessing'));
addpath(fullfile(CodeDir,'supplementary'));
cd(CodeDir);

[BLUE, RED, PURPLE, GREEN, YELLOW] = get_colors;
