function [bPs, bAs, bHs, bFAs, bMs, bCRs] = decodebehavior(AmplitudesSimulated, decoder, THRESH)

% Get fixed parameters
NTRIALS = size(AmplitudesSimulated, 3);

% Average weighted by the decoder to yield decision variables (DVs)
DVs = squeeze(nansum(nansum(bsxfun(@times, AmplitudesSimulated, decoder),1),2)) ./ nansum(nansum(decoder));
%       Note: This could just be the "SuperPixel" area (central 2.5 x 2.5)

% Threshold into decisions
bPs  = ((1:NTRIALS)' <= NTRIALS/2); % by design
bAs  = ((1:NTRIALS)' >  NTRIALS/2);
bHs  = (DVs >= THRESH) & bPs; % over thresh & target present = HIT
bFAs = (DVs >= THRESH) & bAs; % over thresh & target absent = FA
bMs  = (DVs <  THRESH) & bPs; % under thresh & target present = MISS
bCRs = (DVs <  THRESH) & bAs; % under thresh & target absent = CR

end

