function loss = loss_full(x, SIGNAL, DECODER, SEED, MULTTYPE, LAMBDA, DISTANCE, PERFORMANCES, GTCORRELATION)

[width, height, NTRIALS] = size(SIGNAL);
[logell, logrho, logk, tau] = deal(x(1), x(2), x(3), x(4));
% ell > 0           EXP
% rho > 0           EXP
% k > 0             EXP
% 0 < tau < 1       LOGISTIC

% Simulate noise constant over iterations of fmin (fixed seed)
%   Multiplicative noise
rng(SEED);
gain = gamrnd(exp(logk), 1/exp(logk), 1,1,NTRIALS);
%   Additive noise
rng(SEED);
K = @(L) exp(-(DISTANCE ./ (exp(L)^2))); % Gaussian kernel
noise = mvnrnd(zeros(size(DISTANCE,1),1), K(logell), NTRIALS);
noise = permute(reshape(noise, [NTRIALS width height]), [3 2 1]);

% Simulate VSDI activity
switch lower(MULTTYPE)
    case 'signalonly' % multiplicative gain on signal only
        AmplitudesSimulated = bsxfun(@times, gain, SIGNAL) + exp(logrho) .* noise;
    case 'signalnoise' % multiplicative gain on signal and noise
        AmplitudesSimulated = bsxfun(@times, gain, SIGNAL + exp(logrho) .* noise);
end

% Simulate behavior
thresh = 1/(1+exp(-tau));
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, DECODER, thresh);
% Compute performance measures
phit = sum(bHs)/sum(bPs);
pmiss = sum(bMs)/sum(bPs);
pfa = sum(bFAs)/sum(bAs);
pcr = sum(bCRs)/sum(bAs);
Phat = [phit pmiss pfa pcr];

% Measure spatial correlations
[~, Correlation1D_P] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), DISTANCE);
[~, Correlation1D_A] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), DISTANCE);
Correlation1D = [Correlation1D_P Correlation1D_A];
% Compute MSE correlation loss from the true data spatial correlations
xcorr_loss = sum(sum((GTCORRELATION - Correlation1D).^2))/numel(GTCORRELATION);

% Compute MSE performance loss from true data percent correct
performance_loss = sum((PERFORMANCES - Phat).^2)/numel(PERFORMANCES);

% Loss for performance-xcorr L2-constrained optimization program
loss = performance_loss + LAMBDA * xcorr_loss;

end

