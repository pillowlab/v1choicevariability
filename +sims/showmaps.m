function showmaps(AmplitudesSimulated, bP, bA, bH, bFA, bM, bCR)

% Compute maps, noting that the "weights" here are vestigial
Weights = ones(numel(bP),1);
[PresentMap, PresentVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bP);
[AbsentMap, AbsentVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bA);
[HitMap, HitVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bH);
[MissMap, MissVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bM);
[FalseAlarmMap, FalseAlarmVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bFA);
[CorrectRejectMap, CorrectRejectVar] = ComputeWeightedMeanMap(AmplitudesSimulated,Weights,bCR);
HitVMissMap = HitMap - MissMap;
HitVMissVar = zeros(size(HitMap));
FalseAlarmVCorrectRejectMap = FalseAlarmMap - CorrectRejectMap;
FalseAlarmVCorrectRejectVar = zeros(size(HitMap));

plots.maps;
set(f1,'Color','white','units','inches','Position',[1 1 7 5]);
% set(f2,'Color','white','units','inches','Position',[1 1 7 5]);

end

