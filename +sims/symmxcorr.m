function [Distance1D, Correlation1D] = symmxcorr(AmplitudesRenormalized, Distance)
% Computes 1D cross-correlation for all pairwise distances on a 2D map
%
% (hacked from BootCalculateCorrByDistance)

% calculate correlation matrix
[width, height, ntrials] = size(AmplitudesRenormalized);
Amplitudes = reshape(AmplitudesRenormalized,[width*height,ntrials]);
CorrelationMatrix = corrcoef(Amplitudes');

% bin
Distance1D = [0:0.5:max(Distance(:))];
Correlation1D = NaN(1,length(Distance1D));

for i=1:length(Distance1D)
    tempInd = abs(Distance - Distance1D(i))<=.25;
    Correlation1D(i) = nanmean(CorrelationMatrix(tempInd));
end

% trim the last 5 samples
Distance1D = Distance1D(1:end-5)';
Correlation1D = Correlation1D(1:end-5)';