function loss = loss_signalgain(logk, signal, decoder, seed, PERFORMANCE, THRESH)

% Simulate full VSDI activity
rng(seed);
NTRIALS = size(signal,3);

gain = gamrnd(exp(logk), 1/exp(logk), 1,1,NTRIALS);
AmplitudesSimulated = bsxfun(@times, gain, signal); % + exp(logk) .* noise;
% Simulate behavior
[~, ~, bHs, ~, ~, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, THRESH);

% Compute percent correct
correct = (bHs | bCRs);
PC = sum(correct) / numel(correct);
% Squared difference from observed percent correct
loss = (PERFORMANCE - PC)^2;

end

