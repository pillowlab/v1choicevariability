function loss = loss_lengthscale(logell, K, Distance, GTCorrelation1D, signal, seed)

% Get fixed parameters, set the seed to draw the same random numbers on
% every iteration of fmin
rng(seed);
[width, height, NTRIALS] = size(signal);

% Generate noise
noise = mvnrnd(zeros(size(Distance,1),1),K(logell),NTRIALS);
noise = permute(reshape(noise,[NTRIALS width height]),[3 2 1]);
AmplitudesSimulated = noise + signal;

% Measure its spatial correlations
[~, Correlation1D_P] = sims.symmxcorr(AmplitudesSimulated(:,:,1:(NTRIALS/2)), Distance);
[~, Correlation1D_A] = sims.symmxcorr(AmplitudesSimulated(:,:,(NTRIALS/2+1):end), Distance);
Correlation1D = [Correlation1D_P Correlation1D_A];

% Compute MSE loss to the true data spatial correlations
loss = sum(sum((Correlation1D - GTCorrelation1D).^2))/numel(Correlation1D);

end

