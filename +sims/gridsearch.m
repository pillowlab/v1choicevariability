function [xopt, fopt] = gridsearch(f, xint, N)
% as a plug-in substitute for the fmin suite

% initial round
x = linspace(xint(1), xint(2), 100); losses = nan(size(x));
for k = 1:numel(x), losses(k) = f(x(k)); end
[~, xmi] = min(losses);

% subsequent rounds
for n = 1:(N-1)
    x = linspace(x(xmi-1), x(xmi+1), 100); losses = nan(size(x));
    for k = 1:numel(x), losses(k) = f(x(k)); end
    [~, xmi] = min(losses);
end

% return the optimum
xopt = x(xmi);
fopt = losses(xmi);

end