function loss = loss_magnitude(logrho, noise, signal, decoder, PERFORMANCES, THRESH)

% Simulate full VSDI activity
AmplitudesSimulated = signal + exp(logrho) .* noise;
% Simulate behavior
[bPs, bAs, bHs, bFAs, bMs, bCRs] = sims.decodebehavior(AmplitudesSimulated, decoder, THRESH);

% Compute percent correct
phit = sum(bHs)/sum(bPs);
pmiss = sum(bMs)/sum(bPs);
pfa = sum(bFAs)/sum(bAs);
pcr = sum(bCRs)/sum(bAs);
P = [phit pmiss pfa pcr].*0.5;
% Squared difference from observed performances
loss = sum((PERFORMANCES - P).^2);

end

