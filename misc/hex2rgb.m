function rgb = hex2rgb(hex)
        rgb = reshape(sscanf(hex.','%2x'),3,[]).'/255;
