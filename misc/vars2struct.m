function out = vars2struct(vars)
    %This function is designed to store copies of variables as fields of a
    %structure. The idea behind this is that it enables you to quickly put
    %a bunch of variables in the workspace belonging to one analysis under
    %one 'heading' so they aren't overwritten. 'vars' should be a string or
    %cell array of strings that are variable names.    ACS 31May2012
   if ~iscell(vars),vars={vars};end;
   for v = 1:length(vars),out.(vars{v})=evalin('caller',vars{v});end;