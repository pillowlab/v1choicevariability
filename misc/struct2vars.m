function struct2vars(s)
    %This function is designed to unpack copies of variables from fields of
    %a structure. The idea behind this is that it enables you to quickly
    %assign a bunch of variables in the workspace belonging to one analysis
    %under one 'heading'. See vars2struct for more info. -ACS 06Feb2014
    fields = fieldnames(s);
    for fx = 1:numel(fields),
        assignin('caller',fields{fx},s.(fields{fx}));
    end;