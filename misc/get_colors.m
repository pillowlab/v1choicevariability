function [BLUE, RED, PURPLE, GREEN, YELLOW, ALTBLUE, ALTRED] = get_colors()

BLUE = hex2rgb('2D6199');
RED  = hex2rgb('B2353B');
PURPLE = hex2rgb('896782');
GREEN  = hex2rgb('658C64');
YELLOW = hex2rgb('FFE57F');

ALTBLUE = hex2rgb('1C3D60');
ALTRED  = hex2rgb('581A1D');
