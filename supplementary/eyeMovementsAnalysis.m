
setDirs;


% ******* load Preprocessed data *******************************************
load([ReadDir 'Preprocessed.mat']);
load([ReadDir 'Inclusion.mat']);

% ******* set parameters for analysis **************************************
sParameters = Parameters();

% ******* reduce data ******************************************************
% First reduction in trials: 10163 --> 10153
load([WriteDir 'dat_MM190805.mat']);
Amplitudes = Data.Amplitudes(:,:,bInclude);
SPTimeCourse = Data.SPTimeCourse(:,bInclude);
RT = Data.RT(bInclude);
Xs = Data.Xs(bInclude,:);
Ys = Data.Ys(bInclude,:);
TargCtrX = Data.TargCtrX(bInclude);
TargCtrY = Data.TargCtrY(bInclude);
AdjustmentX = Data.AdjustmentX(bInclude);
AdjustmentY = Data.AdjustmentY(bInclude);
% All of these variables are __ x 10153 trials
clear dat;
clear bInclude;

% Second reduction in trials: 10163 --> 10153 --> 9979, OK!
load([WriteDir 'dat_MM190805_processed.mat']);
Amplitudes = Amplitudes(:,:,bInclude);
SPTimeCourse = SPTimeCourse(:,bInclude);
RT = RT(bInclude);
Xs = Data.Xs(bInclude,:);
Ys = Data.Ys(bInclude,:);
TargCtrX = Data.TargCtrX(bInclude);
TargCtrY = Data.TargCtrY(bInclude);
AdjustmentX = Data.AdjustmentX(bInclude);
AdjustmentY = Data.AdjustmentY(bInclude);

Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

bInclude = bInclude(bInclude);

%%%%%% debug check
CurrentList = unique([dat.date dat.monkey],'rows')
numTex = size(CurrentList(CurrentList(:,end)==1,:),1)
numCharlie = size(CurrentList(CurrentList(:,end)==2,:),1)
%%%%%%

%%
% ******* remove high contrast trials and choose monkey(s) *****************
bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
bMyMonkey = dat.monkey == 1 | dat.monkey == 2;
% bMyMonkey = dat.datenum == 27;
bInclude = bMyMonkey & ~bIsInHighContrastBlock;

bP = dat.bP & bInclude;
bA = dat.bA & bInclude;
bH = dat.bH & bInclude;
bM = dat.bM & bInclude;
bFA = dat.bFA & bInclude;
bCR = dat.bCR & bInclude;

nrmContrResampled = dat.nrmContr;
nrmContrResampled(bA) = randsample(nrmContrResampled(bP),sum(bA),'true');
RenormalizationWeightsSpace = permute(repmat(nrmContrResampled,[1 17 17]),[2 3 1]);
AmplitudesRenormalized = Amplitudes ./ RenormalizationWeightsSpace;
RenormalizationWeightsTime = permute(repmat(nrmContrResampled,[1 120]),[2 1]);
SPTimeCourseRenormalized = SPTimeCourse ./ RenormalizationWeightsTime;

SuperPixelAmps = AmplitudesRenormalized(7:11,7:11,:);
SuperPixelAmps = squeeze(mean(mean(SuperPixelAmps,2),1));

%% Load data formatted for model-fitting
    % Written intermediate to "c1_fitModelData.m"
load([WriteDir 'eyeMvts_trialData.mat']);

%% Eye Movements Analysis
B = 10000;

% First load the data, reduce, choose monkey(s), etc.
Coordinates.X = [1:size(Amplitudes,1)];
Coordinates.Y = [1:size(Amplitudes,2)];

% Get fixation stats
[timeAxisEyes, FixPointX, FixPointY, ...
    DistanceFromFP, WithinTrialFixationSD] = ...
    ComputeEyeMovementStatisticsLite(Xs,Ys,TargCtrX,TargCtrY,AdjustmentX,AdjustmentY,...
    RT,bH,bM,bFA,bCR,sParameters);

% Descriptive Stats
fprintf('Fixation Metrics: fixation std: %.4f += %.4f, distance from fixpoint: %.4f += %.4f\n\n',...
    mean(WithinTrialFixationSD),...
    std(WithinTrialFixationSD),...
    mean(DistanceFromFP),...
    std(DistanceFromFP))

% Partition Best vs Worst
bBestWithin = WithinTrialFixationSD <= median(WithinTrialFixationSD);
bWorstWithin = WithinTrialFixationSD > median(WithinTrialFixationSD);
bBestAcross = DistanceFromFP <= median(DistanceFromFP);
bWorstAcross = DistanceFromFP > median(DistanceFromFP);

for kk = 1:4
    switch kk
        case 1
            % H within
            fprintf('HITS WITHIN-----------------------------\n')
            indBest = bH & bBestWithin';
            indWorst = bH & bWorstWithin';
            nOutcomeBest = sum(bH & bBestWithin');
            nBest = sum((bH | bM) & bBestWithin');
            nOutcomeWorst = sum(bH & bWorstWithin');
            nWorst = sum((bH | bM) & bWorstWithin');
            
        case 2
            % FA within
            fprintf('FALSE ALARMS WITHIN-----------------------------\n')
            indBest = bFA & bBestWithin';
            indWorst = bFA & bWorstWithin';
            nOutcomeBest = sum(bFA & bBestWithin');
            nBest = sum((bFA | bCR) & bBestWithin');
            nOutcomeWorst = sum(bFA & bWorstWithin');
            nWorst = sum((bFA | bCR) & bWorstWithin');
            
        case 3
            % H across
            fprintf('HITS ACROSS-----------------------------\n')
            indBest = bH & bBestAcross';
            indWorst = bH & bWorstAcross';
            nOutcomeBest = sum(bH & bBestAcross');
            nBest = sum((bH | bM) & bBestAcross');
            nOutcomeWorst = sum(bH & bWorstAcross');
            nWorst = sum((bH | bM) & bWorstAcross');
            
        case 4
            % FA across
            fprintf('FALSE ALARMS ACROSS-----------------------------\n')
            indBest = bFA & bBestAcross';
            indWorst = bFA & bWorstAcross';
            nOutcomeBest = sum(bFA & bBestAcross');
            nBest = sum((bFA | bCR) & bBestAcross');
            nOutcomeWorst = sum(bFA & bWorstAcross');
            nWorst = sum((bFA | bCR) & bWorstAcross');
    end
    
    % bootstrap behavior (prob Hit/FA)
    [bootPOutcomeBest POutcomeBest LO95 HI95 LO99 HI99] = BootPC(nOutcomeBest,nBest,B);
    [bootPOutcomeWorst POutcomeWorst LO95 HI95 LO99 HI99] = BootPC(nOutcomeWorst,nWorst,B);
    bootDiff = bootPOutcomeBest - bootPOutcomeWorst;
    fprintf('prob best = %.2f (%.2f, %.2f)\n',nOutcomeBest/nBest,quantile(bootPOutcomeBest,[0.025 0.975]));
    fprintf('prob worst = %.2f (%.2f, %.2f)\n',nOutcomeWorst/nWorst,quantile(bootPOutcomeWorst,[0.025 0.975]));
    fprintf('bootstrap p = %.4f\n\n',sum(bootDiff < 0)/B);
    
    % bootstrap VSDI amplitude <---- this is definitely wrong...
%     AA = 1000*SuperPixelAmps((bH | bM) & bBestWithin'); % <---- I can't replace any indices here and replicate the data.
%     BB = 1000*SuperPixelAmps((bH | bM) & bWorstWithin');
    AA = 1000*SuperPixelAmps(indBest); % <---- I can't replace any indices here and replicate the data.
    BB = 1000*SuperPixelAmps(indWorst);    
    bootVals = BootMeanDifference(AA,BB,B);
%     fprintf('vsdi amplitude best = %.2f (%.2f, %.2f)\n',mean(AA),quantile(AA,[0.025 0.975]));
%     fprintf('vsdi amplitude worst = %.2f (%.2f, %.2f)\n',mean(BB),quantile(BB,[0.025 0.975]));
%     fprintf('bootstrap p = %.4f\n\n',sum(bootVals<0)/B);
    [~, m, ~, ~, lo, hi, ~, ~] =  BootMean(AA, B);
    fprintf('vsdi amplitude best = %.2f (%.2f, %.2f)\n', mean(AA), lo, hi);
    [~, m, ~, ~, lo, hi, ~, ~] =  BootMean(BB, B);
    fprintf('vsdi amplitude worst = %.2f (%.2f, %.2f)\n', mean(BB), lo, hi);
    fprintf('bootstrap p = %.4f\n\n',sum(bootVals<0)/B);
    
    [~, s, lo, hi, ~, ~] =  BootSTD(AA, B);
    fprintf('vsdi std best = %.2f (%.2f, %.2f)\n', std(AA), lo, hi);
    [~, s, lo, hi, ~, ~] =  BootSTD(BB, B);
    fprintf('vsdi std worst = %.2f (%.2f, %.2f)\n', std(BB), lo, hi);
%     fprintf('bootstrap p = %.4f\n\n',sum(bootVals<0)/B);
end

%% model-fitting best-half and worst-half

% BEST WITHIN
    trialData1 = trialData(bBestWithin' & ~ii25,:);
    [prshat1,errbarSD1] = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
    MFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
    fprintf('DF best within = %.2f (%.2f, %.2f)\n',DFhat1,[0 0]);
    fprintf('MF best within= %.2f (%.2f, %.2f)\n',MFhat1,[0 0]);

% WORST WITHIN
    trialData1 = trialData(bWorstWithin' & ~ii25,:);
    [prshat1,errbarSD1] = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
    MFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
    fprintf('DF worst within = %.2f (%.2f, %.2f)\n',DFhat1,[0 0]);
    fprintf('MF worst within= %.2f (%.2f, %.2f)\n',MFhat1,[0 0]);
    
% BEST ACROSS
    trialData1 = trialData(bBestAcross' & ~ii25,:);
    [prshat1,errbarSD1] = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
    MFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
    fprintf('DF best across = %.2f (%.2f, %.2f)\n',DFhat1,[0 0]);
    fprintf('MF best across= %.2f (%.2f, %.2f)\n',MFhat1,[0 0]);
    
% WORST ACROSS
    trialData1 = trialData(bWorstAcross' & ~ii25,:);
    [prshat1,errbarSD1] = compMLest_VSDIvarSNRdata(trialData1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
    MFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
    fprintf('DF worst across = %.2f (%.2f, %.2f)\n',DFhat1,[0 0]);
    fprintf('MF worst across= %.2f (%.2f, %.2f)\n',MFhat1,[0 0]);
    
%% map fitting method (old)

% % Bootstrap Map Fits
% B = 1000;
% bootBestWithinFitValues = BootFitWeightedMeanGaussian(Coordinates,AmplitudesRenormalized(:,:,bP' & bBestWithin),Weights(bP' & bBestWithin),B);
% bootWorstWithinFitValues = BootFitWeightedMeanGaussian(Coordinates,AmplitudesRenormalized(:,:,bP' & bWorstWithin),Weights(bP' & bWorstWithin),B);
% bootBestAcrossFitValues = BootFitWeightedMeanGaussian(Coordinates,AmplitudesRenormalized(:,:,bP' & bBestAcross),Weights(bP' & bBestAcross),B);
% bootWorstAcrossFitValues = BootFitWeightedMeanGaussian(Coordinates,AmplitudesRenormalized(:,:,bP' & bWorstAcross),Weights(bP' & bWorstAcross),B);
%
% bootStat = bootBestAcrossFitValues-bootWorstAcrossFitValues;
%
% % mean
% ampmean = mean(bootStat(:,1))
% ampse = std(bootStat(:,1))
% pvalue = sum(bootStat(:,1)<0)/B
%
% % x pos
% xmean = (mean(bootStat(:,2))-median(Coordinates.X)).*sParameters.PIXELS16X16_TO_MM
% xse = std(bootStat(:,2)).*sParameters.PIXELS16X16_TO_MM
% pvalue = sum(bootStat(:,2)<0)/B
%
% % y pos
% ymean = (mean(bootStat(:,3))-median(Coordinates.Y)).*sParameters.PIXELS16X16_TO_MM
% yse = std(bootStat(:,3)).*sParameters.PIXELS16X16_TO_MM
% pvalue = sum(bootStat(:,3)<0)/B
%
% spread = sqrt(bootStat(:,6).^2 + bootStat(:,6).^2).*sParameters.PIXELS16X16_TO_MM;
% spreadmean = mean(spread)
% spreadse = std(spread)
% pvalue = sum(spread<0)/B
%
% bootStat1 = bootBestWithinFitValues;
% spread1 = sqrt(bootStat1(:,6).^2 + bootStat1(:,6).^2).*sParameters.PIXELS16X16_TO_MM;
% bootStat2 = bootWorstWithinFitValues;
% spread2 = sqrt(bootStat2(:,6).^2 + bootStat2(:,6).^2).*sParameters.PIXELS16X16_TO_MM;
% spreaddiff = spread1 - spread2;
% pvalue = sum(spreaddiff<0)/B



% % Bootstrap map fits
% FittedPresentMap = FuncGaussian2D(FitValuesPresentMap, Coordinates);
% f = figure;
% subplot(1,2,1); imagesc(PresentMap'); axis square;
% subplot(1,2,2); imagesc(FittedPresentMap'); axis square;

