
%% Parameters for simulation:

% Mean function              u_X(t) = E[ X_t ]
SPTimeCourseSimulatedRenormalized(isinf(SPTimeCourseSimulatedRenormalized)) = NaN;
uP = nanmean(SPTimeCourseSimulatedRenormalized(:,bP), 2);
uP(isnan(uP)) = []; % Remove trailing NaNs
uA = nanmean(SPTimeCourseSimulatedRenormalized(:,bA), 2);
uA(isnan(uA)) = []; % Remove trailing NaNs
t = timeAxis(1:numel(uP));

% (Auto-)Covariance function K_XX(t1, t2) = E[ (X_t1 - u_t1)(X_t2 - u_t2) ]
K = zeros(numel(uP));
for i = 1:size(uP)
    for j=1:i
        tmp = nancov(SPTimeCourseSimulatedRenormalized(i,:),SPTimeCourseSimulatedRenormalized(j,:));
        K(i,j) = tmp(2,1);
    end
end
K = K + triu(K',1); % fill upper diagonal
K = K - (eps+min(eig(K))).*eye(numel(uP)); % force posdef

%% Simulate with P/A means
SPTimeCourseSimulated = cat(1, mvnrnd(uP, K, 5000), mvnrnd(uA, K, 5000))';
% % % Or simulate with 0 mean
% % SPTimeCourseSimulated = mvnrnd(zeros(size(uP)), K, 10000)';

% Parse trials into observed proportions of H/M/FA/CR
proportions = [sum(bH) sum(bM) sum(bFA) sum(bCR)] ./ numel(bH);
startend = MS2Frame(t, [100 400]);
DV = mean(SPTimeCourseSimulated(startend(1):startend(2), :), 1)';
loss = @(tau) norm(proportions - ...
    [sum(DV(1:5000) >= tau) sum(DV(1:5000) < tau) sum(DV(5001:end) >= tau) sum(DV(5001:end) < tau)] ./ 10000, 2);
% Basic gridsearch over values for threshold
tauopts = linspace(min(DV), max(DV), 1000);
tauloss = cellfun(loss, num2cell(tauopts));
[~, idx] = min(tauloss);
threshold = tauopts(idx);
% Apply the threshold
bPs = [true([5000,1]); false([5000, 1])];
bAs = [false([5000, 1]); true([5000, 1])];
bHs = (DV>=threshold & bPs);
bMs = (DV< threshold & bPs);
bFAs = (DV>=threshold & bAs);
bCRs = (DV< threshold & bAs);
obsproportions = [sum(bHs) sum(bMs) sum(bFAs) sum(bCRs)] ./ numel(bHs);

%% Apply preprocessing as if real data
integRange = [100, 300];
integRangeFrames = startend;
WeightsSimulated = ones(1, 10000);
RTs = zeros([1, 10000]);

[meanGrandAbsentTimeCourse, ~] = ...
    ComputeWeightedMeanAndSEWaveform(SPTimeCourseSimulated,WeightsSimulated,bAs);
meanAs = nanmean(meanGrandAbsentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
SPTimeCourseSimulatedRenormalized = SPTimeCourseSimulated - meanAs; % makes average of absent trials 0 in integration window

[meanGrandPresentTimeCourse, ~] = ...
    ComputeWeightedMeanAndSEWaveform(SPTimeCourseSimulatedRenormalized,WeightsSimulated,bPs);
meanPs = nanmean(meanGrandPresentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
SPTimeCourseSimulatedRenormalized = SPTimeCourseSimulatedRenormalized ./ meanPs; % makes average of present trials 1 in integration window

bShowPlot = [0 1 1 0 1 1 0 0];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR]
f = figure; hold on; set(f, 'Color', 'w');

plotRange = [1 numel(uP)];
h1 = CreateWeightedTimeCoursePanel(SPTimeCourseSimulatedRenormalized,WeightsSimulated,bHs,bMs,bFAs,bCRs,RTs,t,bShowPlot,plotRange,integRange,0,YLIMS);
set(gca,'fontsize',14,'ytick',-1:3); boto;
xlim([-300 340]);set(f,'Color','white','units','inches','Position',[1 1 4 * 7/5 4]);
set(gca,'ylim',[-3 5])
xlabel('Time (ms)','fontsize',14)
ylabel('\Delta F/F (Normalized)')
set(findobj(gcf,'linewidth',6),'linewidth',2);