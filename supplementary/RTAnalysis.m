% Time course of discriminability between H-vs-M, CR-vs-FA and P-vs-A using
% a sliding 100 ms window; when does the separation become significant; for
% each monkey separately and for both monkeys combined

% %% Change "bMyMonkey" to change which monkey (or both) is/are analyzed

% %% Currently assuming the 100ms window starts at the left edge (conservative estimates)

% %% Presumably uncertainty and therefore significance to be measured with
% bootstrapping, but at what stage do we bootstrap? Resample with
% replacement each of the Hits, Misses, etc.?

%% Boiler-plate code to import required data
            setDirs;


            % ******* load Preprocessed data *******************************************
            load([ReadDir 'Preprocessed.mat']);
            load([ReadDir 'Inclusion.mat']);

            % ******* set parameters for analysis **************************************
            sParameters = Parameters();

            % ******* reduce data ******************************************************
            % First reduction in trials: 10163 --> 10153
            load([WriteDir 'dat_MM190805.mat']);
            Amplitudes = Data.Amplitudes(:,:,bInclude);
            SPTimeCourse = Data.SPTimeCourse(:,bInclude);
            RT = Data.RT(bInclude);

            % All of these variables are __ x 10153 trials
            clear dat;
            clear bInclude;

            % Second reduction in trials: 10163 --> 10153 --> 9979, OK!
            load([WriteDir 'dat_MM190805_processed.mat']);
            Amplitudes = Amplitudes(:,:,bInclude);
            SPTimeCourse = SPTimeCourse(:,bInclude);
            RT = RT(bInclude);

            Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

            bInclude = bInclude(bInclude);

%%           
            % ******* remove high contrast trials and choose monkey(s) *****************
            bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
            bMyMonkey = dat.monkey == 1 | dat.monkey == 2;
%             bMyMonkey = dat.monkey == 1;
%             bMyMonkey = dat.monkey == 2;
            bInclude = bMyMonkey & ~bIsInHighContrastBlock;

            bP = dat.bP & bInclude;
            bA = dat.bA & bInclude;
            bH = dat.bH & bInclude;
            bM = dat.bM & bInclude;
            bFA = dat.bFA & bInclude;
            bCR = dat.bCR & bInclude;

            % ******* re-normalize *****************************************************
            nrmContrResampled = dat.nrmContr;
            nrmContrResampled(bA) = randsample(nrmContrResampled(bP),sum(bA),'true');
            RenormalizationWeightsSpace = permute(repmat(nrmContrResampled,[1 17 17]),[2 3 1]);
            AmplitudesRenormalized = Amplitudes ./ RenormalizationWeightsSpace;
            RenormalizationWeightsTime = permute(repmat(nrmContrResampled,[1 120]),[2 1]);
            SPTimeCourseRenormalized = SPTimeCourse ./ RenormalizationWeightsTime;


            % ******* weight experiments by SNR ****************************************
            % AmplitudesRenormalized = AmplitudesRenormalized .* permute(repmat(dat.nrmSNR,[1 17 17]),[2 3 1]);
            % SPTimeCourseRenormalized = SPTimeCourseRenormalized .* permute(repmat(dat.nrmSNR,[1 120]),[2 1]);
            Weights = dat.nrmSNR';

            % ******* re-re-normalize Absent = 0 and Present = 1 ************************
            integRange = [min(IntegrationTimes.StartMS(bP|bA)) max(IntegrationTimes.EndMS(bP|bA))];
            integRangeFrames = MS2Frame(Data.timeAxis,integRange);
            %GrandPresentTimeCourses = SPTimeCourse(:,(bH | bM));

            [meanGrandAbsentTimeCourse, ~] = ...
                ComputeWeightedMeanAndSEWaveform(SPTimeCourseRenormalized,Weights,bA);
            meanA = nanmean(meanGrandAbsentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
            SPTimeCourseRenormalized = SPTimeCourseRenormalized - meanA; % makes average of absent trials 0 in integration window

            [meanGrandPresentTimeCourse, ~] = ...
                ComputeWeightedMeanAndSEWaveform(SPTimeCourseRenormalized,Weights,bP);
            meanP = nanmean(meanGrandPresentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
            SPTimeCourseRenormalized = SPTimeCourseRenormalized ./ meanP; % makes average of present trials 1 in integration window

%% Correlation between RT and Hit/FA responses

subTimes = unique(MS2Frame(Data.timeAxis,100:200)); % All time samples between 100 and 200 ms
SPTimeCourseRenormalized100ms = nanmean(SPTimeCourseRenormalized(subTimes,:));

[r_H, p_H] = corr(SPTimeCourseRenormalized100ms(bH)',RT(bH)');
[r_FA, p_FA] = corr(SPTimeCourseRenormalized100ms(bFA)',RT(bFA)');

figure();
subplot(1,2,1);
scatter(SPTimeCourseRenormalized100ms(bH),RT(bH),8,BLUE,'filled');
botos; set(gca,'ylim',[100 600]); xl = get(gca,'xlim');
h = lsline;
set(h,'linewidth',2,'color','k','linestyle','-');
xlabel('\Delta F/F, int. 100-200ms'); ylabel('RT');
text(10,575,sprintf('r = %.3f\np = %.3f**\nN = %d',r_H, p_H, sum(bH)),'fontsize',14)
subplot(1,2,2);
scatter(SPTimeCourseRenormalized100ms(bFA),RT(bFA),8,RED,'filled');
botos; set(gca,'ylim',[100 600],'xlim',xl,'yticklabel',[])
h = lsline;
set(h,'linewidth',2,'color','k','linestyle','-');
xlabel('\Delta F/F, int. 100-200ms')
text(10,575,sprintf('r = %.3f\np = %.3f\nN = %d',r_FA, p_FA, sum(bFA)),'fontsize',14)

%% Reaction time analyses
RT0 = RT - 150; % Earliest-possible RT is coded as 150ms

figure('defaultaxesfontsize',16); hold on;
h1 = histogram(RT0(bH), 50, 'facealpha',0.5, 'normalization','probability', 'facecolor',BLUE); 
h2 = histogram(RT0(bFA), 50, 'facealpha',0.5, 'normalization','probability', 'facecolor',RED); 
legend({'Hit', 'FA'})
line([450 450], get(gca,'ylim'), 'linestyle',':', 'color','k', 'linewidth',2, 'HandleVisibility','off')
xlim(get(gca,'xlim').*[0 1])
ylabel('Proportion of trials')
set(gca,'yticklabel',[])


figure('defaultaxesfontsize',16); 
subplot(211); hold on;
h1 = histogram(RT0(dat.monkey == 1 & bH), 40, 'facealpha',0.5, 'normalization','probability', 'facecolor',BLUE); 
h2 = histogram(RT0(dat.monkey == 1 & bFA), 40, 'facealpha',0.5, 'normalization','probability', 'facecolor',RED); 
legend({'Hit', 'FA'})
line([450 450], get(gca,'ylim'), 'linestyle',':', 'color','k', 'linewidth',2, 'HandleVisibility','off')
xlim(get(gca,'xlim').*[0 1])
% ylabel('Proportion of trials')
set(gca,'yticklabel',[])
title('Monkey T')
boto;
subplot(212); hold on;
histogram(RT0(dat.monkey == 2 & bH), 40, 'facealpha',0.5, 'normalization','probability', 'facecolor',BLUE); 
histogram(RT0(dat.monkey == 2 & bFA), 40, 'facealpha',0.5, 'normalization','probability', 'facecolor',RED); 
line([450 450], get(gca,'ylim'), 'linestyle',':', 'color','k', 'linewidth',2)
xlabel('RT (ms)')
xlim(get(gca,'xlim').*[0 1])
ylabel('Proportion of trials')
set(gca,'yticklabel',[])
title('Monkey C')
boto;
