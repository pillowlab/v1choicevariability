% Normality tests for decision variables under all pooling rules. Are they
% all approximately Gaussian, and do they all have approximately equal
% variance?

% %% Data fails all normality and equivariance tests, which is a pretty

%% Boiler-plate code to import required data
            a0_setDirs;
            
            % Decision variable amplitudes under nine pooling models
            load([ReadDir 'AmpsByModel'])

            % Reimport trial labels
            load([WriteDir 'dat_MM190805_processed.mat']);

            Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

            bInclude = bInclude(bInclude);

            bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
            bMyMonkey = dat.monkey == 1 | dat.monkey == 2;
            % bMyMonkey = dat.datenum == 27;
            bInclude = bMyMonkey & ~bIsInHighContrastBlock;

            bP = dat.bP & bInclude;
            bA = dat.bA & bInclude;
            bH = dat.bH & bInclude;
            bM = dat.bM & bInclude;
            bFA = dat.bFA & bInclude;
            bCR = dat.bCR & bInclude;

            conds = {bH, bM, bFA, bCR};


%% Visualize normality/equivariance in the data with a huge plot

figure('position',[560   370   730   578]);
poolingrules = {'Fitted Choice-\ntriggered Average','Fitted Average\nEvoked','Gaussian\n(\\sigma=0.5 mm)','Gaussian\n(\\sigma=1 mm)','Gaussian\n(\\sigma=2 mm)','Gaussian\n(\\sigma=3 mm)','Gaussian\n(\\sigma=4 mm)','Optimal\n(Present v. Absent)','Optimal\n(Yes v. No)'};
conditions = {'Hit','Miss','FA','CR'};

nrow = size(AmpsByModel,1);
ncol = numel(conds);
ax = tight_subplot(nrow, ncol, [.025 .025], [.15 .05], [.2 .05]);

for k = 1:size(AmpsByModel,1)
    % Plot normalized histograms of the data
    for l = 1:numel(conds)
        axes(ax(ncol*(k-1) + l)); hold on;
        histogram(AmpsByModel(k,conds{l}),linspace(-20,20,50),'Normalization','pdf','facecolor',[0.95 0.95 0.95])
        if k == 1
            text(0.5,1.1,sprintf(conditions{l}),'horizontalalignment','center','units','normalized','fontsize',14);
        end
        boto;
        if l == 1
            text(-1.1,0.5,sprintf(poolingrules{k}),'units','normalized','fontsize',14);
        end
    end
    % Fit and inspect Gaussians to each condition/pooling rule
    grpdata = [];
    grplbl  = [];
    for l = 1:numel(conds)
        grpdata = cat(1, grpdata, AmpsByModel(k,conds{l})');
        grplbl  = cat(1,  grplbl, l*ones(numel(AmpsByModel(k,conds{l})),1));
    end
    [pdca,gn,gl] = fitdist(grpdata,'Normal','By',grplbl);
    vars = [];
    varcis = [];
    for l = 1:numel(pdca)
        vars = cat(2,vars,pdca{l}.sigma);
        tmp = paramci(pdca{l},'Alpha',0.05/nchoosek(4,2));
        varcis = cat(2,varcis, tmp(:,2));
    end
%     subplot(nrow,ncol,ncol*k)
%     errorbar([1 2 3 4],vars,vars-varcis(1,:),varcis(2,:)-vars,[],[],'ko', 'markersize',8);
%     boto; set(gca,'xtick',[1,2,3,4],'xticklabel',{'H','M','FA','CR'},'xlim',[0.5 4.5],'ylim',[2.5 6]); ylabel('$\sigma^2$');
%     unequalvar = (cat(1,unequalvar,any(min(varcis(2,:)) < varcis(1,:)) || any(max(varcis(1,:)) > varcis(2,:))));
%     if k == 1
%         title('Group variances');
%     end
    % Re-fit shared variances over all distributions, plot fit with
    % individual vs shared variance
    mus = [];
    for l = 1:numel(pdca), mus = cat(1,mus,pdca{l}.mu); end
    
    pd = fitdist(grpdata-mus(grplbl),'Normal');
    sharedsigma = pd.sigma;
    for l = 1:numel(pdca)
        axes(ax(ncol*(k-1) + l)); hold on;
        xx = get(gca,'xlim'); xx = linspace(xx(1),xx(2),150);
        yy = pdf(makedist('Normal','mu',pdca{l}.mu,'sigma',pdca{l}.sigma), xx);
        line1 = plot(xx, yy, 'linewidth', 3, 'color', [213 87 59]./255);
        yy = pdf(makedist('Normal','mu',pdca{l}.mu,'sigma',sharedsigma), xx);
        line2 = plot(xx, yy, 'linewidth', 2, 'color', [72 99 156]./255);
    end
end

set(ax,'xlim',[-20 20]);
uppery = cell2mat(get(ax,'ylim')); uppery = reshape(uppery(:,2),4,[]); 
newuppery = bsxfun(@times, ones(size(uppery)), max(uppery,[],1)); 
% set(ax, 'ylim',mat2cell([zeros(size(newuppery(:))) newuppery(:)], ones(numel(ax),1), 2))
cellfun(@set, num2cell(ax), repmat({'ylim'},size(ax)), mat2cell([zeros(size(newuppery(:))) newuppery(:)], ones(numel(ax),1), 2))
% for cax = ax(1:4:end)'
%     set(cax,'yticklabel',yticks(cax));
% end
set(ax,'ytick',[]);
for cax = ax(end-3:end)'
    set(cax,'xtick',-20:10:20,'xticklabel',-20:10:20);
end
xlabel(ax(end-3),'\Delta F/F','fontsize',14)

axes(ax(end-1))
legend([line1 line2],{'Best Gaussian fit, group-wise variance','Best Gaussian fit, equivariance'},'location','southeast','fontsize',14)



%% Inspect variances of Gaussian fits (equivariance test by inspection)

% Return boolean per pooling rule with whether any two of the variances are
% unequal with Bonferroni-corrected confidence bounds (4-choose-2)
% %%%%%% THIS CODE WAS COPIED INTO THE MAIN CELL ABOVE
% % unequalvar = [];
% % for k = 1:size(AmpsByModel,1)
% %     grpdata = [];
% %     grplbl  = [];
% %     for l = 1:numel(conds)
% %         grpdata = cat(1, grpdata, AmpsByModel(k,conds{l})');
% %         grplbl  = cat(1,  grplbl, l*ones(numel(AmpsByModel(k,conds{l})),1));
% %     end
% %     [pdca,gn,gl] = fitdist(grpdata,'Normal','By',grplbl);
% %     vars = [];
% %     varcis = [];
% %     for l = 1:numel(pdca)
% %         vars = cat(2,vars,pdca{l}.sigma);
% %         tmp = paramci(pdca{l},'Alpha',0.05/nchoosek(4,2));
% %         varcis = cat(2,varcis, tmp(:,2));
% %     end
% %     errorbar([1 2 3 4],vars,varcis(1,:),varcis(2,:),[],[])
% %     unequalvar = (cat(1,unequalvar,any(min(varcis(2,:)) < varcis(1,:)) || any(max(varcis(1,:)) > varcis(2,:))));
% % end

% Return all of the variances and respective confidence intervals
% % vars = [];
% % varcis = [];
% % for k = 1:size(AmpsByModel,1)
% %     grpdata = [];
% %     grplbl  = [];
% %     for l = 1:numel(conds)
% %         grpdata = cat(1, grpdata, AmpsByModel(k,conds{l})');
% %         grplbl  = cat(1,  grplbl, l*ones(numel(AmpsByModel(k,conds{l})),1));
% %     end
% %     [pdca,gn,gl] = fitdist(grpdata,'Normal','By',grplbl);
% %     for l = 1:numel(pdca)
% %         vars = cat(2,vars,pdca{l}.sigma);
% %         tmp = paramci(pdca{l},'Alpha',0.05/nchoosek(4,2));
% %         varcis = cat(2,varcis, tmp(:,2));
% %     end
% % end

%% Fit distributions, subtract group means, re-fit common variance
% % makedist('Normal','mu',75,'sigma',10)

% % for k = 1:size(AmpsByModel,1)
% %     grpdata = [];
% %     grplbl  = [];
% %     for l = 1:numel(conds)
% %         grpdata = cat(1, grpdata, AmpsByModel(k,conds{l})');
% %         grplbl  = cat(1,  grplbl, l*ones(numel(AmpsByModel(k,conds{l})),1));
% %     end
% %     [pdca,gn,gl] = fitdist(grpdata,'Normal','By',grplbl);
% %     mus = [];
% %     for l = 1:numel(pdca)
% %         mus = cat(1,mus,pdca{l}.mu);
% %     end
% %     pd = fitdist(grpdata-mus(grplbl),'Normal');
% %     sharedsigma = pd.sigma;
% %     for l = 1:numel(pdca)
% %         xx = get(gca,'xlim'); xx = linspace(xx(1),xx(2),150);
% %         yy = pdf(xx,makedist('Normal','mu',pdca{l}.mu,'sigma',sharedsigma));
% %         plot(xx,yy)
% %     end
% % end

%% Test for normality -- Shaprio-Wilk test
% %%%%%% THIS CODE IS NOT PRODUCTIVE FOR TESTING NORMALITY IN OUR LARGE DATASET
% % normtest = nan(size(AmpsByModel,1), numel(conds), 2);
% % for k = 1:size(AmpsByModel,1)
% %     for l = 1:numel(conds)
% %         [a,b,~] = stats.swtest(AmpsByModel(k,conds{l}),0.05);
% %         normtest(k,l,:) = [a,b];
% %     end
% % end

%% Test for equal variance -- Brown-Forsythe test
% Bartlett == underlying assumption of normality, testing equal variance (around mean)
% Brown-Forsythe == no such assumption, still testing equal variance (around median)

% %%%%%% THIS CODE IS NOT PRODUCTIVE FOR TESTING EQUIVARIANCE IN OUR LARGE DATASET
% % conds = {bH, bM, bFA, bCR};
% % eqvartest = nan(size(AmpsByModel,1), 1);
% % for k = 1:size(AmpsByModel,1)
% %     grpdata = [];
% %     grplbl  = [];
% %     for l = 1:numel(conds)
% %         grpdata = cat(1, grpdata, AmpsByModel(k,conds{l})');
% %         grplbl  = cat(1,  grplbl, l*ones(numel(AmpsByModel(k,conds{l})),1));
% %     end
% %     [p, stats] = vartestn(grpdata, grplbl, 'testtype','bartlett', 'display','off');
% %     eqvartest(k) = p;
% % end

