% Time course of discriminability between H-vs-M, CR-vs-FA and P-vs-A using
% a sliding 100 ms window; when does the separation become significant; for
% each monkey separately and for both monkeys combined

% %% Change "bMyMonkey" to change which monkey (or both) is/are analyzed

% %% Currently assuming the 100ms window starts at the left edge (conservative estimates)

% %% Presumably uncertainty and therefore significance to be measured with
% bootstrapping, but at what stage do we bootstrap? Resample with
% replacement each of the Hits, Misses, etc.?

%% Boiler-plate code to import required data
            setDirs;


            % ******* load Preprocessed data *******************************************
            load([ReadDir 'Preprocessed.mat']);
            load([ReadDir 'Inclusion.mat']);

            % ******* set parameters for analysis **************************************
            sParameters = Parameters();

            % ******* reduce data ******************************************************
            % First reduction in trials: 10163 --> 10153
            load([WriteDir 'dat_MM190805.mat']);
            Amplitudes = Data.Amplitudes(:,:,bInclude);
            SPTimeCourse = Data.SPTimeCourse(:,bInclude);
            RT = Data.RT(bInclude);

            % All of these variables are __ x 10153 trials
            clear dat;
            clear bInclude;

            % Second reduction in trials: 10163 --> 10153 --> 9979, OK!
            load([WriteDir 'dat_MM190805_processed.mat']);
            Amplitudes = Amplitudes(:,:,bInclude);
            SPTimeCourse = SPTimeCourse(:,bInclude);
            RT = RT(bInclude);

            Weights = ones([1 sum(bInclude)]);  % vestigial variable - keeping to retain flexibility

            bInclude = bInclude(bInclude);
            
%% Renormalize trial-by-trial temporal activity
% We currently use this: (80ms centered on 0)
normalizer = nanmean(SPTimeCourse(find(Data.timeAxis==0) + (-4:4),:));
% but could also use this: (earliest possible 80ms)
% normalizer = nanmean(SPTimeCourse(1:9,:));

SPTimeCourse = bsxfun(@minus, SPTimeCourse, normalizer);

            % ******* remove high contrast trials and choose monkey(s) *****************
            bIsInHighContrastBlock = dat.blockcontrmaxinds == 25;
            bMyMonkey = dat.monkey == 1 | dat.monkey == 2; % Both monkeys
%             bMyMonkey = dat.monkey == 1; % Monkey 1
%             bMyMonkey = dat.monkey == 2; % Monkey 2
            bInclude = bMyMonkey & ~bIsInHighContrastBlock;

            bP = dat.bP & bInclude;
            bA = dat.bA & bInclude;
            bH = dat.bH & bInclude;
            bM = dat.bM & bInclude;
            bFA = dat.bFA & bInclude;
            bCR = dat.bCR & bInclude;
            integRange = [min(IntegrationTimes.StartMS(bP|bA)) max(IntegrationTimes.EndMS(bP|bA))];
            
%% Bootstrapping for significance of difference waveforms

            % ******* re-normalize *****************************************************
            nrmContrResampled = dat.nrmContr;
            nrmContrResampled(bA) = randsample(nrmContrResampled(bP),sum(bA),'true');
            RenormalizationWeightsSpace = permute(repmat(nrmContrResampled,[1 17 17]),[2 3 1]);
            AmplitudesRenormalized = Amplitudes ./ RenormalizationWeightsSpace;
            RenormalizationWeightsTime = permute(repmat(nrmContrResampled,[1 120]),[2 1]);
            SPTimeCourseRenormalized = SPTimeCourse ./ RenormalizationWeightsTime;


            % ******* weight experiments by SNR ****************************************
            % AmplitudesRenormalized = AmplitudesRenormalized .* permute(repmat(dat.nrmSNR,[1 17 17]),[2 3 1]);
            % SPTimeCourseRenormalized = SPTimeCourseRenormalized .* permute(repmat(dat.nrmSNR,[1 120]),[2 1]);
            Weights = dat.nrmSNR';

            integRange = [min(IntegrationTimes.StartMS(bP|bA)) max(IntegrationTimes.EndMS(bP|bA))];
            integRangeFrames = MS2Frame(Data.timeAxis,integRange);
            %GrandPresentTimeCourses = SPTimeCourse(:,(bH | bM));
            SPTimeCourseRenormalized = SPTimeCourse ./ RenormalizationWeightsTime;

            [meanGrandAbsentTimeCourse, ~] = ...
                ComputeWeightedMeanAndSEWaveform_heavy(SPTimeCourseRenormalized,Weights,bA);
            meanA = nanmean(meanGrandAbsentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
            % This is a bug?
            % % SPTimeCourseRenormalized = SPTimeCourseRenormalized ./ repmat(meanA,[size(SPTimeCourseRenormalized,1) size(SPTimeCourseRenormalized,2)]);
            SPTimeCourseRenormalized = SPTimeCourseRenormalized - meanA; % makes average of absent trials 0 in integration window

            [meanGrandPresentTimeCourse, ~] = ...
                ComputeWeightedMeanAndSEWaveform_heavy(SPTimeCourseRenormalized,Weights,bP);
            meanP = nanmean(meanGrandPresentTimeCourse(integRangeFrames(1):integRangeFrames(2)));
            % This is inefficient
            % % SPTimeCourseRenormalized = SPTimeCourseRenormalized ./ repmat(meanP,[size(SPTimeCourseRenormalized,1) size(SPTimeCourseRenormalized,2)]);
            SPTimeCourseRenormalized = SPTimeCourseRenormalized ./ meanP; % makes average of present trials 1 in integration window

%% Bootstrap across trial types to get null distribution of discriminability
% "OUTER LOOP"

NBOOT = 5000;
NTRIALS = numel(Weights);
% Define kernel for 100ms windows
nsamples_in_kernel = 100/sParameters.FrameFrequency;
kern = ones(nsamples_in_kernel,1)/nsamples_in_kernel;

% HIT - MISS
group1 = bH;
group2 = bM;
    bootWeightedMeanTC = zeros(size(SPTimeCourseRenormalized,1),NBOOT);
    trialIndices = find(group1|group2);
    NG1 = sum(group1); % Number of trials in group 1
    for k = 1:NBOOT
        bGroup1 = false(NTRIALS,1);
        bGroup2 = false(NTRIALS,1);
        % Resample the two groups by shuffling trial indices
        resampling = randsample(trialIndices,numel(trialIndices));
        bGroup1(resampling(1:NG1)) = true;
        bGroup2(resampling((NG1+1):end)) = true;

        % Recompute weighted mean difference waveform
        [wmdTC, ~] = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,bGroup1,bGroup2);
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    trueWeightedMeanTC = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,group1,group2);
    % Convolve all signals into 100ms bins
    bootAcrossTCBinned_HM    = conv2(bootWeightedMeanTC, kern, 'valid');
    bootAcrossTCBinnedPrc_HM = prctile(bootAcrossTCBinned_HM,[2.5 97.5],2);
    trueTCBinned    = conv(trueWeightedMeanTC, kern, 'valid');
    % Measure autocorrelation of signal?
    significance_HM = (trueTCBinned > bootAcrossTCBinnedPrc_HM(:,2)' |  trueTCBinned < bootAcrossTCBinnedPrc_HM(:,1)');

% FALSE ALARM - CORRECT REJECT
group1 = bFA;
group2 = bCR;
    bootWeightedMeanTC = zeros(size(SPTimeCourseRenormalized,1),NBOOT);
    trialIndices = find(group1|group2);
    NG1 = sum(group1); % Number of trials in group 1
    for k = 1:NBOOT
        bGroup1 = false(NTRIALS,1);
        bGroup2 = false(NTRIALS,1);
        % Resample the two groups by shuffling trial indices
        resampling = randsample(trialIndices,numel(trialIndices));
        bGroup1(resampling(1:NG1)) = true;
        bGroup2(resampling((NG1+1):end)) = true;

        % Recompute weighted mean difference waveform
        [wmdTC, ~] = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,bGroup1,bGroup2);
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    trueWeightedMeanTC = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,group1,group2);
    % Convolve all signals into 100ms bins
    bootAcrossTCBinned_FACR    = conv2(bootWeightedMeanTC, kern, 'valid');
    bootAcrossTCBinnedPrc_FACR = prctile(bootAcrossTCBinned_FACR,[2.5 97.5],2);
    trueTCBinned    = conv(trueWeightedMeanTC, kern, 'valid');
    % Measure autocorrelation of signal?
    significance_FACR = (trueTCBinned > bootAcrossTCBinnedPrc_FACR(:,2)' |  trueTCBinned < bootAcrossTCBinnedPrc_FACR(:,1)');

% PRESENT - ABSENT
group1 = bP;
group2 = bA;
    bootWeightedMeanTC = zeros(size(SPTimeCourseRenormalized,1),NBOOT);
    trialIndices = find(group1|group2);
    NG1 = sum(group1); % Number of trials in group 1
    for k = 1:NBOOT
        bGroup1 = false(NTRIALS,1);
        bGroup2 = false(NTRIALS,1);
        % Resample the two groups by shuffling trial indices
        resampling = randsample(trialIndices,numel(trialIndices));
        bGroup1(resampling(1:NG1)) = true;
        bGroup2(resampling((NG1+1):end)) = true;

        % Recompute weighted mean difference waveform
        [wmdTC, ~] = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,bGroup1,bGroup2);
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    trueWeightedMeanTC = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,group1,group2);
    % Convolve all signals into 100ms bins
    bootAcrossTCBinned_PA    = conv2(bootWeightedMeanTC, kern, 'valid');
    bootAcrossTCBinnedPrc_PA = prctile(bootAcrossTCBinned_PA,[2.5 97.5],2);
    trueTCBinned    = conv(trueWeightedMeanTC, kern, 'valid');
    % Measure autocorrelation of signal?
    significance_PA = (trueTCBinned > bootAcrossTCBinnedPrc_PA(:,2)' |  trueTCBinned < bootAcrossTCBinnedPrc_PA(:,1)');

ntime = numel(trueTCBinned);
timeAxis = Data.timeAxis;
timeAxis = timeAxis(1:ntime);    
    
% Remove any significance of less than a certain length
reqlen = 5;

cache = [];
for k = 1:numel(significance_HM)
    if significance_HM(k)
        cache = [cache k];
    else
        if numel(cache) < reqlen
            significance_HM(cache) = false;
        end
        cache = [];
    end
end

cache = [];
for k = 1:numel(significance_FACR)
    if significance_FACR(k)
        cache = [cache k];
    else
        if numel(cache) < reqlen
            significance_FACR(cache) = false;
        end
        cache = [];
    end
end

cache = [];
for k = 1:numel(significance_PA)
    if significance_PA(k)
        cache = [cache k];
    else
        if numel(cache) < reqlen
            significance_PA(cache) = false;
        end
        cache = [];
    end
end

%% Generate Figure 6

XLIMS = [0 340];
timeAxis = Data.timeAxis;
% bShowPlot = [0 0 0 0 0 0 1 1 1];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]
bShowPlot = [1 1 1 1 1 1 0 0 0];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]

plots.timecourses;
% plots.timecourses_significancepatches;

%% Bootstrap within trial type to get null distribution on first crossing time
% "INNER LOOP"

NBOOT = 5000;
NTRIALS = numel(Weights);

% HIT - MISS
group1 = bH;
group2 = bM;
    bootWeightedMeanTC = zeros(size(SPTimeCourse,1),NBOOT);
    group1Indices = find(group1);
    group2Indices = find(group2);
    NG1 = sum(group1); % Number of trials in group 1
    NG2 = sum(group2); % Number of trials in group 2
    for k = 1:NBOOT
        
        % Resample within the first group with replacement
        resampling = randsample(group1Indices, NG1, true);
        wmTC1 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
                % Resample within the first group with replacement
        resampling = randsample(group2Indices, NG2, true);
        wmTC2 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
        wmdTC = wmTC1 - wmTC2;
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    % Convolve all signals into 100ms bins
    bootWithinTCBinned_HM = conv2(bootWeightedMeanTC, kern, 'valid');
    signif = bsxfun(@gt, bootWithinTCBinned_HM, bootAcrossTCBinnedPrc_HM(:,2)) | bsxfun(@lt, bootWithinTCBinned_HM, bootAcrossTCBinnedPrc_HM(:,1));
    signif(1:(find(timeAxis==0)-1), :) = false;
    lenkern = ones(reqlen, 1) ./ reqlen;
    signif = conv2(signif, lenkern, 'valid') >= 1; % Restrict to sequences of significance ? ``reqlen``
    firstCross_HM = arrayfun(@(i) find(signif(:,i), 1) , 1:size(signif,2), 'uni', 0);
    firstCross_HM = cell2mat(firstCross_HM(~cellfun(@isempty, firstCross_HM)));
    firstCrossPrc_HM = prctile(timeAxis(firstCross_HM), [2.5, 97.5]);
    
% HIT - MISS
group1 = bFA;
group2 = bCR;
    bootWeightedMeanTC = zeros(size(SPTimeCourse,1),NBOOT);
    group1Indices = find(group1);
    group2Indices = find(group2);
    NG1 = sum(group1); % Number of trials in group 1
    NG2 = sum(group2); % Number of trials in group 2
    for k = 1:NBOOT
        
        % Resample within the first group with replacement
        resampling = randsample(group1Indices, NG1, true);
        wmTC1 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
                % Resample within the first group with replacement
        resampling = randsample(group2Indices, NG2, true);
        wmTC2 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
        wmdTC = wmTC1 - wmTC2;
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    % Convolve all signals into 100ms bins
    bootWithinTCBinned_FACR = conv2(bootWeightedMeanTC, kern, 'valid');
    signif = bsxfun(@gt, bootWithinTCBinned_FACR, bootAcrossTCBinnedPrc_FACR(:,2)) | bsxfun(@lt, bootWithinTCBinned_FACR, bootAcrossTCBinnedPrc_FACR(:,1));
    signif(1:(find(timeAxis==0)-1), :) = false;
    lenkern = ones(reqlen, 1) ./ reqlen;
    signif = conv2(signif, lenkern, 'valid') >= 1; % Restrict to sequences of significance ? ``reqlen``
    firstCross_FACR = arrayfun(@(i) find(signif(:,i), 1) , 1:size(signif,2), 'uni', 0);
    firstCross_FACR = cell2mat(firstCross_FACR(~cellfun(@isempty, firstCross_FACR)));
    firstCrossPrc_FACR = prctile(timeAxis(firstCross_FACR), [2.5, 97.5]);
    
% PRESENT - ABSENT
group1 = bP;
group2 = bA;
    bootWeightedMeanTC = zeros(size(SPTimeCourse,1),NBOOT);
    group1Indices = find(group1);
    group2Indices = find(group2);
    NG1 = sum(group1); % Number of trials in group 1
    NG2 = sum(group2); % Number of trials in group 2
    for k = 1:NBOOT
        
        % Resample within the first group with replacement
        resampling = randsample(group1Indices, NG1, true);
        wmTC1 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
                % Resample within the first group with replacement
        resampling = randsample(group2Indices, NG2, true);
        wmTC2 = nansum(bsxfun(@times, SPTimeCourseRenormalized(:, resampling), Weights(resampling)), 2) ./ ...
            nansum(Weights(resampling));
        wmdTC = wmTC1 - wmTC2;
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    % Convolve all signals into 100ms bins
    bootWithinTCBinned_PA = conv2(bootWeightedMeanTC, kern, 'valid');
    signif = bsxfun(@gt, bootWithinTCBinned_PA, bootAcrossTCBinnedPrc_PA(:,2)) | bsxfun(@lt, bootWithinTCBinned_PA, bootAcrossTCBinnedPrc_PA(:,1));
    signif(1:(find(timeAxis==0)-1), :) = false;
    lenkern = ones(reqlen, 1) ./ reqlen;
    signif = conv2(signif, lenkern, 'valid') >= 1; % Restrict to sequences of significance ? ``reqlen``
    firstCross_PA = arrayfun(@(i) find(signif(:,i), 1) , 1:size(signif,2), 'uni', 0);
    firstCross_PA = cell2mat(firstCross_PA(~cellfun(@isempty, firstCross_PA)));
    firstCrossPrc_PA = prctile(timeAxis(firstCross_PA), [2.5, 97.5]);
    

%% Generate Figure 6, March 2021 version

XLIMS = [0 340];
timeAxis = Data.timeAxis;
bShowPlot = [1 1 1 1 0 0 0 0 0];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]
% bShowPlot = [0 1 1 0 0 0 1 0 1];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]

plots.timecourses_alt;
% plots.timecourses_significancepatches;

bShowPlot = [1 0 0 1 1 1 0 0 0];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]
% bShowPlot = [0 0 0 0 1 1 0 1 1];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]

plots.timecourses_alt;

%% Generate Figure 6 -- Supplementary Figure 1

XLIMS = [0 340];
timeAxis = Data.timeAxis;
bShowPlot =   [0 0 0 0 0 0 1 1 1];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]
% bShowPlot = [0 1 1 0 0 0 1 0 1];  % turn on and off which trial types to show [Present Hit Miss Absent FA CR H-M FA-CR P-A]

plots.timecourses_alt;
plots.timecourses_significancepatches;

%%
% %% Understanding early discriminability -- exploratory analyses
% 
% % 1 -- (slow noise) Noise correlations and/or temporal cross-correlations 
% % between pre-stimulus onset activity with post-stimulus onset activity
% 
% pre_win = -300:-100;
% pre_win_Frames = unique(MS2Frame(Data.timeAxis,pre_win));
% preR = nanmean(SPTimeCourseRenormalized(pre_win_Frames,:));
% post_win = 100:300;
% post_win_Frames = unique(MS2Frame(Data.timeAxis,post_win));
% postR = nanmean(SPTimeCourseRenormalized(post_win_Frames,:));
% 
% figure;
% ha = tight_subplot(2, 2, .1, .1, .1);
% msz = 12;
% xlims = [-23.9942   24.3241];
% ylims = [-20.3404   20.2288];
% for k = 1:numel(ha)
%     axes(ha(k));
%     line([0 0], ylims, 'color',[0.5 0.5 0.5],'linewidth',1); hold on;
%     line(xlims, [0 0], 'color',[0.5 0.5 0.5],'linewidth',1);
% end
% axes(ha(1)); scatter(preR(bH),postR(bH),msz,BLUE,'filled'); boto;
% [r,p] = corr(preR(bH)',postR(bH)');
% lh = lsline(); set(lh, 'linewidth',2,'color','k');
% text(0.5,1.1,'Hit','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(2)); scatter(preR(bM),postR(bM),msz+4,BLUE); boto;
% [r,p] = corr(preR(bH)',postR(bH)');
% lh = lsline(); set(lh, 'linewidth',2,'color','k');
% text(0.5,1.1,'Miss','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% axes(ha(3)); scatter(preR(bFA),postR(bFA),msz+4,RED); boto;
% [r,p] = corr(preR(bH)',postR(bH)');
% lh = lsline(); set(lh, 'linewidth',2,'color','k');
% text(0.5,1.1,'FA','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% set(ha(3), 'xtick',-20:10:20, 'xticklabel',-20:10:20, 'ytick',-20:10:20, 'yticklabel',-20:10:20);
% xlabel('VSDI activity PRE'); ylabel('VSDI activity POST');
% axes(ha(4)); scatter(preR(bCR),postR(bCR),msz,RED,'filled'); boto;
% [r,p] = corr(preR(bH)',postR(bH)');
% lh = lsline(); set(lh, 'linewidth',2,'color','k');
% text(0.5,1.1,'CR','FontSize',18,'FontName','Helvetica','HorizontalAlignment','center','Units','Normalized')
% set(ha,'xlim',xlims)
% set(ha,'ylim',ylims)
% 
% % 1.1 -- Clean up these comparisons with a proper linear model
% % tbl = table(postR',preR',categorical(bH|bCR),categorical(bP),'VariableNames',{'POST','PRE','Correct','Present'});
% % tbl = rmmissing(tbl);
% % fitlm(tbl,'POST ~ PRE*Correct*Present');
% 
% % 2 -- low activity ==> incorrect/chance-level behavior, attentional downstate) 
% % t-test of average DF/F pre-stimulus onset between correct and incorrect 
% % trails & median-split performance based on pre-stimulus DF/F
% 
% figure;
% ha = tight_subplot(1, 2, .1, .1, .1);
% axes(ha(1));
% grp1 = preR(bH|bCR);
% grp2 = preR(bFA|bM);
% [AAMean, AALOSE, AAHISE] = deal(nanmean(grp1), nanmean(grp1)-nanstd(grp1)./sqrt(sum(~isnan(grp1))), nanmean(grp1)+nanstd(grp1)./sqrt(sum(~isnan(grp1))));
% [DDMean, DDLOSE, DDHISE] = deal(nanmean(grp2), nanmean(grp2)-nanstd(grp2)./sqrt(sum(~isnan(grp2))), nanmean(grp2)+nanstd(grp2)./sqrt(sum(~isnan(grp2))));
% h = ShowBarPlotWithError(1,AAMean,AALOSE,AAHISE,[0 0 0],'-',.5);
% h = ShowBarPlotWithError(2,DDMean,DDLOSE,DDHISE,[0.5 0.5 0.5],'-',.5); botos;
% set(ha(1), 'xtick',[1 2],'xticklabel',{'Correct','Incorrect'}, 'ytick',-0.3:0.1:0.2,'yticklabel',-0.3:0.1:0.2)
% line(get(gca,'xlim'),[0 0],'linewidth',1,'color','k');
% ylabel('VSDI activity PRE');
% [h, p] = ttest2(grp1, grp2);
% axes(ha(2));
% grp1 = preR >= nanmedian(preR);
% grp2 = preR < nanmedian(preR);
% [bootValsAA AAMean AALOSE AAHISE AALO95 AAHI95 AALO99 AAHI99] = BootWeightedMean((bH(grp1)|bCR(grp1)),ones(size(bH(grp1))),1000);
% [bootValsDD DDMean DDLOSE DDHISE DDLO95 DDHI95 DDLO99 DDHI99] = BootWeightedMean((bH(grp2)|bCR(grp2)),ones(size(bH(grp2))),1000);
% h = ShowBarPlotWithError(1,AAMean,AALOSE,AAHISE,BLUE,'-',.5);
% h = ShowBarPlotWithError(2,DDMean,DDLOSE,DDHISE,RED,'-',.5); botos;
% set(gca,'ylim',[0.7 0.75])
% set(ha(2), 'xtick',[1 2],'xticklabel',{'Top 50%','Bottom 50%'}, 'ytick',0.7:0.01:0.75,'yticklabel',0.7:0.01:0.75)
% ylabel('Percent correct'); xlabel('VSDI activity PRE');
% % [h, p] = ttest2(grp1, grp2);