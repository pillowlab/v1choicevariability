% Do bootstrapping of DF and MF estimates, with trials subsampled by eye
% movement data. RUN ON NON-PERSONAL MACHINE, this takes a lot of time.

a0_setDirs;

% Load data

load([WriteDir 'dat_MM190805_pruned']);  % loads 'dat' with trial information
load([WriteDir 'AmpsByModel']);  % new data (from new maps)
load([WriteDir 'ii25']);  % load list of trials which were in blocks of contrast 25
load([WriteDir 'eyeMvts_bestWorst.mat']); % load list of best/worst within/across eye movement stats

% % % USE THIS DEFN OF SUBS TO SPLIT BY EYE MOVEMENT
% %     subs = {find(bBestWithin)',find(bWorstWithin)',find(bBestAcross)',find(bWorstAcross)'};

% % % USE THIS DEFN OF SUBS TO REPLICATE THE ORIG DF/MF BOOTSTRAP (TABLE 1)
% %     subs = {(1:size(AmpsByModel,2))'};
    
% THIS ONE DOES BOTH (for pooling MODEL 1)
    subs = {(1:size(AmpsByModel,2))',find(bBestWithin)',find(bWorstWithin)',find(bBestAcross)',find(bWorstAcross)'};

mVSDI = AmpsByModel';
ntrials = size(mVSDI,1);
dat = rmfield(dat,{'blockcontr','blockcontrmax'});

NBOOT = 5000;
NSUBS = numel(subs); % We're choosing one model for four different subsamplings by eye movement.

%%

warning('on')
[MFs,DFs,exitflags] = deal(zeros(NBOOT,NSUBS));

for isub = 1:NSUBS  % Specify which (of 9) pooling models to use
    fprintf('-- Eye movement condition %d -- \n', isub);
for jboot = 4404:NBOOT
    fprintf('.');
    if mod(jboot,50) == 0, fprintf('\n'); end
    
    % Create bootstrap resampling from the candidate trials
    iiboot = sort(randsample(subs{isub},numel(subs{isub}),true));
    % Apply the bootstrap down/resampling to the data structure
    datboot = dat;
    fields = fieldnames(datboot);
    for k = 1:length(fields)
        x = datboot.(fields{k});
        x = x(iiboot,:);
        datboot = setfield(datboot,fields{k},x);
    end
    ii25boot = ii25(iiboot);

    % Downsample data by eye movement condition and resample w/ bootstrap
    Mraw = mVSDI(iiboot,1); % measurements
    D = (datboot.bH|datboot.bFA);   % decision (1=yes or 0=no)
    X = (datboot.bH|datboot.bM);   % stimulus (1=present or 0=absent)
    contr = datboot.contr;  % contrast of that trial
    cs = unique(contr); cs = cs(cs>0);  % list of distinct contrasts
    ncs = length(cs); % number of contrasts
    nsess = max(datboot.sessnums); % number of sessions

    % Estimate the SNR for each experiment and contrast level
    [M,Xeff,Mwts,Mmus,Mmupred,usess,ucontr] = findExptSNRs(Mraw,X,datboot);

    % Build data matrix
    Dat = [Xeff,M,D,Mwts];

    % Remove high contrast trials
    Dat1 = Dat;
    Dat1(ii25boot,:) = [];

    % Fit model to all low-contrast data
    [prshat1, ~, exitflag, ~] = compMLest_VSDIvarSNRdata(Dat1);
    DFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.vardi);
    MFhat1 = prshat1.varsh/(prshat1.varsh+prshat1.varmi);
    % Save bootstrapped estimates
    DFs(jboot,isub) = DFhat1;
    MFs(jboot,isub) = MFhat1;
    exitflags(jboot,isub) = exitflag;    
end    
end

save([WriteDir 'eyeMvts_DFMFbootstrap'], 'DFs', 'MFs', 'exitflags')
