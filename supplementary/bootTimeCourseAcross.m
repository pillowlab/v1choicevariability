function out = bootTimeCourseAcross(group1, group2)

    bootWeightedMeanTC = zeros(size(SPTimeCourseRenormalized,1),NBOOT);
    trialIndices = find(group1|group2);
    NG1 = sum(group1); % Number of trials in group 1
    for k = 1:NBOOT
        bGroup1 = false(NTRIALS,1);
        bGroup2 = false(NTRIALS,1);
        % Resample the two groups by shuffling trial indices
        resampling = randsample(trialIndices,numel(trialIndices));
        bGroup1(resampling(1:NG1)) = true;
        bGroup2(resampling((NG1+1):end)) = true;

        % Recompute weighted mean difference waveform
        [wmdTC, ~] = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,bGroup1,bGroup2);
        bootWeightedMeanTC(:,k) = wmdTC;
    end
    trueWeightedMeanTC = ComputeWeightedMeanAndSEDifferenceWaveform(SPTimeCourseRenormalized,Weights,group1,group2);
    % Convolve all signals into 100ms bins
    bootTCBinned_PA    = conv2(bootWeightedMeanTC, kern, 'valid');
    bootTCBinnedPrc_PA = prctile(bootTCBinned_PA,[2.5 97.5],2);
    trueTCBinned    = conv(trueWeightedMeanTC, kern, 'valid');
    % Measure autocorrelation of signal?
    significance_PA = (trueTCBinned > bootTCBinnedPrc_PA(:,2)' |  trueTCBinned < bootTCBinnedPrc_PA(:,1)');