function [WeightedMeanTC WeightedSETC] = ...
    ComputeWeightedMeanAndSEWaveform_heavy(SPTimeCourse,Weights,bA)

% THIS IS A COPY OF ``ComputeWeightedMeanAndSEWaveform``, WITH A MUCH
% LARGER NUMBER OF BOOTSTRAP ITERS. MOST FUNCTIONS ALSO CALL THE ORIGINAL, 
% but were originally defined for 100 bootstrap iters (this is too few,
% no?). Those functions are untenable if we blow up this number. 
%
% Intended use case: Call it once with the ideal number of bootstrap iters,
% and save the intermediate result so that you don't have to do it again.
% (At time of writing, this is the normalizations for the timecourses.)


Frames = size(SPTimeCourse,1);
WeightedMeanTC = NaN([1 Frames]);
WeightedSETC = NaN([1 Frames]);
B = 5000;

% 
% for i=1:Frames
%     
%     % identify missing values
%     bNaN = isnan(SPTimeCourse(i,:));
% 
%     % A
%     bTheseA = bA' & ~bNaN;
%     nA = sum(bTheseA);
%     theseA = SPTimeCourse(i,bTheseA);
%     thisWeightedMeanA = sum(theseA .* Weights(bTheseA)) / sum(Weights(bTheseA));
%     thisWeightedVarA = sum((theseA - thisWeightedMeanA).^2 .* Weights(bTheseA)) / sum(Weights(bTheseA));
% 
%     % mean difference
%     WeightedMeanTC(i) = thisWeightedMeanA;
%     
%     % SE
%     WeightedSETC(i) = sqrt(thisWeightedVarA/nA);
%     
% end


for i=1:Frames
    
    % identify missing values
    bNaN = isnan(SPTimeCourse(i,:));

    % A
    bTheseA = bA' & ~bNaN;
    nA = sum(bTheseA);
    theseA = SPTimeCourse(i,bTheseA);
    theseWeights = Weights(bTheseA);
    
    for b=1:B
        bsWeightedMeanA(b) = mean(randsample(theseA,nA,true,theseWeights));
    end
        
    % mean
    WeightedMeanTC(i) = mean(bsWeightedMeanA);
    
    % SE
    WeightedSETC(i) = std(bsWeightedMeanA);
    
end


